<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>distmat</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>distmat</title>
      <description>
        <text lang="en">Create a distance matrix from a multiple sequence alignment</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/distmat.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>phylogeny:distance</category>
    <command>distmat</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <datatype>
              <class>Alignment</class>
            </datatype>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>MSF</dataFormat>
            <dataFormat>PAIR</dataFormat>
            <dataFormat>MARKX0</dataFormat>
            <dataFormat>MARKX1</dataFormat>
            <dataFormat>MARKX2</dataFormat>
            <dataFormat>MARKX3</dataFormat>
            <dataFormat>MARKX10</dataFormat>
            <dataFormat>SRS</dataFormat>
            <dataFormat>SRSPAIR</dataFormat>
            <dataFormat>SCORE</dataFormat>
            <dataFormat>UNKNOWN</dataFormat>
            <dataFormat>MULTIPLE</dataFormat>
            <dataFormat>SIMPLE</dataFormat>
            <dataFormat>MATCH</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
          <comment>
            <text lang="en">File containing a sequence alignment.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_required</name>
      <prompt lang="en">Required section</prompt>

      <parameters>

        <parameter>
          <name>e_nucmethod</name>
          <prompt lang="en">Multiple substitution correction methods for nucleotides</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <vlist>
            <velem>
              <value>0</value>
              <label>Uncorrected</label>
            </velem>
            <velem>
              <value>1</value>
              <label>Jukes-cantor</label>
            </velem>
            <velem>
              <value>2</value>
              <label>Kimura</label>
            </velem>
            <velem>
              <value>3</value>
              <label>Tamura</label>
            </velem>
            <velem>
              <value>4</value>
              <label>Tajima-nei</label>
            </velem>
            <velem>
              <value>5</value>
              <label>Jin-nei gamma</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -nucmethod=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>2</argpos>
          <comment>
            <text lang="en">Multiple substitution correction methods for nucleotides.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_protmethod</name>
          <prompt lang="en">Multiple substitution correction methods for proteins</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <vlist>
            <velem>
              <value>0</value>
              <label>Uncorrected</label>
            </velem>
            <velem>
              <value>1</value>
              <label>Jukes-cantor</label>
            </velem>
            <velem>
              <value>2</value>
              <label>Kimura protein</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -protmethod=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>3</argpos>
          <comment>
            <text lang="en">Multiple substitution correction methods for proteins.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_additional</name>
      <prompt lang="en">Additional section</prompt>

      <parameters>

        <parameter>
          <name>e_ambiguous</name>
          <prompt lang="en">Use the ambiguous codes in the calculation.</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -ambiguous")[ bool(value) ]</code>
          </format>
          <argpos>4</argpos>
          <comment>
            <text lang="en">Option to use the ambiguous codes in the calculation of the Jukes-Cantor method or if the sequences are proteins.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_gapweight</name>
          <prompt lang="en">Weight given to gaps</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.</value>
          </vdef>
          <format>
            <code proglang="python">("", " -gapweight=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>5</argpos>
          <comment>
            <text lang="en">Option to weight gaps in the uncorrected (nucleotide) and Jukes-Cantor distance methods.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_position</name>
          <prompt lang="en">Base position to analyse</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>123</value>
          </vdef>
          <format>
            <code proglang="python">("", " -position=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>6</argpos>
          <comment>
            <text lang="en">Choose base positions to analyse in each codon i.e. 123 (all bases), 12 (the first two bases), 1, 2, or 3 individual  bases.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_calculatea</name>
          <prompt lang="en">Calculate the nucleotide jin-nei parameter 'a'</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -calculatea")[ bool(value) ]</code>
          </format>
          <argpos>7</argpos>
          <comment>
            <text lang="en">This will force the calculation of parameter 'a' in the Jin-Nei Gamma distance calculation, otherwise the default is 1.0  (see -parametera option).</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_parametera</name>
          <prompt lang="en">Nucleotide jin-nei parameter 'a'</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>1.0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -parametera=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>8</argpos>
          <comment>
            <text lang="en">User defined parameter 'a' to be use in the Jin-Nei Gamma distance calculation. The suggested value to be used is 1.0 (Jin  et al.) and this is the default.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the output file (e_outfile)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>distmat.e_outfile</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>9</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>EmbossDistanceMatrix</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>10</argpos>
    </parameter>
  </parameters>
</program>

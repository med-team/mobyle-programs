<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>clustalw-sequence</name>
    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="Entities/ClustalW_package.xml"/>
    <doc>
      <title>Clustalw: Sequence to Profile alignments</title>
      <description>
        <text lang="en">Sequentially add profile2 sequences to profile1 alignment</text>
      </description>
    </doc>
    <category>alignment:multiple</category>
    <command>clustalw -sequences</command>
  </head>
  <parameters>
    <paragraph>
      <name>profile</name>
      <prompt lang="en">Profile Alignments parameters</prompt>
      <argpos>2</argpos>
      <comment>
        <text lang="en">By PROFILE ALIGNMENT, we mean alignment using existing alignments. Profile alignments allow you to store alignments of your favorite sequences and add new sequences to them in small bunches at a time. (e.g. an alignment output file from CLUSTAL W). One or both sets of input sequences may include secondary structure assignments or gap penalty masks to guide the alignment.</text>
        <text lang="en">Merge 2 alignments by profile alignment</text>
      </comment>
      <parameters>
        <parameter ismandatory="1" ismaininput="1" issimple="1">
          <name>profile1</name>
          <prompt lang="en">Profile 1</prompt>
          <type>
            <datatype>
              <class>Alignment</class>
            </datatype>
            <dataFormat>CLUSTAL</dataFormat>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -profile1=$value" : ""</code>
            <code proglang="python">( "" , " -profile1=" + str( value ) )[value is not None]</code>
          </format>
        </parameter>
        <parameter ismandatory="1" ismaininput="1" issimple="1">
          <name>profile2</name>
          <prompt lang="en">Profile 2</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>FASTA</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -profile2=$value" : ""</code>
            <code proglang="python">( "" , " -profile2=" + str( value ) )[value is not None]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>general_settings</name>
      <prompt lang="en">General settings</prompt>
      <argpos>3</argpos>
      <parameters>
        <parameter>
          <name>typeseq</name>
          <prompt lang="en">Protein or DNA (-type)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>auto</value>
          </vdef>
          <vlist>
            <velem undef="1">
              <value>auto</value>
              <label>Automatic</label>
            </velem>
            <velem>
              <value>protein</value>
              <label>Protein</label>
            </velem>
            <velem>
              <value>dna</value>
              <label>DNA</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value) ? " -type=$value" : ""</code>
            <code proglang="python">("", " -type="+str(value))[value is not None]</code>
          </format>
        </parameter>
        <parameter issimple="1">
          <name>quicktree</name>
          <prompt lang="en">Toggle Slow/Fast pairwise alignments (-quicktree)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>slow</value>
          </vdef>
          <vlist>
            <velem>
              <value>slow</value>
              <label>Slow</label>
            </velem>
            <velem>
              <value>fast</value>
              <label>Fast</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">($value eq "fast") ? " -quicktree" : ""</code>
            <code proglang="python">( "" , " -quicktree")[ value == "fast"]</code>
          </format>
          <comment>
            <text lang="en">slow: by dynamic programming (slow but accurate)</text>
            <text lang="en">fast: method of Wilbur and Lipman (extremely fast but approximate)</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>fastpw</name>
      <prompt lang="en">Fast Pairwise Alignments parameters</prompt>
      <precond>
        <code proglang="perl">$quicktree eq "fast"</code>
        <code proglang="python">quicktree == "fast"</code>
      </precond>
      <argpos>2</argpos>
      <comment>
        <text lang="en">These similarity scores are calculated from fast, approximate, global alignments, which are controlled by 4 parameters. 2 techniques are used to make these alignments very fast: 1) only exactly matching fragments (k-tuples) are considered; 2) only the 'best' diagonals (the ones with most k-tuple matches) are used.</text>
      </comment>
      <parameters>
        <parameter>
          <name>ktuple</name>
          <prompt lang="en">Word size (-ktuple)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -ktuple=$value" : ""</code>
            <code proglang="python">( "" , " -ktuple=" + str( value ) )[value is not None and value != vdef ]</code>
          </format>
          <argpos>2</argpos>
          <comment>
            <text lang="en">K-TUPLE SIZE: This is the size of exactly matching fragment that is used. INCREASE for speed (max= 2 for proteins; 4 for DNA), DECREASE for sensitivity. For longer sequences (e.g. &gt;1000 residues) you may need to increase the default.</text>
          </comment>
        </parameter>
        <parameter>
          <name>topdiags</name>
          <prompt lang="en">Number of best diagonals (-topdiags)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>5</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -topdiags=$value" : ""</code>
            <code proglang="python">( "" , " -topdiags=" + str( value ))[value is not None and value != vdef ]</code>
          </format>
          <argpos>2</argpos>
          <comment>
            <text lang="en">The number of k-tuple matches on each diagonal (in an imaginary dot-matrix plot) is calculated. Only the best ones (with most matches) are used in the alignment. This parameter specifies how many. Decrease for speed; increase for sensitivity.</text>
          </comment>
        </parameter>
        <parameter>
          <name>window</name>
          <prompt lang="en">Window around best diags (-window)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>5</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -window=$value" : ""</code>
            <code proglang="python">( "" , " -window=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
          <argpos>2</argpos>
          <comment>
            <text lang="en">WINDOW SIZE: This is the number of diagonals around each of the 'best' diagonals that will be used. Decrease for speed; increase for sensitivity</text>
          </comment>
        </parameter>
        <parameter>
          <name>pairgap</name>
          <prompt lang="en">Gap penalty (-pairgap)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>3</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -pairgap=$value" : ""</code>
            <code proglang="python">( "" , " -pairgap=" + str( value ))[ value is not None and value != vdef ]</code>
          </format>
          <argpos>2</argpos>
          <comment>
            <text lang="en">This is a penalty for each gap in the fast alignments. It has little affect on the speed or sensitivity except for extreme values.</text>
          </comment>
        </parameter>
        <parameter>
          <name>score</name>
          <prompt lang="en">Percent or absolute score ? (-score)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>percent</value>
          </vdef>
          <vlist>
            <velem>
              <value>percent</value>
              <label>Percent</label>
            </velem>
            <velem>
              <value>absolute</value>
              <label>Absolute</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? " -score=$value" : ""</code>
            <code proglang="python">( "" , " -score=" +str( value ) )[value is not None and value !=vdef]</code>
          </format>
          <argpos>2</argpos>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>slowpw</name>
      <prompt lang="en">Slow Pairwise Alignments parameters</prompt>
      <precond>
        <code proglang="perl">$quicktree eq "slow"</code>
        <code proglang="python">quicktree == "slow"</code>
      </precond>
      <argpos>2</argpos>
      <comment>
        <text lang="en">These parameters do not have any affect on the speed of the alignments. They are used to give initial alignments which are then rescored to give percent identity scores. These % scores are the ones which are displayed on the screen. The scores are converted to distances for the trees.</text>
      </comment>
      <parameters>
        <parameter>
          <name>pwgapopen</name>
          <prompt lang="en">Gap opening penalty (-pwgapopen)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>10.00</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -pwgapopen=$value" : ""</code>
            <code proglang="python">( "" , " -pwgapopen=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
        </parameter>
        <parameter>
          <name>pwgapext</name>
          <prompt lang="en">Gap extension penalty (-pwgapext)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.10</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -pwgapext=$value" : ""</code>
            <code proglang="python">( "" , " -pwgapext=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
        </parameter>
        <paragraph>
          <name>slowpw_prot</name>
          <prompt lang="en">Protein parameters</prompt>
          <precond>
            <code proglang="perl">$typeseq eq "protein"</code>
            <code proglang="python">typeseq == "protein"</code>
          </precond>
          <parameters>
            <parameter>
              <name>pwmatrix</name>
              <prompt lang="en">Protein weight matrix (-pwmatrix)</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>gonnet</value>
              </vdef>
              <vlist>
                <velem>
                  <value>blosum</value>
                  <label>BLOSUM30 (Henikoff)</label>
                </velem>
                <velem>
                  <value>gonnet</value>
                  <label>Gonnet 250</label>
                </velem>
                <velem>
                  <value>pam</value>
                  <label>PAM350 (Dayhoff)</label>
                </velem>
                <velem>
                  <value>id</value>
                  <label>Identity matrix</label>
                </velem>
              </vlist>
              <format>
                <code proglang="perl">(defined $value and $value ne $vdef) ? " -pwmatrix=$value" : ""</code>
                <code proglang="python">( "" , " -pwmatrix=" + str(value) )[value is not None and value != vdef ]</code>
              </format>
              <comment>
                <text lang="en">The scoring table which describes the similarity of each amino acid to each other. For DNA, an identity matrix is used.</text>
                <text lang="en">BLOSUM (Henikoff). These matrices appear to be the best available for carrying out data base similarity (homology searches). The matrices used are: Blosum80, 62, 40 and 30.</text>
                <text lang="en">The Gonnet Pam 250 matrix has been reported as the best single matrix for alignment, if you only choose one matrix. Our experience with profile database searches is that the Gonnet series is unambiguously superior to the Blosum series at high divergence. However, we did not get the series to perform systematically better than the Blosum series in Clustal W (communication of the authors).</text>
                <text lang="en">PAM (Dayhoff). These have been extremely widely used since the late '70s. We use the PAM 120, 160, 250 and 350 matrices.</text>
              </comment>
            </parameter>
          </parameters>
        </paragraph>
        <paragraph>
          <name>slowpw_dna</name>
          <prompt lang="en">DNA parameters</prompt>
          <precond>
            <code proglang="perl">$typeseq eq "dna"</code>
            <code proglang="python">typeseq == "dna"</code>
          </precond>
          <parameters>
            <parameter>
              <name>pwdnamatrix</name>
              <prompt lang="en">DNA weight matrix (-pwdnamatrix)</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>iub</value>
              </vdef>
              <vlist>
                <velem>
                  <value>iub</value>
                  <label>IUB</label>
                </velem>
                <velem>
                  <value>clustalw</value>
                  <label>CLUSTALW</label>
                </velem>
              </vlist>
              <format>
                <code proglang="perl">(defined $value and $value ne $vdef) ? " -pwdnamatrix=$value" : ""</code>
                <code proglang="python">( "" , " -pwdnamatrix=" + str(value) )[ value is not None and value != vdef ]</code>
              </format>
              <comment>
                <text lang="en">For DNA, a single matrix (not a series) is used. Two hard-coded matrices are available:</text>
                <text lang="en">1) IUB. This is the default scoring matrix used by BESTFIT for the comparison of nucleic acid sequences. X's and N's are treated as matches to any IUB ambiguity symbol. All matches score 1.9; all mismatches for IUB symbols score 0.</text>
                <text lang="en">2) CLUSTALW(1.6). The previous system used by ClustalW, in which matches score 1.0 and mismatches score 0. All matches for IUB symbols also score 0.</text>
              </comment>
            </parameter>
          </parameters>
        </paragraph>
      </parameters>
    </paragraph>
    <paragraph>
      <name>structure</name>
      <prompt lang="en">Structure Alignments parameters</prompt>
      <argpos>2</argpos>
      <comment>
        <text lang="en">These options, when doing a profile alignment, allow you to set 2D structure parameters. If a solved structure is available, it can be used to guide the alignment by raising gap penalties within secondary structure elements, so that gaps will preferentially be inserted into unstructured surface loops. Alternatively, a user-specified gap penalty mask can be supplied directly.</text>
        <text lang="en">A gap penalty mask is a series of numbers between 1 and 9, one per position in the alignment. Each number specifies how much the gap opening penalty is to be raised at that position (raised by multiplying the basic gap opening penalty by the number) i.e. a mask figure of 1 at a position means no change in gap opening penalty; a figure of 4 means that the gap opening penalty is four times greater at that position, making gaps 4 times harder to open.</text>
        <text lang="en">Gap penalty masks is to be supplied with the input sequences. The masks work by raising gap penalties in specified regions (typically secondary structure elements) so that gaps are preferentially opened in the less well conserved regions (typically surface loops).</text>
        <text lang="en">CLUSTAL W can read the masks from SWISS-PROT, CLUSTAL or GDE format input files. For many 3-D protein structures, secondary structure information is recorded in the feature tables of SWISS-PROT database entries. You should always check that the assignments are correct - some are quite inaccurate. CLUSTAL W looks for SWISS-PROT HELIX and STRAND assignments e.g.</text>
        <text lang="en">FT HELIX 100 115</text>
        <text lang="en">FT HELIX 100 115</text>
        <text lang="en">The structure and penalty masks can also be read from CLUSTAL alignment format as comment lines beginning !SS_ or GM_ e.g.</text>
        <text lang="en">!SS_HBA_HUMA ..aaaAAAAAAAAAAaaa.aaaAAAAAAAAAAaaaaaaAaaa.........aaaAAAAAA</text>
        <text lang="en">!GM_HBA_HUMA 112224444444444222122244444444442222224222111111111222444444</text>
        <text lang="en">HBA_HUMA VLSPADKTNVKAAWGKVGAHAGEYGAEALERMFLSFPTTKTYFPHFDLSHGSAQVKGHGK</text>
        <text lang="en">Note that the mask itself is a set of numbers between 1 and 9 each of which is assigned to the residue(s) in the same column below. In GDE flat file format, the masks are specified as text and the names must begin with SS_ or GM_. Either a structure or penalty mask or both may be used. If both are included in an alignment, the user will be asked which is to be used.</text>
      </comment>
      <parameters>
        <parameter>
          <name>nosecstr1</name>
          <prompt lang="en">Do not use secondary structure-gap penalty mask for profile 1 (-nosecstr1)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -nosecstr1" : ""</code>
            <code proglang="python">( "" , " -nosecstr1")[ value ]</code>
          </format>
          <argpos>2</argpos>
          <comment>
            <text lang="en">This option controls whether the input secondary structure information or gap penalty masks will be used.</text>
          </comment>
        </parameter>
        <parameter>
          <name>nosecstr2</name>
          <prompt lang="en">Do not use secondary structure-gap penalty mask for profile 2 (-nosecstr2)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -nosecstr2" : ""</code>
            <code proglang="python">( "" , " -nosecstr2")[ value ]</code>
          </format>
          <comment>
            <text lang="en">This option controls whether the input secondary structure information or gap penalty masks will be used.</text>
          </comment>
        </parameter>
        <parameter>
          <name>helixgap</name>
          <prompt lang="en">Helix gap penalty (-helixgap)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>4</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -helixgap=$value" : ""</code>
            <code proglang="python">( "" , " -helixgap=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
          <comment>
            <text lang="en">This option provides the value for raising the gap penalty at core Alpha Helical (A) residues. In CLUSTAL format, capital residues denote the A and B core structure notation. The basic gap penalties are multiplied by the amount specified.</text>
          </comment>
        </parameter>
        <parameter>
          <name>strandgap</name>
          <prompt lang="en">Strand gap penalty (-strandgap)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>4</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -strandgap=$value" : ""</code>
            <code proglang="python">( "" , " -strandgap=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
          <comment>
            <text lang="en">This option provides the value for raising the gap penalty at Beta Strand (B) residues. In CLUSTAL format, capital residues denote the A and B core structure notation. The basic gap penalties are multiplied by the amount specified.</text>
          </comment>
        </parameter>
        <parameter>
          <name>loopgap</name>
          <prompt lang="en">Loop gap penalty (-loopgap)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -loopgap=$value" : ""</code>
            <code proglang="python">( "" , " -loopgap=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
          <comment>
            <text lang="en">This option provides the value for the gap penalty in Loops. By default this penalty is not raised. In CLUSTAL format, loops are specified by . in the secondary structure notation.</text>
          </comment>
        </parameter>
        <parameter>
          <name>terminalgap</name>
          <prompt lang="en">Secondary structure terminal penalty (-terminalgap)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>2</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -terminalgap=$value" : ""</code>
            <code proglang="python">( "" , " -terminalgap=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
          <comment>
            <text lang="en">This option provides the value for setting the gap penalty at the ends of secondary structures. Ends of secondary structures are observed to grow and-or shrink in related structures. Therefore by default these are given intermediate values, lower than the core penalties. All secondary structure read in as lower case in CLUSTAL format gets the reduced terminal penalty.</text>
          </comment>
        </parameter>
        <parameter>
          <name>helixendin</name>
          <prompt lang="en">Helix terminal positions: number of residues inside helix to be treated as terminal (-helixendin)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>3</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -helixendin=$value" : ""</code>
            <code proglang="python">( "" , " -helixendin=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
          <comment>
            <text lang="en">This option (together with the -helixendin) specify the range of structure termini for the intermediate penalties. In the alignment output, these are indicated as lower case. For Alpha Helices, by default, the range spans the end helical turn.</text>
          </comment>
        </parameter>
        <parameter>
          <name>helixendout</name>
          <prompt lang="en">Helix terminal positions: number of residues outside helix to be treated as terminal (-helixendout)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -helixendout=$value" : ""</code>
            <code proglang="python">( "" , " -helixendout=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
          <comment>
            <text lang="en">This option (together with the -helixendin) specify the range of structure termini for the intermediate penalties. In the alignment output, these are indicated as lower case. For Alpha Helices, by default, the range spans the end helical turn.</text>
          </comment>
        </parameter>
        <parameter>
          <name>strandendin</name>
          <prompt lang="en">Strand terminal positions: number of residues inside strand to be treated as terminal (-strandendin)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -strandendin=$value" : ""</code>
            <code proglang="python">( "" , " -strandendin=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
          <comment>
            <text lang="en">This option (together with the -strandendout option) specify the range of structure termini for the intermediate penalties. In the alignment output, these are indicated as lower case. For Beta Strands, the default range spans the end residue and the adjacent loop residue, since sequence conservation often extends beyond the actual H-bonded Beta Strand.</text>
          </comment>
        </parameter>
        <parameter>
          <name>strandendout</name>
          <prompt lang="en">Strand terminal positions: number of residues outside strand to be treated as terminal (-strandendout)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -strandendout=$value" : ""</code>
            <code proglang="python">( "" , " -strandendout=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
          <comment>
            <text lang="en">This option (together with the -strandendin option) specify the range of structure termini for the intermediate penalties. In the alignment output, these are indicated as lower case. For Beta Strands, the default range spans the end residue and the adjacent loop residue, since sequence conservation often extends beyond the actual H-bonded Beta Strand.</text>
          </comment>
        </parameter>
        <parameter>
          <name>secstrout</name>
          <prompt lang="en">Output in alignment (-secstrout)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>STRUCTURE</value>
          </vdef>
          <vlist>
            <velem>
              <value>STRUCTURE</value>
              <label>Secondary Structure</label>
            </velem>
            <velem>
              <value>MASK</value>
              <label>Gap Penalty Mask</label>
            </velem>
            <velem>
              <value>BOTH</value>
              <label>Structure and Penalty Mask</label>
            </velem>
            <velem>
              <value>NONE</value>
              <label>None</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? " -secstrout=$value" : ""</code>
            <code proglang="python">( "" , " -secstrout=" + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
          <comment>
            <text lang="en">This option lets you choose whether or not to include the masks in the CLUSTAL W output alignments. Showing both is useful for understanding how the masks work. The secondary structure information is itself very useful in judging the alignment quality and in seeing how residue conservation patterns vary with secondary structure.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>outputparam</name>
      <prompt lang="en">Output parameters</prompt>
      <argpos>5</argpos>
      <parameters>
        <parameter>
          <name>outputformat</name>
          <prompt lang="en">Output format (-output)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>null</value>
          </vdef>
          <vlist>
            <velem undef="1">
              <value>null</value>
              <label>CLUSTAL</label>
            </velem>
            <velem>
              <value>GCG</value>
              <label>GCG</label>
            </velem>
            <velem>
              <value>GDE</value>
              <label>GDE</label>
            </velem>
            <velem>
              <value>PHYLIPI</value>
              <label>PHYLIP</label>
            </velem>
            <velem>
              <value>NEXUS</value>
              <label>NEXUS</label>
            </velem>
            <velem>
              <value>PIR</value>
              <label>PIR/NBRF</label>
            </velem>
            <velem>
              <value>FASTA</value>
              <label>FASTA</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? " -output=$value" : ""</code>
            <code proglang="python">( "" , " -output=" + str( value) )[ value is not None and value != vdef ]</code>
          </format>
        </parameter>
        <parameter>
          <name>seqnos</name>
          <prompt lang="en">Output sequence numbers in the output file (for clustalw output only) (-seqnos)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">not defined $outputformat</code>
            <code proglang="python">outputformat is None</code>
          </precond>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -seqnos=on" : ""</code>
            <code proglang="python">( "" , " -seqnos=on")[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>outorder</name>
          <prompt lang="en">Result order (-outorder)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>aligned</value>
          </vdef>
          <vlist>
            <velem>
              <value>input</value>
              <label>Input</label>
            </velem>
            <velem>
              <value>aligned</value>
              <label>Aligned</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? " -outorder=$value" : ""</code>
            <code proglang="python">( "" , " -outorder=" + str(value))[ value is not None and value != vdef ]</code>
          </format>
        </parameter>
        <parameter>
          <name>outfile</name>
          <prompt lang="en">Sequence alignment file name(-outfile)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -outfile=$value" : ""</code>
            <code proglang="python">( "" , " -outfile=" + str( value))[ value is not None ]</code>
          </format>
        </parameter>
        <parameter isout="1">
          <name>clustalaligfile</name>
          <prompt>Alignment file</prompt>
          <type>
            <datatype>
              <class>Alignment</class>
            </datatype>
            <dataFormat>CLUSTAL</dataFormat>
          </type>
          <precond>
            <code proglang="perl">not defined $outputformat</code>
            <code proglang="python">outputformat is None</code>
          </precond>
          <filenames>
            <code proglang="perl">(defined $outfile)? "$outfile":"*.aln"</code>
            <code proglang="python">("*.aln", str(outfile))[outfile is not None]</code>
          </filenames>
          <comment>
            <text lang="en">In the conservation line output in the clustal format alignment file, three characters are used:</text>
            <text lang="en">'*' indicates positions which have a single, fully conserved residue.</text>
            <text lang="en">':' indicates that one of the following 'strong' groups is fully conserved (STA,NEQK,NHQK,NDEQ,QHRK,MILV,MILF,HY,FYW).</text>
            <text lang="en">'.' indicates that one of the following 'weaker' groups is fully conserved (CSA,ATV,SAG,STNK,STPA,SGND,SNDEQK,NDEQHK,NEQHRK,FVLIM,HFY).</text>
            <text lang="en">These are all the positively scoring groups that occur in the Gonnet Pam250
matrix. The strong and weak groups are defined as strong score &gt;0.5 and weak
score =&lt;0.5 respectively.</text>
          </comment>
        </parameter>
        <parameter isout="1">
          <name>aligfile</name>
          <prompt>Alignment file</prompt>
          <type>
            <datatype>
              <class>Alignment</class>
            </datatype>
            <dataFormat>
              <ref param="outputformat"/>
            </dataFormat>
          </type>
          <precond>
            <code proglang="perl">$outputformat =~ /^(NEXUS|GCG|PHYLIPI|FASTA)$/</code>
            <code proglang="python">outputformat in [ "NEXUS", "GCG", "PHYLIPI","FASTA"]</code>
          </precond>
          <filenames>
            <code proglang="perl">(defined $outfile)? "$outfile":"*.fasta *.nxs *.phy *.msf"</code>
            <code proglang="python">{ "OUTFILE":outfile, "FASTA":"*.fasta", "NEXUS": "*.nxs", "PHYLIPI": "*.phy" , 'GCG': '*.msf' }[( "OUTFILE", outputformat)[outfile is None]]</code>
          </filenames>
        </parameter>
        <parameter isout="1">
          <name>seqfile</name>
          <prompt>Sequences file</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>
            	<test param="outputformat" eq="PIR">NBRF</test>
                <test param="outputformat" eq="GDE">GDE</test>
            </dataFormat>
          </type>
          <precond>
            <code proglang="perl">$outputformat =~ /^(GDE|PIR)$/</code>
            <code proglang="python">outputformat in [ 'GDE', 'PIR' ]</code>
          </precond>
          <filenames>
            <code proglang="perl">(defined $outfile)? "$outfile":"*.gde *.pir"</code>
            <code proglang="python">{ "OUTFILE":outfile,  'GDE':'*.gde', 'PIR':'*.pir}[( "OUTFILE", outputformat)[outfile is None]]</code>
          </filenames>
        </parameter>
        <parameter isout="1">
          <name>dndfile</name>
          <prompt>Tree file</prompt>
          <type>
            <datatype>
              <class>Tree</class>
            </datatype>
            <dataFormat>NEWICK</dataFormat>
          </type>
          <filenames>
            <code proglang="perl">"*.dnd"</code>
            <code proglang="python">"*.dnd"</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>
  </parameters>
</program>

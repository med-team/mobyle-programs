<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>emowse</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>emowse</title>
      <description>
        <text lang="en">Search protein sequences by digest fragment molecular weight</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/emowse.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:protein:composition</category>
    <command>emowse</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_infile</name>
          <prompt lang="en">Peptide molecular weight values file</prompt>
          <type>
            <datatype>
              <class>PeptideMolweights</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -infile=" + str(value))[value is not None]</code>
          </format>
          <argpos>2</argpos>
        </parameter>

        <parameter>
          <name>e_mwdata</name>
          <prompt lang="en">Molecular weights data file</prompt>
          <type>
            <datatype>
              <class>MolecularWeights</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -mwdata=" + str(value))[value is not None ]</code>
          </format>
          <argpos>3</argpos>
        </parameter>

        <parameter>
          <name>e_frequencies</name>
          <prompt lang="en">Amino acid frequencies data file</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>AminoAcidFrequencies</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -frequencies=" + str(value))[value is not None ]</code>
          </format>
          <argpos>4</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_required</name>
      <prompt lang="en">Required section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_weight</name>
          <prompt lang="en">Whole sequence molwt</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -weight=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>5</argpos>
        </parameter>

        <parameter>
          <name>e_mono</name>
          <prompt lang="en">Use monoisotopic weights</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -mono")[ bool(value) ]</code>
          </format>
          <argpos>6</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_advanced</name>
      <prompt lang="en">Advanced section</prompt>

      <parameters>

        <parameter>
          <name>e_enzyme</name>
          <prompt lang="en">Enzymes and reagents</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <vlist>
            <velem>
              <value>1</value>
              <label>Trypsin</label>
            </velem>
            <velem>
              <value>2</value>
              <label>Lys-c</label>
            </velem>
            <velem>
              <value>3</value>
              <label>Arg-c</label>
            </velem>
            <velem>
              <value>4</value>
              <label>Asp-n</label>
            </velem>
            <velem>
              <value>5</value>
              <label>V8-bicarb</label>
            </velem>
            <velem>
              <value>6</value>
              <label>V8-phosph</label>
            </velem>
            <velem>
              <value>7</value>
              <label>Chymotrypsin</label>
            </velem>
            <velem>
              <value>8</value>
              <label>Cnbr</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -enzyme=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>7</argpos>
        </parameter>

        <parameter>
          <name>e_pcrange</name>
          <prompt lang="en">Allowed whole sequence weight variability (value from 0 to 75)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>25</value>
          </vdef>
          <format>
            <code proglang="python">("", " -pcrange=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 0 is required</text>
            </message>
            <code proglang="python">value &gt;= 0</code>
          </ctrl>
          <ctrl>
            <message>
              <text lang="en">Value less than or equal to 75 is required</text>
            </message>
            <code proglang="python">value &lt;= 75</code>
          </ctrl>
          <argpos>8</argpos>
        </parameter>

        <parameter>
          <name>e_tolerance</name>
          <prompt lang="en">Tolerance (value from 0.1 to 1.0)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.1</value>
          </vdef>
          <format>
            <code proglang="python">("", " -tolerance=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 0.1 is required</text>
            </message>
            <code proglang="python">value &gt;= 0.1</code>
          </ctrl>
          <ctrl>
            <message>
              <text lang="en">Value less than or equal to 1.0 is required</text>
            </message>
            <code proglang="python">value &lt;= 1.0</code>
          </ctrl>
          <argpos>9</argpos>
        </parameter>

        <parameter>
          <name>e_partials</name>
          <prompt lang="en">Partials factor (value from 0.1 to 1.0)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.4</value>
          </vdef>
          <format>
            <code proglang="python">("", " -partials=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 0.1 is required</text>
            </message>
            <code proglang="python">value &gt;= 0.1</code>
          </ctrl>
          <ctrl>
            <message>
              <text lang="en">Value less than or equal to 1.0 is required</text>
            </message>
            <code proglang="python">value &lt;= 1.0</code>
          </ctrl>
          <argpos>10</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the output file (e_outfile)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>emowse.e_outfile</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>11</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>EmowseReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>12</argpos>
    </parameter>
  </parameters>
</program>

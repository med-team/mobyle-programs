<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program xmlns:xi="http://www.w3.org/2001/XInclude">
  <head>
    <name>rnafold</name>
    <xi:include href="Entities/ViennaRNA_package.xml"/>
    <doc>
      <title>RNAfold</title>
      <description>
        <text lang="en">Calculate secondary structures of RNAs</text>
      </description>
      <authors>Hofacker, Fontana, Bonhoeffer, Stadler</authors>
      <reference>I.L. Hofacker, W. Fontana, P.F. Stadler, S. Bonhoeffer, M. Tacker, P. Schuster (1994) Fast Folding and Comparison of RNA Secondary Structures. Monatshefte f. Chemie 125: 167-188</reference>
      <reference>A. Walter, D Turner, J Kim, M Lyttle, P Muller, D Mathews, M Zuker Coaxial stacking of helices enhances binding of oligoribonucleotides. PNAS, 91, pp 9218-9222, 1994</reference>
      <reference>M. Zuker, P. Stiegler (1981) Optimal computer folding of large RNA sequences using thermodynamic and auxiliary information, Nucl Acid Res 9: 133-148</reference>
      <reference>J.S. McCaskill (1990) The equilibrium partition function and base pair binding probabilities for RNA secondary structures, Biopolymers 29: 11051119 D.H. Turner N. Sugimoto and S.M. Freier (1988) RNA structure prediction, Ann Rev Biophys Biophys Chem 17: 167-192</reference>
      <reference>D. Adams (1979) The hitchhiker's guide to the galaxy, Pan Books, London</reference>
      <doclink>http://www.tbi.univie.ac.at/RNA/RNAfold.html</doclink>
      <comment>
        <text lang="en">RNAfold reads RNA sequences, calculates their minimum free energy (mfe) structure and prints the mfe structure in bracket notation and its free energy. It also produces PostScript files with plots of the resulting secondary structure graph and a "dot plot" of the base pairing matrix.</text>
      </comment>
    </doc>
    <category>sequence:nucleic:2D_structure</category>
    <category>structure:2D_structure</category>
    <command>RNAfold</command>
  </head>
  <parameters>
    <parameter ismandatory="1" issimple="1">
      <name>seqin</name>
      <prompt lang="en">RNA/DNA Sequence File</prompt>
      <type>
      <biotype>RNA</biotype>
      <biotype>DNA</biotype>
        <datatype>
          <class>SequenceWithStructureConstraint</class>
					<superclass>AbstractText</superclass>
        </datatype>
        <dataFormat>FASTA</dataFormat>
      </type>
      <format>
        <code proglang="perl">" &lt;$value"</code>
        <code proglang="python">" &lt;"+str(value)</code>
      </format>
      <comment>
        <text lang="en">The string for the structure constraint must be of the length of the sequence.</text>
        <text lang="en">Rq: No constraint should be applied during structure predictions.</text>
        <text lang="en">Structure constraint:</text>
        <text lang="en">| : paired with another base</text>
        <text lang="en">&gt; : base i is paired with a base j&gt;i</text>
        <text lang="en">x : base must not pair</text>
        <text lang="en">&lt; : base i is paired with a base j&lt;i </text>
        <text lang="en">. : no constraint at all</text>
        <text lang="en">matching brackets ( ): base i pairs base j</text>
      </comment>
      <argpos>1000</argpos>
    </parameter>
    <paragraph>
      <name>control</name>
      <prompt lang="en">Control options</prompt>
      <argpos>2</argpos>
      <parameters>
        <parameter>
          <name>partition</name>
          <prompt lang="en">Calculate the partition function and base pairing probability matrix (-p)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">not $pf</code>
            <code proglang="python">not pf</code>
          </precond>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -p" : ""</code>
            <code proglang="python">( "" , " -p" )[ value ]</code>
          </format>
          <comment>
            <text lang="en">Calculate the partition function and base pairing probability matrix 
            in addition to the mfe structure. Default is calculation of mfe structure only. 
            Prints a coarse representation of the pair probabilities in form of a pseudo bracket 
            notation, the ensemble free energy, the frequency of the mfe structure, and the 
            structural diversity. Note that unless you also specify -d2 or -d0, the partition 
            function and mfe calculations will use a slightly different energy model. See the 
            discussion of dangling end options below.</text>
          </comment>
        </parameter>
        <parameter>
          <name>pf</name>
          <prompt lang="en">Calculate  the partition function but not the pair probabilities (-p0)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">not $partition</code>
            <code proglang="python">not partition</code>
          </precond>
		  <vdef>
		  	<value>0</value>
		  </vdef>
          <format>
            <code proglang="perl">(defined $value)? " -p0" : ""</code>
            <code proglang="python">( "" , " -p0" )[ value ]</code>
          </format>
          <comment>
            <text lang="en">Calculate  the partition function but not the pair probabilities, 
            saving about 50% in runtime. Prints the  ensemble free energy -kT ln(Z).</text>
          </comment>
        </parameter>
        <parameter>
	<name>p2</name>
	<prompt lang="en">In addition to pair probabilities compute stack probabilities (-p2)</prompt>
      <type>
      <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
		  <vdef>
		  	<value>0</value>
		  </vdef>
          <format>
            <code proglang="perl">(defined $value)? " -p2" : ""</code>
            <code proglang="python">( "" , " -p2 " )[ value ]</code>
          </format>
            <comment>
            	<text lang="en">    In addition to pair probabilities compute stack probabil-
              ities, i.e. the probability that a  pair  (i,j  and  the
              immediately interior pair (i+1,j-1) are formed simultane-
              ously. A second postscript dot plot called "name_dp2.ps",
              or  "dot2.ps"  (if the sequence does not have a name), is
              produced that contains pair probabilities  in  the  upper
              right half and stack probabilities in the lower left.</text>
              </comment>
         </parameter>
        
        <parameter>
        	<name>mea</name>
        	<prompt lang="en">Calculate an MEA (maximum expected accuracy) structure</prompt>
        	<type>
        		<datatype>
        			<class>Float</class>
        		</datatype>
        	</type>
        	<precond>
        		<code proglang="perl">defined $partition</code>
        		<code proglang="python">partition</code>
        	</precond>
        	<vdef>
        		<value>1</value>
        	</vdef>
        	<format>
            <code proglang="perl">(defined $value and $value != $vdef)? " -MEA $value" : ""</code>
            <code proglang="python">( "" , " -MEA " + str(value) )[ value is not None and value != vdef]</code>
          </format>
          <comment>
          	<text lang="en">
              Calculate  an  MEA (maximum expected accuracy) structure,
              where the expected accuracy is  computed  from  the  pair
              probabilities:   each   base  pair  (i,j)  gets  a  score
              2*gamma*p_ij and the score of an unpaired base  is  given
              by  the  probability of not forming a pair. The parameter
              gamma tunes the importance of correctly  predicted  pairs
              versus  unpaired  bases.  Thus, for small values of gamma
              the MEA structure will contain only pairs with very  high
              probability.   The  default value is gamma=1.  Using -MEA
              implies -p for computing the pair probabilities.</text>
        </comment>
        </parameter>
        <parameter>
          <name>temperature</name>
          <prompt lang="en">Rescale energy parameters to a temperature of temp C. (-T)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>37</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef)? " -T $value" : ""</code>
            <code proglang="python">( "" , " -T " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>tetraloops</name>
          <prompt lang="en">Do not include special stabilizing energies for certain tetraloops (-4)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -4" : ""</code>
            <code proglang="python">( "" , " -4" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>dangling</name>
          <prompt lang="en">How to treat dangling end energies for bases adjacent to helices in free ends and multiloops (-d)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>-d1</value>
          </vdef>
          <vlist>
            <velem>
              <value>-d1</value>
              <label>Only unpaired bases can participate in at most one dangling end (-d1)</label>
            </velem>
            <velem>
              <value>-d</value>
              <label>Ignores dangling ends altogether (-d)</label>
            </velem>
            <velem>
              <value>-d2</value>
              <label>The check is ignored, this is the default for partition function folding (-d2)</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef)? " $value" : ""</code>
            <code proglang="python">( "" , " " + str(value) )[ value is not None and value != vdef]</code>
          </format>
          <comment>
            <text lang="en">How to treat 'dangling end' energies for bases adjacent to helices in free ends and multiloops: Normally only unpaired bases can participate in at most one dangling end. With -d2 this check is ignored, this is the default for partition function folding (-p). -d ignores dangling ends altogether. Note that by default pf and mfe folding treat dangling ends differently, use -d2 (or -d) in addition to -p to ensure that both algorithms use the same energy model.</text>
          </comment>
        </parameter>
        <parameter>
          <name>scale</name>
          <prompt lang="en">Use scale*mfe as an estimate for the free energy (-S)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value)? " -S $value" : ""</code>
            <code proglang="python">( "" , " -S " + str(value) )[ value is not None ]</code>
          </format>
          <comment>
            <text lang="en">In the calculation of the pf use scale*mfe as an estimate for the ensemble free energy (used to avoid overflows). The default is 1.07, usefull values are 1.0 to 1.2. Occasionally needed for long sequences. You can also recompile the programm to use double precision.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>input</name>
      <prompt lang="en">Input parameters</prompt>
      <argpos>2</argpos>
      <parameters>
        <parameter>
          <name>constraints</name>
          <prompt lang="en">Calculate structures subject to constraints (-C)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -C" : ""</code>
            <code proglang="python">( "" , " -C" )[ value ]</code>
          </format>
          <comment>
            <text lang="en">The programm reads first the sequence then the a string containg constraints on the structure encoded with the symbols: </text>
            <text lang="en">| (the corresponding base has to be paired x (the base is unpaired)</text>
            <text lang="en">&lt; (base i is paired with a base j&gt;i)</text>
            <text lang="en">&gt; (base i is paired with a base j&lt;i)</text>
            <text lang="en">matching brackets ( ) (base i pairs base j)</text>
            <text lang="en">Pf folding ignores constraints of type '|' '&lt;' and '&gt;', but disallow all pairs conflicting with a constraint of type 'x' or '( )'. This is usually sufficient to enforce the constraint.</text>
          </comment>
        </parameter>
        <parameter>
          <name>noLP</name>
          <prompt lang="en">Avoid lonely pairs (helices of length 1) (-noLP)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -noLP" : ""</code>
            <code proglang="python">( "" , " -noLP" )[ value ]</code>
          </format>
          <comment>
            <text lang="en">Produce structures without lonely pairs (helices of length 1). For partition function folding this only disallows pairs that can only occur isolated. Other pairs may still occasionally occur as helices of length 1.</text>
          </comment>
        </parameter>
        <parameter>
          <name>noGU</name>
          <prompt lang="en">Do not allow GU pairs (-noGU)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -noGU" : ""</code>
            <code proglang="python">( "" , " -noGU" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>noCloseGU</name>
          <prompt lang="en">Do not allow GU pairs at the end of helices (-noCloseGU)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -noCloseGU" : ""</code>
            <code proglang="python">( "" , " -noCloseGU" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>nsp</name>
          <prompt lang="en">Non standard pairs (comma seperated list) (-nsp)</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value)? " -nsp $value" : "" </code>
            <code proglang="python">( ""  , " -nsp " + str(value) )[ value is not None ]</code>
          </format>
          <comment>
            <text lang="en">Allow other pairs in addition to the usual AU, GC and GU pairs. Pairs is a comma seperated list of additionally allowed pairs. If a the first character is a '-' then AB will imply that AB and BA are allowed pairs. e.g. RNAfold -nsp -GA will allow GA and AG pairs. Nonstandard pairs are given 0 stacking energy.</text>
          </comment>
        </parameter>
        <parameter>
          <name>parameter</name>
          <prompt lang="en">Energy parameter file (-P)</prompt>
          <type>
            <datatype>
              <class>EnergyParameter</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value)? " -P $value" : ""</code>
            <code proglang="python">( "" , " -P " + str(value) )[ value is not None ]</code>
          </format>
          <comment>
            <text lang="en">Read energy parameters from paramfile, instead of using the default parameter set.</text>
          </comment>
        </parameter>
        <!-- core dumped on raclette
	<parameter>
	  <name>energy</name>
	  <prompt lang="en">Energy parameters for the artificial ABCD... alphabet (-e)</prompt>
	  <type>
	    <datatype>
	      <class>Choice</class>
	    </datatype>
	  </type>
	  <vdef>
	    <value>Null</value>
	  </vdef>
	  <vlist>
	    <velem undef="1">
	      <value>Null</value>
	      <label>No energy for the artificial ABCD</label>
	    </velem>
	    <velem>
	      <value>1</value>
	      <label>Use energy parameters for GC pairs (1)</label>
	    </velem>
	    <velem>
	      <value>2</value>
	      <label>Use energy parameters for AU pairs (2)</label>
	    </velem>
	  </vlist>
	  <format>
	    <code proglang="perl">(defined $value and $value != $vdef)? " -e $value" : ""</code>
	    <code proglang="python">( "" , " -e " + str(value) )[ value is not None and value!=vdef]</code>
	  </format>
	</parameter>
-->
      </parameters>
    </paragraph>
    <paragraph>
      <name>output_options</name>
      <prompt lang="en">Output options</prompt>
      <parameters>
        
        <parameter isout="1">
          <name>outfile_name</name>
          <prompt lang="en">Result file</prompt>
          <type>
            <datatype>
              <class>SequenceWithStructureConstraint</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="perl">"$seqin.tmp"</code>
            <code proglang="python">str(seqin)+'.tmp'</code>
          </filenames>
        </parameter>
        <parameter isstdout="1">
          <name>outfile</name>
          <prompt lang="en">Result file</prompt>
          <type>
            <datatype>
              <class>RnafoldOutput</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="perl">"rnafold.out"</code>
            <code proglang="python">"rnafold.out"</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>
    <parameter ishidden="1">
      <name>readseq</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">not $constraints/</code>
        <code proglang="python">not constraints</code>
      </precond>
      <format>
        <code proglang="perl">"mv $seqin $seqin.ori &amp;&amp; <xi:include href="../../Local/Services/Programs/Env/ViennaRNA_readseq.xml" xpointer="xpointer(/readseq_path/text())"><xi:fallback/></xi:include>readseq -f=19 -a $seqin.ori &gt; $seqin  &amp;&amp; "</code>
        <code proglang="python">"mv %s %s.ori &amp;&amp; <xi:include href="../../Local/Services/Programs/Env/ViennaRNA_readseq.xml" xpointer="xpointer(/readseq_path/text())"><xi:fallback/></xi:include>readseq -f=19 -a %s.ori &gt; %s &amp;&amp; " %( seqin , seqin , seqin , seqin )</code>
      </format>
      <argpos>-10</argpos>
    </parameter>
    <parameter isout="1">
      <name>psfiles</name>
      <prompt>Postscript file</prompt>
      <type>
        <datatype>
          <class>PostScript</class>
          <superclass>Binary</superclass>
        </datatype>
      </type>
      <filenames>
        <code proglang="perl">"*.ps"</code>
        <code proglang="python">"*.ps"</code>
      </filenames>
    </parameter>
  </parameters>
</program>

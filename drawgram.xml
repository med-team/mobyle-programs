<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>drawgram</name>
    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="Entities/phylip_package.xml"/>
    <doc>
      <title>drawgram</title>
      <description>
        <text lang="en">Plots a cladogram- or phenogram-like rooted tree</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/phylip/doc/drawgram.html</doclink>
      <comment>
        <text lang="en">DRAWGRAM  interactively   plots  a  cladogram-  or
	  phenogram-like rooted  tree diagram, with  many options including
	  orientation of tree and branches,  style of tree, label sizes and
	  angles, tree depth, margin  sizes, stem lengths, and placement of
	  nodes in the  tree. Particularly if you can  use your computer to
	  preview the plot, you can  very effectively adjust the details of
	  the plotting to get just the kind of plot you want.</text>
      </comment>
    </doc>
    <category>phylogeny:display</category>
    <category>display:tree</category>
  </head>
  <parameters>
    <parameter iscommand="1" ishidden="1">
      <name>drawgram</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"drawgram &lt;drawgram.params"</code>
        <code proglang="python">"drawgram &lt;drawgram.params"</code>
      </format>
      <argpos>0</argpos>
    </parameter>
    <parameter ismandatory="1" issimple="1" ismaininput="1">
      <name>treefile</name>
      <prompt lang="en">Tree File (intree)</prompt>
      <type>
        <datatype>
          <class>Tree</class>
        </datatype>
        <dataFormat>NEWICK</dataFormat>
      </type>
      <format>
        <code proglang="perl">"ln -s $treefile intree &amp;&amp; "</code>
        <code proglang="python">"ln -s "+ str( treefile ) +" intree &amp;&amp; "</code>
      </format>
      <argpos>-10</argpos>
      <comment>
        <text lang="en">Tree in Newick format.</text>
      </comment>
      <example>
(A,(B,(H,(D,(J,(((G,E),(F,I)),C))))));
      </example>
    </parameter>
    <parameter ishidden="1">
      <name>screen_type</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"0\\n"</code>
        <code proglang="python">"0\n"</code>
      </format>
      <argpos>-1</argpos>
      <paramfile>drawgram.params</paramfile>
    </parameter>
    <paragraph>
      <name>options</name>
      <prompt lang="en">Drawgram options</prompt>
      <parameters>
        <parameter>
          <name>plotter</name>
          <prompt lang="en">Which plotter or printer will the tree be drawn on (P)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>L</value>
          </vdef>
          <vlist>
            <velem>
              <value>L</value>
              <label>Postscript printer file format (L)</label>
            </velem>
            <velem>
              <value>M</value>
              <label>PICT format (M)</label>
            </velem>
            <velem>
              <value>J</value>
              <label>HP Laserjet PCL file format (J)</label>
            </velem>
            <velem>
              <value>W</value>
              <label>MS-Windows Bitmap (W)</label>
            </velem>
            <velem>
              <value>K</value>
              <label>TeKtronix 4010 graphics terminal (K)</label>
            </velem>
            <velem>
              <value>H</value>
              <label>Hewlett-Packard pen plotter (H)</label>
            </velem>
            <velem>
              <value>D</value>
              <label>DEC ReGIS graphics (D)</label>
            </velem>
            <velem>
              <value>B</value>
              <label>Houston Instruments plotter (B)</label>
            </velem>
            <velem>
              <value>E</value>
              <label>Epson MX-80 dot-matrix printer (E)</label>
            </velem>
            <velem>
              <value>C</value>
              <label>Prowriter/Imagewriter dot-matrix printer (C)</label>
            </velem>
            <velem>
              <value>O</value>
              <label>Okidata dot-matrix printer (O)</label>
            </velem>
            <velem>
              <value>T</value>
              <label>Toshiba 24-pin dot-matrix printer (T)</label>
            </velem>
            <velem>
              <value>P</value>
              <label>PCX file format (P)</label>
            </velem>
            <velem>
              <value>X</value>
              <label>X Bitmap format (X)</label>
            </velem>
            <velem>
              <value>F</value>
              <label>FIG 2.0 format (F)</label>
            </velem>
            <velem>
              <value>A</value>
              <label>Idraw drawing program format (A)</label>
            </velem>
            <velem>
              <value>Z</value>
              <label>VRML Virtual Reality Markup Language file (Z)</label>
            </velem>
            <velem>
              <value>V</value>
              <label>POVRAY 3D rendering program file (V)</label>
            </velem>
            <velem>
              <value>R</value>
              <label>Rayshade 3D rendering program file (R)</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? "P\\n$value\\n" : ""</code>
            <code proglang="python">("" , "P\n" + str(value) +"\n")[ value is not None and value != vdef ]</code>
          </format>
          <argpos>2</argpos>
          <paramfile>drawgram.params</paramfile>
        </parameter>
        <paragraph>
          <name>xbitmap_options</name>
          <prompt lang="en">Bitmap options</prompt>
          <precond>
            <code proglang="perl">$plotter =~ /^[XW]$/</code>
            <code proglang="python">plotter in [ "X" , "W" ]</code>
          </precond>
          <parameters>
            <parameter ismandatory="1">
              <name>xres</name>
              <prompt lang="en">X resolution (in pixels)</prompt>
              <type>
                <datatype>
                  <class>Integer</class>
                </datatype>
              </type>
              <vdef>
                <value>500</value>
              </vdef>
              <format>
                <code proglang="perl">"$value\\n"</code>
                <code proglang="python">str( value ) + "\n"</code>
              </format>
              <ctrl>
                <message>
                  <text lang="en">X resolution cannot exceed 2500 pixels</text>
                </message>
                <code proglang="perl">$value &lt;= 2500</code>
                <code proglang="python">value &lt;= 2500</code>
              </ctrl>
              <argpos>3</argpos>
              <paramfile>drawgram.params</paramfile>
            </parameter>
            <parameter ismandatory="1">
              <name>yres</name>
              <prompt lang="en">Y resolution (in pixels)</prompt>
              <type>
                <datatype>
                  <class>Integer</class>
                </datatype>
              </type>
              <vdef>
                <value>500</value>
              </vdef>
              <format>
                <code proglang="perl">"$value\\n"</code>
                <code proglang="python">str( value ) + "\n"</code>
              </format>
              <ctrl>
                <message>
                  <text lang="en">Y resolution cannot exceed 2500 pixels</text>
                </message>
                <code proglang="perl">$value &lt;= 2500</code>
                <code proglang="python">value &lt;= 2500</code>
              </ctrl>
              <argpos>4</argpos>
              <paramfile>drawgram.params</paramfile>
            </parameter>
          </parameters>
        </paragraph>
        <paragraph>
          <name>laserjet_options</name>
          <prompt lang="en">Laserjet options</prompt>
          <precond>
            <code proglang="perl">$plotter eq "J"</code>
            <code proglang="python">plotter == "J"</code>
          </precond>
          <parameters>
            <parameter ismandatory="1">
              <name>laserjet_resolution</name>
              <prompt lang="en">Laserjet resolution</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>3</value>
              </vdef>
              <vlist>
                <velem>
                  <value>1</value>
                  <label>75 DPI (1)</label>
                </velem>
                <velem>
                  <value>2</value>
                  <label>150 DPI (2)</label>
                </velem>
                <velem>
                  <value>3</value>
                  <label>300 DPI (3)</label>
                </velem>
              </vlist>
              <format>
                <code proglang="perl">"$value\\n"</code>
                <code proglang="python">str(value) +"\n"</code>
              </format>
              <argpos>3</argpos>
              <paramfile>drawgram.params</paramfile>
            </parameter>
          </parameters>
        </paragraph>
        <paragraph>
          <name>pcx_options</name>
          <prompt lang="en">Paintbrush options</prompt>
          <precond>
            <code proglang="perl">$plotter eq "P"</code>
            <code proglang="python">plotter == "P"</code>
          </precond>
          <parameters>
            <parameter ismandatory="1">
              <name>pcx_resolution</name>
              <prompt lang="en">Paintbrush PCX resolution</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>3</value>
              </vdef>
              <vlist>
                <velem>
                  <value>1</value>
                  <label>EGA 640 X 350 (1)</label>
                </velem>
                <velem>
                  <value>2</value>
                  <label>VGA 800 X 600 (2)</label>
                </velem>
                <velem>
                  <value>3</value>
                  <label>VGA 1024 X 768 (3)</label>
                </velem>
              </vlist>
              <format>
                <code proglang="perl">"$value\\n"</code>
                <code proglang="python">str(value) + "\n"</code>
              </format>
              <argpos>3</argpos>
              <paramfile>drawgram.params</paramfile>
            </parameter>
          </parameters>
        </paragraph>
        <paragraph>
          <name>ps_options</name>
          <prompt lang="en">PostScript options</prompt>
          <precond>
            <code proglang="perl">$plotter eq "L"</code>
            <code proglang="python">plotter == "L"</code>
          </precond>
          <parameters>
            <parameter>
              <name>font</name>
              <prompt lang="en">Font (F)</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>Times-Roman</value>
              </vdef>
              <vlist>
                <velem>
                  <value>Courier</value>
                  <label>Courier</label>
                </velem>
                <velem>
                  <value>Helvetica</value>
                  <label>Helvetica</label>
                </velem>
                <velem>
                  <value>Helvetica-Bold</value>
                  <label>Helvetica-Bold</label>
                </velem>
                <velem>
                  <value>Helvetica-BoldOblique</value>
                  <label>Helvetica-BoldOblique</label>
                </velem>
                <velem>
                  <value>Helvetica-Oblique</value>
                  <label>Helvetica-Oblique</label>
                </velem>
                <velem>
                  <value>Hershey</value>
                  <label>Hershey</label>
                </velem>
                <velem>
                  <value>Times</value>
                  <label>Times</label>
                </velem>
                <velem>
                  <value>Times-Bold</value>
                  <label>Times-Bold</label>
                </velem>
                <velem>
                  <value>Times-BoldItalic</value>
                  <label>Times-BoldItalic</label>
                </velem>
                <velem>
                  <value>Times-Italic</value>
                  <label>Times-Italic</label>
                </velem>
                <velem>
                  <value>Times-Roman</value>
                  <label>Times-Roman</label>
                </velem>
              </vlist>
              <format>
                <code proglang="perl">(defined $value and $value ne $vdef) ? "F\\n$value\\n" : ""</code>
                <code proglang="python">("", "F\n"+str(value)+"\n")[value is not None and value != vdef]</code>
              </format>
              <argpos>5</argpos>
              <paramfile>drawgram.params</paramfile>
            </parameter>
          </parameters>
        </paragraph>
        <paragraph>
          <name>pov_options</name>
          <prompt lang="en">POVRAY options</prompt>
          <precond>
            <code proglang="perl">$plotter eq "V"</code>
            <code proglang="python">plotter == "V"</code>
          </precond>
          <parameters>
            <parameter ishidden="1">
              <name>pov_validate</name>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <format>
                <code proglang="perl">"Y\\n"</code>
                <code proglang="python">"Y\n"</code>
              </format>
              <argpos>2000</argpos>
              <paramfile>drawgram.params</paramfile>
            </parameter>
          </parameters>
        </paragraph>
        <paragraph>
          <name>vrml_options</name>
          <prompt lang="en">VRML options</prompt>
          <precond>
            <code proglang="perl">$plotter eq "Z"</code>
            <code proglang="python">plotter == "Z"</code>
          </precond>
          <parameters>
            <parameter ishidden="1">
              <name>vrml_validate</name>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <format>
                <code proglang="perl">"Y\\n"</code>
                <code proglang="python">"Y\n"</code>
              </format>
              <argpos>2000</argpos>
              <paramfile>drawgram.params</paramfile>
            </parameter>
          </parameters>
        </paragraph>
        <paragraph>
          <name>ray_options</name>
          <prompt lang="en">Rayshade options</prompt>
          <precond>
            <code proglang="perl">$plotter eq "R"</code>
            <code proglang="python">plotter == "R"</code>
          </precond>
          <parameters>
            <parameter ishidden="1">
              <name>ray_validate</name>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <format>
                <code proglang="perl">"Y\\n"</code>
                <code proglang="python">"Y\n"</code>
              </format>
              <argpos>2000</argpos>
              <paramfile>drawgram.params</paramfile>
            </parameter>
          </parameters>
        </paragraph>
        <parameter ishidden="1">
          <name>screen</name>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">"V\\nN\\n"</code>
            <code proglang="python">"V\nN\n"</code>
          </format>
          <argpos>1</argpos>
          <paramfile>drawgram.params</paramfile>
        </parameter>
        <parameter>
          <name>grows</name>
          <prompt lang="en">Tree grows... (H)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>Horizontally</value>
          </vdef>
          <vlist>
            <velem>
              <value>Vertically</value>
              <label>Vertically</label>
            </velem>
            <velem>
              <value>Horizontally</value>
              <label>Horizontally</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? "H\\n" : ""</code>
            <code proglang="python">( "" , "H\n" )[ value is not None and value != vdef ]</code>
          </format>
          <argpos>5</argpos>
          <paramfile>drawgram.params</paramfile>
        </parameter>
        <parameter>
          <name>tree_style</name>
          <prompt lang="en">Tree style (S)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>C</value>
          </vdef>
          <flist>
            <felem>
              <value>P</value>
              <label>Phenogram (P)</label>
              <code proglang="perl">"S\\nP\\n"</code>
              <code proglang="python">"S\nP\n"</code>
            </felem>
            <felem>
              <value>C</value>
              <label>Cladogram (C)</label>
              <code proglang="perl">""</code>
              <code proglang="python">""</code>
            </felem>
            <felem>
              <value>S</value>
              <label>Swoopogram (S)</label>
              <code proglang="perl">"S\\nS\\n"</code>
              <code proglang="python">"S\nS\n"</code>
            </felem>
            <felem>
              <value>E</value>
              <label>Eurogram (E)</label>
              <code proglang="perl">"S\\nE\\n"</code>
              <code proglang="python">"S\nE\n"</code>
            </felem>
            <felem>
              <value>V</value>
              <label>Curvogram (V)</label>
              <code proglang="perl">"S\\nV\\n"</code>
              <code proglang="python">"S\nV\n"</code>
            </felem>
            <felem>
              <value>O</value>
              <label>Circular tree (O)</label>
              <code proglang="perl">"S\\nO\\n"</code>
              <code proglang="python">"S\nO\n"</code>
            </felem>
          </flist>
          <argpos>5</argpos>
          <paramfile>drawgram.params</paramfile>
        </parameter>
        <parameter>
          <name>branch_lengths</name>
          <prompt lang="en">Use branch lengths (B)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? "" : "B\\n"</code>
            <code proglang="python">("B\n" , "")[ value ]</code>
          </format>
          <argpos>5</argpos>
          <paramfile>drawgram.params</paramfile>
        </parameter>
        <parameter>
          <name>horizontal_margins</name>
          <prompt lang="en">Horizontal margins (M)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>1.65</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? "M\\n$value\\n$vertical_margins\\n" : ""</code>
            <code proglang="python">("" , "M\n" + str( value ) + "\n" + str( vertical_margins ) + "\n")[ value is not None and value != vdef ]</code>
          </format>
          <argpos>10</argpos>
          <paramfile>drawgram.params</paramfile>
        </parameter>
        <parameter>
          <name>vertical_margins</name>
          <prompt lang="en">Vertical margins</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>2.16</value>
          </vdef>
          <format>
            <code proglang="perl">""</code>
            <code proglang="python">""</code>
          </format>
          <argpos>9</argpos>
          <paramfile>drawgram.params</paramfile>
        </parameter>
        <parameter>
          <name>scale</name>
          <prompt lang="en">Scale of branch length (R)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? "R\\n$value\\n" : ""</code>
            <code proglang="python">("" , "R\n" +str( value ) +"\n")[ value is not None ]</code>
          </format>
          <argpos>5</argpos>
          <comment>
            <text lang="en">Default value: Automatically rescaled</text>
          </comment>
          <paramfile>drawgram.params</paramfile>
        </parameter>
        <parameter>
          <name>depth</name>
          <prompt lang="en">Depth/Breadth of tree (D)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>1.00</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? "D\\n$value\\n" : ""</code>
            <code proglang="python">( "" , "D\n" + str( value ) + "\n" )[ value is not None and value != vdef ]</code>
          </format>
          <argpos>5</argpos>
          <paramfile>drawgram.params</paramfile>
        </parameter>
        <parameter>
          <name>stem</name>
          <prompt lang="en">Stem-length/tree-depth (T)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.05</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? "T\\n$value\\n" : ""</code>
            <code proglang="python">("" , "T\n" + str( value ) + "\n")[ value is not None and value != vdef ]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">You should enter a value between 0.0 and 1.0.</text>
            </message>
            <code proglang="perl">$value &gt;= 0.0 and $value &lt; 1.0</code>
            <code proglang="python">value &gt;= 0.0 and value &lt; 1.0</code>
          </ctrl>
          <argpos>5</argpos>
          <comment>
            <text lang="en">Enter the stem length as fraction of tree depth (a value between 0.0 and 1.0).</text>
          </comment>
          <paramfile>drawgram.params</paramfile>
        </parameter>
        <parameter>
          <name>character_height</name>
          <prompt lang="en">Character ht / tip space (C)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.3333</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? "C\\n$value\\n" : ""</code>
            <code proglang="python">("" , "C\n" + str( value ) +"\n")[ value is not None and value != vdef ]</code>
          </format>
          <argpos>5</argpos>
          <comment>
            <text lang="en">Enter character height as fraction of tip spacing.</text>
          </comment>
          <paramfile>drawgram.params</paramfile>
        </parameter>
        <parameter>
          <name>ancestral</name>
          <prompt lang="en">Ancestral nodes (A)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>I</value>
          </vdef>
          <vlist>
            <velem>
              <value>I</value>
              <label>Intermediate (I)</label>
            </velem>
            <velem>
              <value>W</value>
              <label>Weighted (W)</label>
            </velem>
            <velem>
              <value>C</value>
              <label>Centered (C)</label>
            </velem>
            <velem>
              <value>N</value>
              <label>Inner (N)</label>
            </velem>
            <velem>
              <value>V</value>
              <label>So tree is V-shaped (V)</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? "A\\n$value\\n" : ""</code>
            <code proglang="python">("" , "A\n" + str( value )+ "\n")[ value is not None and value != vdef ]</code>
          </format>
          <argpos>5</argpos>
          <paramfile>drawgram.params</paramfile>
        </parameter>
      </parameters>
    </paragraph>
    <parameter isout="1">
      <name>plotfile</name>
      <prompt lang="en">Graphic tree file</prompt>
      <type>
        <datatype>
          <class>Picture</class>
          <superclass>Binary</superclass>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$plotter !~ /^[LMWX]$/</code>
        <code proglang="python">plotter not in [ "L" , "M" , "W", "X" ]</code>
      </precond>
      <filenames>
        <code proglang="perl">"plotfile"</code>
        <code proglang="python">"plotfile"</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>psfile</name>
      <prompt lang="en">Graphic tree file ( postscript format )</prompt>
      <type>
        <datatype>
          <class>PostScript</class>
          <superclass>Binary</superclass>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$plotter eq "L"</code>
        <code proglang="python">plotter == "L"</code>
      </precond>
      <format>
        <code proglang="perl">" &amp;&amp; ln -s plotfile plotfile.ps"</code>
        <code proglang="python">" &amp;&amp; ln -s plotfile plotfile.ps"</code>
      </format>
      <argpos>10</argpos>
      <filenames>
        <code proglang="perl">plotfile.ps</code>
        <code proglang="python">'plotfile.ps'</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>pictfile</name>
      <prompt lang="en">Graphic tree file ( pict format )</prompt>
      <type>
        <datatype>
          <class>Picture</class>
          <superclass>Binary</superclass>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$plotter eq "M"</code>
        <code proglang="python">plotter == "M"</code>
      </precond>
      <format>
        <code proglang="perl">" &amp;&amp; ln -s plotfile plotfile.pict"</code>
        <code proglang="python">" &amp;&amp; ln -s plotfile plotfile.pict"</code>
      </format>
      <argpos>10</argpos>
      <filenames>
        <code proglang="perl">plotfile.pict</code>
        <code proglang="python">'plotfile.pict'</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>xbmfile</name>
      <prompt lang="en">Graphic tree file ( xbm format )</prompt>
      <type>
        <datatype>
          <class>Picture</class>
          <superclass>Binary</superclass>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$plotter eq "X"</code>
        <code proglang="python">plotter == "X"</code>
      </precond>
      <format>
        <code proglang="perl">" &amp;&amp; ln -s plotfile plotfile.xbm"</code>
        <code proglang="python">" &amp;&amp; ln -s plotfile plotfile.xbm"</code>
      </format>
      <argpos>10</argpos>
      <filenames>
        <code proglang="perl">plotfile.xbm</code>
        <code proglang="python">'plotfile.xbm'</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>bmpfile</name>
      <prompt lang="en">Graphic tree file ( bmp format )</prompt>
      <type>
        <datatype>
          <class>Picture</class>
          <superclass>Binary</superclass>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$plotter eq "W"</code>
        <code proglang="python">plotter == "W"</code>
      </precond>
      <format>
        <code proglang="perl">" &amp;&amp; ln -s plotfile plotfile.bmp"</code>
        <code proglang="python">" &amp;&amp; ln -s plotfile plotfile.bmp"</code>
      </format>
      <argpos>10</argpos>
      <filenames>
        <code proglang="perl">plotfile.bmp</code>
        <code proglang="python">'plotfile.bmp'</code>
      </filenames>
    </parameter>
    <parameter ishidden="1">
      <name>confirm</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"Y\\n"</code>
        <code proglang="python">"Y\n"</code>
      </format>
      <argpos>1000</argpos>
      <paramfile>drawgram.params</paramfile>
    </parameter>
  </parameters>
</program>

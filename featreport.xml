<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>featreport</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>featreport</title>
      <description>
        <text lang="en">Reads and writes a feature table</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/featreport.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:edit</category>
    <command>featreport</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,1</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_features</name>
          <prompt lang="en">features option</prompt>
          <type>
            <datatype>
              <class>Features</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -features=" + str(value))[value is not None]</code>
          </format>
          <argpos>2</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the report file</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>featreport.report</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>3</argpos>
        </parameter>

        <parameter>
          <name>e_rformat_outfile</name>
          <prompt lang="en">Choose the report output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>GFF</value>
          </vdef>
          <vlist>
            <velem>
              <value>DASGFF</value>
              <label>Dasgff</label>
            </velem>
            <velem>
              <value>DBMOTIF</value>
              <label>Dbmotif</label>
            </velem>
            <velem>
              <value>DIFFSEQ</value>
              <label>Diffseq</label>
            </velem>
            <velem>
              <value>EMBL</value>
              <label>Embl</label>
            </velem>
            <velem>
              <value>EXCEL</value>
              <label>Excel</label>
            </velem>
            <velem>
              <value>FEATTABLE</value>
              <label>Feattable</label>
            </velem>
            <velem>
              <value>GENBANK</value>
              <label>Genbank</label>
            </velem>
            <velem>
              <value>GFF</value>
              <label>Gff</label>
            </velem>
            <velem>
              <value>LISTFILE</value>
              <label>Listfile</label>
            </velem>
            <velem>
              <value>MOTIF</value>
              <label>Motif</label>
            </velem>
            <velem>
              <value>NAMETABLE</value>
              <label>Nametable</label>
            </velem>
            <velem>
              <value>CODATA</value>
              <label>Codata</label>
            </velem>
            <velem>
              <value>REGIONS</value>
              <label>Regions</label>
            </velem>
            <velem>
              <value>SEQTABLE</value>
              <label>Seqtable</label>
            </velem>
            <velem>
              <value>SIMPLE</value>
              <label>Simple</label>
            </velem>
            <velem>
              <value>SRS</value>
              <label>Srs</label>
            </velem>
            <velem>
              <value>SWISS</value>
              <label>Swiss</label>
            </velem>
            <velem>
              <value>TABLE</value>
              <label>Table</label>
            </velem>
            <velem>
              <value>TAGSEQ</value>
              <label>Tagseq</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -rformat=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>4</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
            <dataFormat>
              <ref param="e_rformat_outfile">
              </ref>
            </dataFormat>
          </type>
          <precond>
            <code proglang="python">e_rformat_outfile in ['DASGFF', 'DBMOTIF', 'DIFFSEQ', 'EMBL', 'EXCEL', 'FEATTABLE', 'GENBANK', 'GFF', 'LISTFILE', 'MOTIF', 'NAMETABLE', 'CODATA', 'REGIONS', 'SEQTABLE', 'SIMPLE', 'SRS', 'SWISS', 'TABLE', 'TAGSEQ']</code>
          </precond>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>5</argpos>
    </parameter>
  </parameters>
</program>

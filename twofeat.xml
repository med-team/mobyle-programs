<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>twofeat</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>twofeat</title>
      <description>
        <text lang="en">Finds neighbouring pairs of features in sequence(s)</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/twofeat.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:edit:feature_table</category>
    <command>twofeat</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>

        <paragraph>
          <name>e_afeaturesection</name>
          <prompt lang="en">First feature options</prompt>

          <parameters>

            <parameter>
              <name>e_asource</name>
              <prompt lang="en">Source of first feature</prompt>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <format>
                <code proglang="python">("", " -asource=" + str(value))[value is not None]</code>
              </format>
              <argpos>2</argpos>
              <comment>
                <text lang="en">By default any feature source in the feature table is allowed. You can set this to match any feature source you wish  to allow. 
  The source name is usually either the name of the program that  detected the feature or it is the feature table (eg: EMBL) that  the feature came from. 
  The source may be wildcarded by using '*'. 
  If you wish to allow more than one source, separate their names  with the character '|', eg: 
  gene* | embl</text>
              </comment>
            </parameter>

            <parameter issimple="1" ismandatory="1">
              <name>e_atype</name>
              <prompt lang="en">Type of first feature</prompt>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <format>
                <code proglang="python">("", " -atype=" + str(value))[value is not None]</code>
              </format>
              <argpos>3</argpos>
              <comment>
                <text lang="en">By default every feature in the feature table is allowed. You can set this to be any feature type you wish to allow. 
  See http://www.ebi.ac.uk/embl/WebFeat/ for a list of the  EMBL feature types and see Appendix A of the Swissprot user  manual in http://www.expasy.org/sprot/userman.html  for a list of the Swissprot feature types. 
  The type may be wildcarded by using '*'. 
  If you wish to allow more than one type, separate their names  with the character '|', eg: 
  *UTR | intron</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_asense</name>
              <prompt lang="en">Sense of first feature</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <vlist>
                <velem>
                  <value>0</value>
                  <label>Any sense</label>
                </velem>
                <velem>
                  <value>+</value>
                  <label>Forward sense</label>
                </velem>
                <velem>
                  <value>-</value>
                  <label>Reverse sense</label>
                </velem>
              </vlist>
              <format>
                <code proglang="python">("", " -asense=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>4</argpos>
              <comment>
                <text lang="en">By default any feature sense is allowed. You can set this to match the required sense.</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_aminscore</name>
              <prompt lang="en">Minimum score of first feature</prompt>
              <type>
                <datatype>
                  <class>Float</class>
                </datatype>
              </type>
              <vdef>
                <value>0.0</value>
              </vdef>
              <format>
                <code proglang="python">("", " -aminscore=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>5</argpos>
              <comment>
                <text lang="en">If this is greater than or equal to the maximum score, then any score is allowed.</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_amaxscore</name>
              <prompt lang="en">Maximum score of first feature</prompt>
              <type>
                <datatype>
                  <class>Float</class>
                </datatype>
              </type>
              <vdef>
                <value>0.0</value>
              </vdef>
              <format>
                <code proglang="python">("", " -amaxscore=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>6</argpos>
              <comment>
                <text lang="en">If this is less than or equal to the maximum score, then any score is permitted.</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_atag</name>
              <prompt lang="en">Tag of first feature</prompt>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <format>
                <code proglang="python">("", " -atag=" + str(value))[value is not None]</code>
              </format>
              <argpos>7</argpos>
              <comment>
                <text lang="en">Tags are the types of extra values that a feature may have. For example in the EMBL feature table, a 'CDS' type of  feature may have the tags '/codon', '/codon_start', '/db_xref',  '/EC_number', '/evidence', '/exception', '/function', '/gene',  '/label', '/map', '/note', '/number', '/partial', '/product',  '/protein_id', '/pseudo', '/standard_name', '/translation',  '/transl_except', '/transl_table', or '/usedin'. Some of these  tags also have values, for example '/gene' can have the value of  the gene name. 
  By default any feature tag in the feature table is allowed. You  can set this to match any feature tag you wish to allow. 
  The tag may be wildcarded by using '*'. 
  If you wish to allow more than one tag, separate their names  with the character '|', eg: 
  gene | label</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_avalue</name>
              <prompt lang="en">Value of first feature's tags</prompt>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <format>
                <code proglang="python">("", " -avalue=" + str(value))[value is not None]</code>
              </format>
              <argpos>8</argpos>
              <comment>
                <text lang="en">Tag values are the values associated with a feature tag. Tags are the types of extra values that a feature may have. For  example in the EMBL feature table, a 'CDS' type of feature may  have the tags '/codon', '/codon_start', '/db_xref',  '/EC_number', '/evidence', '/exception', '/function', '/gene',  '/label', '/map', '/note', '/number', '/partial', '/product',  '/protein_id', '/pseudo', '/standard_name', '/translation',  '/transl_except', '/transl_table', or '/usedin'. Only some of  these tags can have values, for example '/gene' can have the  value of the gene name. By default any feature tag value in the  feature table is allowed. You can set this to match any feature  tag value you wish to allow. 
  The tag value may be wildcarded by using '*'. 
  If you wish to allow more than one tag value, separate their  names with the character '|', eg: 
  pax* | 10</text>
              </comment>
            </parameter>
          </parameters>
        </paragraph>

        <paragraph>
          <name>e_bfeaturesection</name>
          <prompt lang="en">Second feature options</prompt>

          <parameters>

            <parameter>
              <name>e_bsource</name>
              <prompt lang="en">Source of second feature</prompt>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <format>
                <code proglang="python">("", " -bsource=" + str(value))[value is not None]</code>
              </format>
              <argpos>9</argpos>
              <comment>
                <text lang="en">By default any feature source in the feature table is allowed. You can set this to match any feature source you wish  to allow. 
  The source name is usually either the name of the program that  detected the feature or it is the feature table (eg: EMBL) that  the feature came from. 
  The source may be wildcarded by using '*'. 
  If you wish to allow more than one source, separate their names  with the character '|', eg: 
  gene* | embl</text>
              </comment>
            </parameter>

            <parameter issimple="1" ismandatory="1">
              <name>e_btype</name>
              <prompt lang="en">Type of second feature</prompt>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <format>
                <code proglang="python">("", " -btype=" + str(value))[value is not None]</code>
              </format>
              <argpos>10</argpos>
              <comment>
                <text lang="en">By default every feature in the feature table is allowed. You can set this to be any feature type you wish to allow. 
  See http://www.ebi.ac.uk/embl/WebFeat/ for a list of the  EMBL feature types and see Appendix A of the Swissprot user  manual in http://www.expasy.org/sprot/userman.html  for a list of the Swissprot feature types. 
  The type may be wildcarded by using '*'. 
  If you wish to allow more than one type, separate their names  with the character '|', eg: 
  *UTR | intron</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_bsense</name>
              <prompt lang="en">Sense of second feature</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <vlist>
                <velem>
                  <value>0</value>
                  <label>Any sense</label>
                </velem>
                <velem>
                  <value>+</value>
                  <label>Forward sense</label>
                </velem>
                <velem>
                  <value>-</value>
                  <label>Reverse sense</label>
                </velem>
              </vlist>
              <format>
                <code proglang="python">("", " -bsense=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>11</argpos>
              <comment>
                <text lang="en">By default any feature sense is allowed. You can set this to match the required sense.</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_bminscore</name>
              <prompt lang="en">Minimum score of second feature</prompt>
              <type>
                <datatype>
                  <class>Float</class>
                </datatype>
              </type>
              <vdef>
                <value>0.0</value>
              </vdef>
              <format>
                <code proglang="python">("", " -bminscore=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>12</argpos>
              <comment>
                <text lang="en">If this is greater than or equal to the maximum score, then any score is allowed.</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_bmaxscore</name>
              <prompt lang="en">Maximum score of second feature</prompt>
              <type>
                <datatype>
                  <class>Float</class>
                </datatype>
              </type>
              <vdef>
                <value>0.0</value>
              </vdef>
              <format>
                <code proglang="python">("", " -bmaxscore=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>13</argpos>
              <comment>
                <text lang="en">If this is less than or equal to the maximum score, then any score is permitted.</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_btag</name>
              <prompt lang="en">Tag of second feature</prompt>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <format>
                <code proglang="python">("", " -btag=" + str(value))[value is not None]</code>
              </format>
              <argpos>14</argpos>
              <comment>
                <text lang="en">Tags are the types of extra values that a feature may have. For example in the EMBL feature table, a 'CDS' type of  feature may have the tags '/codon', '/codon_start', '/db_xref',  '/EC_number', '/evidence', '/exception', '/function', '/gene',  '/label', '/map', '/note', '/number', '/partial', '/product',  '/protein_id', '/pseudo', '/standard_name', '/translation',  '/transl_except', '/transl_table', or '/usedin'. Some of these  tags also have values, for example '/gene' can have the value of  the gene name. 
  By default any feature tag in the feature table is allowed. You  can set this to match any feature tag you wish to allow. 
  The tag may be wildcarded by using '*'. 
  If you wish to allow more than one tag, separate their names  with the character '|', eg: 
  gene | label</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_bvalue</name>
              <prompt lang="en">Value of second feature's tags</prompt>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <format>
                <code proglang="python">("", " -bvalue=" + str(value))[value is not None]</code>
              </format>
              <argpos>15</argpos>
              <comment>
                <text lang="en">Tag values are the values associated with a feature tag. Tags are the types of extra values that a feature may have. For  example in the EMBL feature table, a 'CDS' type of feature may  have the tags '/codon', '/codon_start', '/db_xref',  '/EC_number', '/evidence', '/exception', '/function', '/gene',  '/label', '/map', '/note', '/number', '/partial', '/product',  '/protein_id', '/pseudo', '/standard_name', '/translation',  '/transl_except', '/transl_table', or '/usedin'. Only some of  these tags can have values, for example '/gene' can have the  value of the gene name. By default any feature tag value in the  feature table is allowed. You can set this to match any feature  tag value you wish to allow. 
  The tag value may be wildcarded by using '*'. 
  If you wish to allow more than one tag value, separate their  names with the character '|', eg: 
  pax* | 10</text>
              </comment>
            </parameter>
          </parameters>
        </paragraph>

        <paragraph>
          <name>e_featurerelationsection</name>
          <prompt lang="en">Feature relation options</prompt>

          <parameters>

            <parameter>
              <name>e_overlap</name>
              <prompt lang="en">Type of overlap required</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>A</value>
              </vdef>
              <vlist>
                <velem>
                  <value>A</value>
                  <label>Any</label>
                </velem>
                <velem>
                  <value>O</value>
                  <label>Overlap required</label>
                </velem>
                <velem>
                  <value>NO</value>
                  <label>No overlaps are allowed</label>
                </velem>
                <velem>
                  <value>NW</value>
                  <label>Overlap required but not within</label>
                </velem>
                <velem>
                  <value>AW</value>
                  <label>A must be all  within b</label>
                </velem>
                <velem>
                  <value>BW</value>
                  <label>B must be all within a</label>
                </velem>
              </vlist>
              <format>
                <code proglang="python">("", " -overlap=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>16</argpos>
              <comment>
                <text lang="en">This allows you to specify the allowed overlaps of the features A and B. 
  You can allow any or no overlaps, specify that they must or must  not overlap, that one must or must not be wholly enclosed  within another feature.</text>
              </comment>
            </parameter>

            <parameter issimple="1" ismandatory="1">
              <name>e_minrange</name>
              <prompt lang="en">The minimum distance between the features</prompt>
              <type>
                <datatype>
                  <class>Integer</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="python">("", " -minrange=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>17</argpos>
              <comment>
                <text lang="en">If this is greater or equal to 'maxrange', then no min or max range is specified</text>
              </comment>
            </parameter>

            <parameter issimple="1" ismandatory="1">
              <name>e_maxrange</name>
              <prompt lang="en">The maximum distance between the features</prompt>
              <type>
                <datatype>
                  <class>Integer</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="python">("", " -maxrange=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>18</argpos>
              <comment>
                <text lang="en">If this is less than or equal to 'minrange', then no min or max range is specified</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_rangetype</name>
              <prompt lang="en">Positions from which to measure the distance</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>N</value>
              </vdef>
              <vlist>
                <velem>
                  <value>N</value>
                  <label>From nearest ends</label>
                </velem>
                <velem>
                  <value>L</value>
                  <label>From left ends</label>
                </velem>
                <velem>
                  <value>R</value>
                  <label>From right ends</label>
                </velem>
                <velem>
                  <value>F</value>
                  <label>From furthest ends</label>
                </velem>
              </vlist>
              <format>
                <code proglang="python">("", " -rangetype=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>19</argpos>
              <comment>
                <text lang="en">This allows you to specify the positions from which the allowed minimum or maximum distance between the features is  measured</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_sense</name>
              <prompt lang="en">Sense of the features</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>A</value>
              </vdef>
              <vlist>
                <velem>
                  <value>A</value>
                  <label>Any sense</label>
                </velem>
                <velem>
                  <value>S</value>
                  <label>Same sense</label>
                </velem>
                <velem>
                  <value>O</value>
                  <label>Opposite sense</label>
                </velem>
              </vlist>
              <format>
                <code proglang="python">("", " -sense=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>20</argpos>
              <comment>
                <text lang="en">This allows you to specify the required sense that the two features must be on. This is ignored (always 'Any') when  looking at protein sequence features.</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_order</name>
              <prompt lang="en">Order of the features</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>A</value>
              </vdef>
              <vlist>
                <velem>
                  <value>A</value>
                  <label>Any</label>
                </velem>
                <velem>
                  <value>AB</value>
                  <label>Feature a then feature b</label>
                </velem>
                <velem>
                  <value>BA</value>
                  <label>Feature b then feature a</label>
                </velem>
              </vlist>
              <format>
                <code proglang="python">("", " -order=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>21</argpos>
              <comment>
                <text lang="en">This allows you to specify the required order of the two features. The order is measured from the start positions of the  features. This criterion is always applied despite the specified  overlap type required.</text>
              </comment>
            </parameter>
          </parameters>
        </paragraph>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_twoout</name>
          <prompt lang="en">Do you want the two features written out individually</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -twoout")[ bool(value) ]</code>
          </format>
          <argpos>22</argpos>
          <comment>
            <text lang="en">If you set this to be true, then the two features themselves will be written out. If it is left as false, then a  single feature will be written out covering the two features you  found.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_typeout</name>
          <prompt lang="en">Name of the output new feature</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">not e_twoout</code>
          </precond>
          <vdef>
            <value>misc_feature</value>
          </vdef>
          <format>
            <code proglang="python">("", " -typeout=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>23</argpos>
          <comment>
            <text lang="en">If you have specified that the pairs of features that are found should be reported as one feature in the ouput, then you can  specify the 'type' name of the new feature here. By default every  feature in the feature table is allowed. See  http://www.ebi.ac.uk/embl/WebFeat/ for a list of the EMBL  feature types and see Appendix A of the Swissprot user manual in  http://www.expasy.org/sprot/userman.html for a list of the Swissprot  feature types. If you specify an invalid feature type name, then  the default name 'misc_feature' is used.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the report file</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>twofeat.report</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>24</argpos>
        </parameter>

        <parameter>
          <name>e_rformat_outfile</name>
          <prompt lang="en">Choose the report output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>TABLE</value>
          </vdef>
          <vlist>
            <velem>
              <value>DASGFF</value>
              <label>Dasgff</label>
            </velem>
            <velem>
              <value>DBMOTIF</value>
              <label>Dbmotif</label>
            </velem>
            <velem>
              <value>DIFFSEQ</value>
              <label>Diffseq</label>
            </velem>
            <velem>
              <value>EMBL</value>
              <label>Embl</label>
            </velem>
            <velem>
              <value>EXCEL</value>
              <label>Excel</label>
            </velem>
            <velem>
              <value>FEATTABLE</value>
              <label>Feattable</label>
            </velem>
            <velem>
              <value>GENBANK</value>
              <label>Genbank</label>
            </velem>
            <velem>
              <value>GFF</value>
              <label>Gff</label>
            </velem>
            <velem>
              <value>LISTFILE</value>
              <label>Listfile</label>
            </velem>
            <velem>
              <value>MOTIF</value>
              <label>Motif</label>
            </velem>
            <velem>
              <value>NAMETABLE</value>
              <label>Nametable</label>
            </velem>
            <velem>
              <value>CODATA</value>
              <label>Codata</label>
            </velem>
            <velem>
              <value>REGIONS</value>
              <label>Regions</label>
            </velem>
            <velem>
              <value>SEQTABLE</value>
              <label>Seqtable</label>
            </velem>
            <velem>
              <value>SIMPLE</value>
              <label>Simple</label>
            </velem>
            <velem>
              <value>SRS</value>
              <label>Srs</label>
            </velem>
            <velem>
              <value>SWISS</value>
              <label>Swiss</label>
            </velem>
            <velem>
              <value>TABLE</value>
              <label>Table</label>
            </velem>
            <velem>
              <value>TAGSEQ</value>
              <label>Tagseq</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -rformat=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>25</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
            <dataFormat>
              <ref param="e_rformat_outfile">
              </ref>
            </dataFormat>
          </type>
          <precond>
            <code proglang="python">e_rformat_outfile in ['DASGFF', 'DBMOTIF', 'DIFFSEQ', 'EMBL', 'EXCEL', 'FEATTABLE', 'GENBANK', 'GFF', 'LISTFILE', 'MOTIF', 'NAMETABLE', 'CODATA', 'REGIONS', 'SEQTABLE', 'SIMPLE', 'SRS', 'SWISS', 'TABLE', 'TAGSEQ']</code>
          </precond>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>26</argpos>
    </parameter>
  </parameters>
</program>

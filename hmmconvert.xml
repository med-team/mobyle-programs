<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais                                               -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>hmmconvert</name>
    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="Entities/hmmer_package.xml"/>
    <doc>
      <title>HMMCONVERT</title>
      <description>
        <text lang="en">Convert profile HMM file to a HMMER format</text>
      </description>
      <comment>
        <text lang="en">hmmconvert reads an HMM file from oldhmmfile in any HMMER format, and writes it to a new file newhmmfile in a new format. oldhmmfile and newhmmfile must be different files; you can't reliably overwrite the old file. By default, the new HMM file is written in HMMER 2 ASCII format.</text>
      </comment>
    </doc>
    <category>hmm:building</category>
    <command>hmmconvert</command>
  </head>
  <parameters>
    <parameter>
      <name>oldhmmfile</name>
      <prompt lang="en">Old HMM ascii file</prompt>
      <type>
        <datatype>
          <class>HmmProfile</class>
          <superclass>AbstractText</superclass>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">not $oldhmmfile </code>
        <code proglang="python">oldhmmfile is not None</code>
      </precond>
      <format>
        <code proglang="perl">" $oldhmmfile"</code>
        <code proglang="python">" " + str(oldhmmfile)</code>
      </format>
      <ctrl>
        <message>
          <text lang="en">Do not enter ascii and bin files at the same time</text>
        </message>
        <code proglang="perl">not defined $oldbinfile</code>
        <code proglang="python">oldbinfile is None</code>
      </ctrl>
      <argpos>2</argpos>
    </parameter>
    <parameter>
      <name>oldbinfile</name>
      <prompt lang="en">Old HMM binary file</prompt>
      <type>
        <datatype>
          <class>HmmProfileBin</class>
          <superclass>Binary</superclass>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">not $oldhmmfile </code>
        <code proglang="python">oldbinfile is not None</code>
      </precond>
      <format>
        <code proglang="perl">" $oldbinfile"</code>
        <code proglang="python">" " + str(oldbinfile)</code>
      </format>
      <ctrl>
        <message>
          <text lang="en">Do not enter ascii and bin files at the same time</text>
        </message>
        <code proglang="perl">not defined $oldhmmfile</code>
        <code proglang="python">oldhmmfile is None</code>
      </ctrl>
      <argpos>2</argpos>
    </parameter>
    <paragraph>
      <name>advanced</name>
      <prompt lang="en">Advanced options</prompt>
      <argpos>1</argpos>
      <parameters>
        <parameter>
          <name>new_format</name>
          <prompt lang="en">New format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>-a</value>
          </vdef>
          <vlist>
            <velem>
              <value>-a</value>
              <label>HMMER to ASCII file</label>
            </velem>
            <velem>
              <value>-b</value>
              <label>HMMER to binary file</label>
            </velem>
            <velem>
              <value>-2</value>
              <label>output backward compatible HMMER2 ASCII format</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">($value)? " $value":""</code>
            <code proglang="python">" "+str(value)</code>
          </format>
          <argpos>1</argpos>
        </parameter>
        <parameter>
          <name>outfmt</name>
          <prompt lang="en">Choose output legacy 3.x file formats by name</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$new_format ne  '-2'</code>
            <code proglang="python">new_format != '-2'</code>
          </precond>
          <vdef>
            <value>3/b</value>
          </vdef>
          <vlist>
            <velem>
              <value>3/b</value>
              <label>3/b code</label>
            </velem>
            <velem>
              <value>3/a</value>
              <label>3/a code</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">($value ne $vdef)? " --outfmt $value":""</code>
            <code proglang="python">("", " --outfmt "+str(value))[value !=vdef]</code>
          </format>
          <comment>
            <text lang="en">Output in a HMMER3 ASCII text format other then the most current one. Valid
      choices for the value are '3/b' or '3/a'. The current format is '3/b', and this is the
      default. There is a slightly different format '3/a' that was used in some alpha test
      code.</text>
          </comment>
          <argpos>1</argpos>
        </parameter>
      </parameters>
    </paragraph>
    <parameter isout="1">
      <name>result_file</name>
      <prompt lang="en">Hmm profile</prompt>
      <type>
        <datatype>
          <class>HmmProfile</class>
          <superclass>AbstractText</superclass>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$new_format eq '-a' or $new_format eq '-2'</code>
        <code proglang="python">new_format == '-a' or new_format == '-2'</code>
      </precond>
      <format>
        <code proglang="perl">(defined $oldhmmfile)? "&gt; $oldhmmfile.convert": "&gt; $oldbinfile.convert"</code>
        <code proglang="python">("&gt; " + str(oldbinfile) + ".convert", "&gt; " + str(oldhmmfile) + ".convert")[oldhmmfile is not None]</code>
      </format>
      <argpos>3</argpos>
      <filenames>
        <code proglang="perl">(defined $oldhmmfile)? "$oldhmmfile.convert": "$oldbinfile.convert"</code>
        <code proglang="python">(str(oldbinfile) + ".convert" , str(oldhmmfile) + ".convert")[oldhmmfile is not None]</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>result_bin_file</name>
      <prompt lang="en">Hmm profile (binary)</prompt>
      <type>
        <datatype>
          <class>HmmProfileBin</class>
          <superclass>Binary</superclass>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$new_format eq '-b'</code>
        <code proglang="python">new_format == '-b'</code>
      </precond>
      <format>
        <code proglang="perl">(defined $oldhmmfile)? "&gt; $oldhmmfile.bin": "&gt; $oldbinfile.bin"</code>
        <code proglang="python">("&gt; " + str(oldbinfile) + ".bin", "&gt; " + str(oldhmmfile) + ".bin")[oldhmmfile is not None]</code>
      </format>
      <argpos>3</argpos>
      <filenames>
        <code proglang="perl">(defined $oldhmmfile)? "$oldhmmfile.bin":"oldbinfile.bin"</code>
        <code proglang="python">(str(oldbinfile) + ".bin" , str(oldhmmfile) + ".bin")[oldhmmfile is not None]</code>
      </filenames>
    </parameter>
  </parameters>
</program>

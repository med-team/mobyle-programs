<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>wise2</name>
    <version>2.2.0</version>
    <doc>
      <title>WISE2</title>
      <description>
        <text lang="en">Comparisons of protein/DNA sequences</text>
      </description>
      <authors>E. Birney</authors>
      <homepagelink>http://www.ebi.ac.uk/Tools/Wise2/doc_wise2.html</homepagelink>
      <sourcelink>ftp://ftp.ebi.ac.uk/pub/software/unix/wise2/</sourcelink>
    </doc>
    <category>alignment:pairwise</category>
  </head>
  <parameters>
    <parameter ismandatory="1" iscommand="1" issimple="1">
      <name>wise2</name>
      <prompt lang="en">Wise program</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>Null</value>
      </vdef>
      <vlist>
        <velem undef="1">
          <value>Null</value>
          <label>Choose a program</label>
        </velem>
        <velem>
          <value>genewise</value>
          <label>Protein to genomic DNA (genewise)</label>
        </velem>
        <velem>
          <value>estwise</value>
          <label>Protein to cDNA (estwise)</label>
        </velem>
      </vlist>
      <format>
        <code proglang="perl">"$value"</code>
        <code proglang="python">str(value)</code>
      </format>
      <argpos>1</argpos>
    </parameter>
    <paragraph>
      <name>protein_file</name>
      <prompt lang="en">Protein file</prompt>
      <parameters>
        <parameter ismandatory="1" issimple="1">
          <name>protein</name>
          <prompt lang="en">Protein sequence File</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>FASTA</dataFormat>
          </type>
          <precond>
            <code proglang="perl">not $hmmer</code>
            <code proglang="python">hmmer is None</code>
          </precond>
          <format>
            <code proglang="perl">" $value"</code>
            <code proglang="python">" " +str(value)</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">You must  give  a protein  sequence file  in fasta format</text>
            </message>
            <code proglang="perl">not ($hmmer)</code>
            <code proglang="python">not (hmmer)</code>
          </ctrl>
          <argpos>2</argpos>
          <comment>
            <text lang="en">You must  give  a protein  sequence file  in fasta format.</text>
          </comment>
        </parameter>
        <parameter ismandatory="1" issimple="1">
          <name>hmmer</name>
          <prompt lang="en">or Protein HMM File</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>HmmTextProfile</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$protein is not defined </code>
            <code proglang="python">protein is None</code>
          </precond>
          <format>
            <code proglang="perl">" $value"</code>
            <code proglang="python">" " + str(value)</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">You must give  an HMMER file</text>
            </message>
            <code proglang="perl">not ($protein)</code>
            <code proglang="python">not (protein)</code>
          </ctrl>
          <argpos>2</argpos>
          <comment>
            <text lang="en">You must give an HMMER file.</text>
          </comment>
        </parameter>
        <parameter ishidden="1">
          <name>hmmer_command</name>
          <prompt lang="en">HMM command (-hmmer)</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">defined $hmmer</code>
            <code proglang="python">hmmer is not None</code>
          </precond>
          <format>
            <code proglang="perl">" -hmmer"</code>
            <code proglang="python">" -hmmer"</code>
          </format>
          <argpos>4</argpos>
        </parameter>
      </parameters>
    </paragraph>
    <parameter ismandatory="1" issimple="1">
      <name>dna</name>
      <prompt lang="en">DNA sequence File</prompt>
      <type>
        <biotype>DNA</biotype>
        <datatype>
          <class>Sequence</class>
        </datatype>
        <dataFormat>FASTA</dataFormat>
      </type>
      <format>
        <code proglang="perl">" $value" </code>
        <code proglang="python">" " + str(value) </code>
      </format>
      <argpos>3</argpos>
    </parameter>
    <parameter ishidden="1">
      <name>quiet</name>
      <prompt lang="en">Silent mode (-silent -quiet)</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">" -silent -quiet" </code>
        <code proglang="python">" -silent -quiet" </code>
      </format>
      <argpos>100</argpos>
    </parameter>
    <paragraph>
      <name>dna_options</name>
      <prompt lang="en">DNA sequence Options</prompt>
      <argpos>5</argpos>
      <parameters>
        <parameter>
          <name>dna_start</name>
          <prompt lang="en">Start position in dna (-u)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -u $value" : ""</code>
            <code proglang="python">( "" , " -u " + str(value) )[ value is not None ]</code>
          </format>
        </parameter>
        <parameter>
          <name>dna_end</name>
          <prompt lang="en">End position in dna (-v)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -v $value" : ""</code>
            <code proglang="python">( "" , " -v " + str(value) )[ value is not None ]</code>
          </format>
        </parameter>
        <parameter>
          <name>strand</name>
          <prompt lang="en">Strand comparison</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>-tfor</value>
          </vdef>
          <vlist>
            <velem>
              <value>-tfor</value>
              <label>Forward (-tfor)</label>
            </velem>
            <velem>
              <value>-trev</value>
              <label>Reverse (-trev)</label>
            </velem>
            <velem>
              <value>-both</value>
              <label>Both (-both)</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? " $value" : ""</code>
            <code proglang="python">( "" , " " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>tabs</name>
          <prompt lang="en">Report positions as absolute to truncated/reverse sequence (-tabs)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -tabs" : ""</code>
            <code proglang="python">( "" , " -tabs" )[ value ]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>protein_options</name>
      <prompt lang="en">Protein comparison Options</prompt>
      <precond>
        <code proglang="perl">not $hmmer</code>
        <code proglang="python">hmmer is None</code>
      </precond>
      <argpos>6</argpos>
      <parameters>
        <parameter>
          <name>protein_start</name>
          <prompt lang="en">Start position in protein (-s)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -s $value" : ""</code>
            <code proglang="python">( "" , " -s " + str(value) )[ value is not None ]</code>
          </format>
        </parameter>
        <parameter>
          <name>protein_end</name>
          <prompt lang="en">End position in protein (-t)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -t $value" : ""</code>
            <code proglang="python">( "" , " -t " + str(value) )[ value is not None ]</code>
          </format>
        </parameter>
        <parameter>
          <name>gap</name>
          <prompt lang="en">Gap penalty (-g)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>12</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -g $value" : ""</code>
            <code proglang="python">( "" , " -g " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>ext</name>
          <prompt lang="en">Gap extension penalty (-e)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>2</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -e $value" : ""</code>
            <code proglang="python">( "" , " -e " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>matrix</name>
          <prompt lang="en">Comparison matrix (-m)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>BLOSUM62.bla</value>
          </vdef>
          <vlist>
            <velem>
              <value>BLOSUM30.bla</value>
              <label>BLOSUM30</label>
            </velem>
            <velem>
              <value>BLOSUM45.bla</value>
              <label>BLOSUM45</label>
            </velem>
            <velem>
              <value>BLOSUM62.bla</value>
              <label>BLOSUM62</label>
            </velem>
            <velem>
              <value>BLOSUM80.bla</value>
              <label>BLOSUM80</label>
            </velem>
            <velem>
              <value>gon120.bla</value>
              <label>GONNET120</label>
            </velem>
            <velem>
              <value>gon160.bla</value>
              <label>GONNET160</label>
            </velem>
            <velem>
              <value>gon200.bla</value>
              <label>GONNET200</label>
            </velem>
            <velem>
              <value>gon250.bla</value>
              <label>GONNET250</label>
            </velem>
            <velem>
              <value>gon350.bla</value>
              <label>GONNET350</label>
            </velem>
            <velem>
              <value>idenity.bla</value>
              <label>Identity</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? " -m $value" : ""</code>
            <code proglang="python">( "" , " -m" + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>gene_model_options</name>
      <prompt lang="en">Model Options</prompt>
      <argpos>7</argpos>
      <parameters>
        <parameter>
          <name>init</name>
          <prompt lang="en">Type of match (-init)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>default</value>
          </vdef>
          <vlist>
            <velem>
              <value>default</value>
              <label>Default</label>
            </velem>
            <velem>
              <value>global</value>
              <label>Global</label>
            </velem>
            <velem>
              <value>local</value>
              <label>Local</label>
            </velem>
            <velem>
              <value>wing</value>
              <label>Wing</label>
            </velem>
            <velem>
              <value>endbias</value>
              <label>Endbias</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? " -init $value" : ""</code>
            <code proglang="python">( "" , " -init " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>subs</name>
          <prompt lang="en">Substitution error rate (-subs)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>1e-5</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -subs $value" : ""</code>
            <code proglang="python">( "" , " -subs " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>indel</name>
          <prompt lang="en">Insertion/deletion error rate (-indel)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>1e-5</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -indel $value" : ""</code>
            <code proglang="python">( "" , " -indel " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>null</name>
          <prompt lang="en">Random Model as synchronous or flat (-null)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>syn</value>
          </vdef>
          <vlist>
            <velem>
              <value>syn</value>
              <label>Synchronous</label>
            </velem>
            <velem>
              <value>flat</value>
              <label>Flat</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? " -null $value" : ""</code>
            <code proglang="python">( "" , " -null " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>alln</name>
          <prompt lang="en">Probability of matching a NNN codon (-alln)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>1.0</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -alln $value" : ""</code>
            <code proglang="python">( "" , " -alln " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <paragraph>
          <name>wise2_model_opt</name>
          <prompt lang="en">Genewise special option</prompt>
          <precond>
            <code proglang="perl">$wise2 eq "genewise"</code>
            <code proglang="python">wise2 == "genewise"</code>
          </precond>
          <parameters>
            <parameter>
              <name>gene</name>
              <prompt lang="en">Gene parameter file (-gene)</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>human.gf</value>
              </vdef>
              <vlist>
                <velem>
                  <value>human.gf</value>
                  <label>Human</label>
                </velem>
                <velem>
                  <value>pb.gf</value>
                  <label>Pb</label>
                </velem>
                <velem>
                  <value>pombe.gf</value>
                  <label>Pombe</label>
                </velem>
                <velem>
                  <value>worm.gf</value>
                  <label>Worm</label>
                </velem>
              </vlist>
              <format>
                <code proglang="perl">(defined $value and $value ne $vdef) ? " -gene $value" : ""</code>
                <code proglang="python">( "" , " -gene " + str(value) )[ value is not None and value != vdef]</code>
              </format>
            </parameter>
            <parameter>
              <name>cfreq</name>
              <prompt lang="en">Using codon bias or not (-cfreq)?</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>flat</value>
              </vdef>
              <vlist>
                <velem>
                  <value>model</value>
                  <label>Model</label>
                </velem>
                <velem>
                  <value>flat</value>
                  <label>Flat</label>
                </velem>
              </vlist>
              <format>
                <code proglang="perl">(defined $value and $value ne $vdef) ? " -cfreq $value" : ""</code>
                <code proglang="python">( "" , " -cfreq " + str(value) )[ value is not None and value != vdef]</code>
              </format>
            </parameter>
            <parameter>
              <name>splice</name>
              <prompt lang="en">Using splice model or GT/AG (-splice)?</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>model</value>
              </vdef>
              <vlist>
                <velem>
                  <value>model</value>
                  <label>Model</label>
                </velem>
                <velem>
                  <value>flat</value>
                  <label>Flat</label>
                </velem>
              </vlist>
              <format>
                <code proglang="perl">(defined $value and $value ne $vdef) ? " -splice $value" : ""</code>
                <code proglang="python">( "" , " -splice " + str(value) )[ value is not None and value != vdef]</code>
              </format>
            </parameter>
            <parameter>
              <name>intron</name>
              <prompt lang="en">Use tied model for introns (-intron)</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>tied</value>
              </vdef>
              <vlist>
                <velem>
                  <value>model</value>
                  <label>Model</label>
                </velem>
                <velem>
                  <value>tied</value>
                  <label>Tied</label>
                </velem>
              </vlist>
              <format>
                <code proglang="perl">(defined $value and $value ne $vdef) ? " -intron $value" : ""</code>
                <code proglang="python">( "" , " -intron " + str(value) )[ value is not None and value != vdef]</code>
              </format>
            </parameter>
            <parameter>
              <name>insert</name>
              <prompt lang="en">Protein insert model (-insert)</prompt>
              <type>
                <datatype>
                  <class>Choice</class>
                </datatype>
              </type>
              <vdef>
                <value>flat</value>
              </vdef>
              <vlist>
                <velem>
                  <value>model</value>
                  <label>Model</label>
                </velem>
                <velem>
                  <value>flat</value>
                  <label>Flat</label>
                </velem>
              </vlist>
              <format>
                <code proglang="perl">(defined $value and $value ne $vdef) ? " -insert $value" : ""</code>
                <code proglang="python">( "" , " -insert " + str(value) )[ value is not None and value != vdef]</code>
              </format>
            </parameter>
          </parameters>
        </paragraph>
      </parameters>
    </paragraph>
    <paragraph>
      <name>output_options</name>
      <prompt lang="en">Output Options</prompt>
      <argpos>9</argpos>
      <parameters>
        <parameter>
          <name>pretty</name>
          <prompt lang="en">Show pretty ascii output (-pretty)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -pretty" : ""</code>
            <code proglang="python">( "" , " -pretty" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>para</name>
          <prompt lang="en">Show parameters (-para)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -para" : ""</code>
            <code proglang="python">( "" , " -para" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>sum</name>
          <prompt lang="en">Show summary output (-sum)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -sum" : ""</code>
            <code proglang="python">( "" , " -sum" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>pep</name>
          <prompt lang="en">Show protein translation, splicing frameshifts (-pep)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -pep" : ""</code>
            <code proglang="python">( "" , " -pep" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>alb</name>
          <prompt lang="en">Show logical AlnBlock alignment (-alb)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -alb" : ""</code>
            <code proglang="python">( "" , " -alb" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>pal</name>
          <prompt lang="en">Show raw matrix alignment (-pal)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -pal" : ""</code>
            <code proglang="python">( "" , " -pal" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>block</name>
          <prompt lang="en">Length of main block in pretty output (-block)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>50</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -block $value" : ""</code>
            <code proglang="python">( "" , " -block " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>divide</name>
          <prompt lang="en">Divide string for multiple outputs (-divide)</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -divide \"$value\"" : ""</code>
            <code proglang="python">( "" , " -divide " + str(value) )[ value is not None ]</code>
          </format>
        </parameter>
        <paragraph>
          <name>wise2_out_opt</name>
          <prompt lang="en">Genewise special option</prompt>
          <precond>
            <code proglang="perl">$wise2 eq "genewise"</code>
            <code proglang="python">wise2 == "genewise"</code>
          </precond>
          <parameters>
            <parameter>
              <name>pseudo</name>
              <prompt lang="en">Mark genes with frameshifts as pseudogenes (-pseudo)</prompt>
              <type>
                <datatype>
                  <class>Boolean</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="perl">($value) ? " -pseudo" : ""</code>
                <code proglang="python">( "" , " -pseudo" )[ value ]</code>
              </format>
            </parameter>
            <parameter>
              <name>genes</name>
              <prompt lang="en">Show gene structure (-genes)</prompt>
              <type>
                <datatype>
                  <class>Boolean</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="perl">($value) ? " -genes" : ""</code>
                <code proglang="python">( "" , " -genes" )[ value ]</code>
              </format>
            </parameter>
            <parameter>
              <name>genesf</name>
              <prompt lang="en">Show gene structure with supporting evidence (-genesf)</prompt>
              <type>
                <datatype>
                  <class>Boolean</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="perl">($value) ? " -genesf" : ""</code>
                <code proglang="python">( "" , " -genesf" )[ value ]</code>
              </format>
            </parameter>
            <parameter>
              <name>embl</name>
              <prompt lang="en">Show EMBL feature format with CDS key (-embl)</prompt>
              <type>
                <datatype>
                  <class>Boolean</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="perl">($value) ? " -embl" : ""</code>
                <code proglang="python">( "" , " -embl" )[ value ]</code>
              </format>
            </parameter>
            <parameter>
              <name>diana</name>
              <prompt lang="en">Show EMBL feature format with misc_feature key for diana (-diana)</prompt>
              <type>
                <datatype>
                  <class>Boolean</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="perl">($value) ? " -diana" : ""</code>
                <code proglang="python">( "" , " -diana" )[ value ]</code>
              </format>
            </parameter>
            <parameter>
              <name>cdna</name>
              <prompt lang="en">Show cDNA (-cdna)</prompt>
              <type>
                <datatype>
                  <class>Boolean</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="perl">($value) ? " -cdna" : ""</code>
                <code proglang="python">( "" , " -cdna" )[ value ]</code>
              </format>
            </parameter>
            <parameter>
              <name>trans</name>
              <prompt lang="en">Show protein translation, breaking at frameshifts (-trans)</prompt>
              <type>
                <datatype>
                  <class>Boolean</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="perl">($value) ? " -trans" : ""</code>
                <code proglang="python">( "" , " -trans" )[ value ]</code>
              </format>
            </parameter>
            <parameter>
              <name>ace</name>
              <prompt lang="en">Ace file gene structure (-ace)</prompt>
              <type>
                <datatype>
                  <class>Boolean</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="perl">($value) ? " -ace" : ""</code>
                <code proglang="python">( "" , " -ace" )[ value ]</code>
              </format>
            </parameter>
            <parameter>
              <name>gff</name>
              <prompt lang="en">Gene Feature Format file (-gff)</prompt>
              <type>
                <datatype>
                  <class>Boolean</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="perl">($value) ? " -gff" : ""</code>
                <code proglang="python">( "" , " -gff" )[ value ]</code>
              </format>
            </parameter>
            <parameter>
              <name>gener</name>
              <prompt lang="en">Raw gene structure (-gener)</prompt>
              <type>
                <datatype>
                  <class>Boolean</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="perl">($value) ? " -gener" : ""</code>
                <code proglang="python">( "" , " -gener" )[ value ]</code>
              </format>
            </parameter>
          </parameters>
        </paragraph>
      </parameters>
    </paragraph>
    <paragraph>
      <name>New_gene_options</name>
      <prompt lang="en">New gene model statistics for genewise</prompt>
      <precond>
        <code proglang="perl">$wise2 eq "genewise"</code>
        <code proglang="python">wise2 == "genewise"</code>
      </precond>
      <argpos>10</argpos>
      <parameters>
        <parameter>
          <name>splice_max_collar</name>
          <prompt lang="en">Maximum Bits value for a splice site (-splice_max_collar)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>5.0</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -splice_max_collar $value" : ""</code>
            <code proglang="python">( "" , " -splice_max_collar " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>splice_min_collar</name>
          <prompt lang="en">Minimum Bits value for a splice site (-splice_min_collar)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>-5.0</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -splice_min_collar $value" : ""</code>
            <code proglang="python">( "" , " -splice_min_collar " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>splice_score_offset</name>
          <prompt lang="en">Score offset for splice sites (-splice_score_offset)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>4.5</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -splice_score_offset $value" : ""</code>
            <code proglang="python">( "" , " -splice_score_offset " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>standard_options</name>
      <prompt lang="en">Standard Options</prompt>
      <argpos>11</argpos>
      <parameters>
        <parameter>
          <name>erroroffstd</name>
          <prompt lang="en">No warning messages (-erroroffstd)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -erroroffstd" : ""</code>
            <code proglang="python">( "" , " -erroroffstd" )[ value ]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
  </parameters>
</program>


<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>hmmbuild</name>
    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="Entities/hmmer_package.xml"/>
    <doc>
      <title>HMMBUILD</title>
      <description>
        <text lang="en">Build a profile HMM from an input multiple alignment</text>
      </description>
    </doc>
    <category>hmm:building</category>
    <command>hmmbuild</command>
  </head>
  <parameters>
    <parameter ismandatory="1" issimple="1">
      <name>alignfile</name>
      <prompt lang="en">Aligned sequences File</prompt>
      <type>
        <datatype>
          <class>Alignment</class>
        </datatype>
        <dataFormat>STOCKHOLM</dataFormat>
      </type>
      <format>
        <code proglang="perl">" $value"</code>
        <code proglang="python">" "+str(value)</code>
      </format>
      <argpos>30</argpos>
    </parameter>
    <parameter issimple="1">
      <name>alphabet</name>
      <prompt lang="en">Forcing an alphabet in input alignment</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>null</value>
      </vdef>
      <vlist>
        <velem undef="1">
          <value>null</value>
          <label>Autodetection</label>
        </velem>
        <velem>
          <value>--amino</value>
          <label>Protein</label>
        </velem>
        <velem>
          <value>--dna</value>
          <label>DNA</label>
        </velem>
        <velem>
          <value>--rna</value>
          <label>RNA</label>
        </velem>
      </vlist>
      <format>
        <code proglang="perl">(defined $value and $value ne $vdef)? " $value" : ""</code>
        <code proglang="python">("", " " + str(value) )[ value is not None and value != vdef]</code>
      </format>
      <comment>
        <text lang="en">The alphabet type (amino, DNA, or RNA) is autodetected by default, by looking at the composition of the
            msafile. Autodetection is normally quite reliable, but occasionally alphabet type may be ambiguous and
            autodetection can fail (for instance, on tiny toy alignments of just a few residues). To avoid this, or to
            increase robustness in automated analysis pipelines, you may specify the alphabet type of msafile with
            these options.</text>
        <text lang="en">Protein: Specify that all sequences in seqfile are proteins. By default, alphabet type is
                    autodetected from looking at the residue composition.</text>
        <text lang="en">DNA: Specify that all sequences in seqfile are DNAs.</text>
        <text lang="en">RNA: Specify that all sequences in seqfile are RNAs.</text>
      </comment>
      <argpos>2</argpos>
    </parameter>
    <parameter ishidden="1">
      <name>hmm_textfile</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">" $alignfile.hmm"</code>
        <code proglang="python">" " + str(alignfile) + ".hmm"</code>
      </format>
      <argpos>20</argpos>
    </parameter>
    <paragraph>
      <name>output_options</name>
      <prompt lang="en">Output options</prompt>
      <argpos>1</argpos>
      <parameters>
        <parameter>
          <name>hmmname</name>
          <prompt lang="en">Name the HMM (-n)</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -n $value" : ""</code>
            <code proglang="python">( "" , " -n " + str(value) )[ value is not None ]</code>
          </format>
          <argpos>1</argpos>
          <comment>
            <text lang="en">Name the new profile. The default is to use the name of the alignment (if one
          is present in the msafile, or, failing that, the name of the hmmfile. If msafile contains
          more than one alignment, -n doesn't work, and every alignment must have a name
          annotated in the msafile (as in Stockholm #=GF ID annotation).</text>
          </comment>
        </parameter>
        <!--<parameter>
          <name>save</name>
          <prompt lang="en">Direct summary output to file, not stdout. (-o)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">($value)? " -o $value" : ""</code>
            <code proglang="python">( "" , " -o " + str(value) )[ value is not None ]</code>
          </format>
          <argpos>1</argpos>
         
        </parameter>
        -->
        <parameter>
          <name>re_save</name>
          <prompt lang="en">Re_save annotated, possibly modified MSA to 'file', in Stockholm format. (-O)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value)? " -O $value" : ""</code>
            <code proglang="python">( "" , " -O " + str(value) )[ value is not None ]</code>
          </format>
          <argpos>1</argpos>
          <comment>
            <text lang="en">After each model is constructed, resave annotated, possibly modified source alignments
            to a file in Stockholm format. The alignments are annotated with a
            reference annotation line indicating which columns were assigned as consensus,
            and sequences are annotated with what relative sequence weights were assigned.
            Some residues of the alignment may have been shifted to accommodate restrictions
            of the Plan7 profile architecture, which disallows transitions between insert
            and delete states..
            </text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>AlternativeConstruction</name>
      <prompt lang="en">Alternative model construction strategies</prompt>
      <argpos>1</argpos>
      <comment>
        <text lang="en">These options control how consensus columns are defined in an alignment.</text>
      </comment>
      <parameters>
        <parameter>
          <name>fast</name>
          <prompt lang="en">Quickly and heuristically determine the architecture of the model (fast)
          </prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " --fast" : ""</code>
            <code proglang="python">( "" , " --fast" )[ value ]</code>
          </format>
          <argpos>1</argpos>
          <comment>
            <text lang="en">Define consensus columns as those that have a fraction &gt;= symfrac of residues
            as opposed to gaps. (See the --symfrac option.) This is the default.</text>
          </comment>
        </parameter>
        <parameter>
          <name>symfrac</name>
          <prompt lang="en">Sets sym fraction controlling for the --fast model construction algorithm, (symfrac) </prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$fast</code>
            <code proglang="python">fast </code>
          </precond>
          <vdef>
            <value>0.5</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " --symfrac $value" : ""</code>
            <code proglang="python">( "" , " --symfrac " + str(value) )[ value is not None and value !=vdef ]
            </code>
          </format>
          <comment>
            <text lang="en">Enter a value &gt;= 0 and &lt;= 1. Define the residue fraction threshold necessary 
              to define a consensus column
              when using the --fast option. The default is 0.5. The symbol fraction in each
              column is calculated after taking relative sequence weighting into account, and ignoring
              gap characters corresponding to ends of sequence fragments (as opposed
              to internal insertions/deletions). 
              Setting this to 0.0 means that every alignment column will be assigned
			  as consensus, which may be useful in some cases. Setting it to 1.0
	          means that only columns that have no gap characters at all will be
              assigned as consensus.
              </text>
          </comment>
          <ctrl>
            <message>
              <text lang="en">Enter a value &gt;= 0 and &lt;= 1</text>
            </message>
            <code proglang="perl">0 &lt;= $value &lt;= 1 </code>
            <code proglang="python">0 &lt;= value &lt;= 1 </code>
          </ctrl>
        </parameter>
        <parameter>
          <name>fragthresh</name>
          <prompt lang="en">Tag sequence as a fragment, (fragthresh)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.5</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value  and $value != $vdef) ? " --fragthresh $value" : ""</code>
            <code proglang="python">( "" , " --fragthresh " + str(value) )[ value is not None and value !=vdef ] </code>
          </format>
          <comment>
            <text lang="en">Enter a value &gt;= 0 and &lt;= 1. We only want to count terminal gaps as deletions if the aligned sequence is known
           to be full-length, not if it is a fragment (for instance, because only part of it was
           sequenced). HMMER uses a simple rule to infer fragments: if the sequence length
           L is less than a fraction x times the mean sequence length of all the sequences
           in the alignment, then the sequence is handled as a fragment. The default is 0.5.</text>
          </comment>
          <ctrl>
            <message>
              <text lang="en">Enter a value &gt;= 0 and &lt;= 1</text>
            </message>
            <code proglang="perl">0 &lt;= $value &lt;= 1 </code>
            <code proglang="python">0 &lt;= value &lt;= 1 </code>
          </ctrl>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>advanced</name>
      <prompt lang="en">Advanced options</prompt>
      <argpos>1</argpos>
      <parameters>
        <parameter>
          <name>relativeWeight</name>
          <prompt lang="en">Alternative relative sequence weighting strategies</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>wpb</value>
          </vdef>
          <vlist>
            <velem>
              <value>wpb</value>
              <label>Henikoff position-based weighting scheme (wpb)</label>
            </velem>
            <velem>
              <value>wgsc</value>
              <label>Gerstein/Sonnhammer/Chothia ad hoc sequence weighting algorithm (wgsc)</label>
            </velem>
            <velem>
              <value>wblosum</value>
              <label>BLOSUM filtering algorithm to weight the sequences (wblosum)</label>
            </velem>
            <velem>
              <value>wnone</value>
              <label>Turn off all sequence weighting (wnone)</label>
            </velem>
            <velem>
              <value>infoWgiven</value>
              <label>Personal weights in file. Give file in next parameter</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">($value ne $vdef and $value ne 'infoWgiven') ? " --$value" : ""</code>
            <code proglang="python">( "" , ' --'+ str(value) )[ value != vdef and value != 'infoWgiven' ]</code>
          </format>
          <comment>
            <text lang="en">HMMER uses an ad hoc sequence weighting algorithm to downweight closely related sequences and upweight
          distantly related ones. This has the effect of making models less biased by uneven phylogenetic
          representation. For example, two identical sequences would typically each receive half the weight that one
          sequence would. These options control which algorithm gets used.</text>
            <text lang="en">wnp: Use the Henikoff position-based sequence weighting scheme [Henikoff and Henikoff,
          J. Mol. Biol. 243:574, 1994]. This is the default.</text>
            <text lang="en">wgsc: Use the Gerstein/Sonnhammer/Chothia weighting algorithm [Gerstein et al, J. Mol.
          Biol. 235:1067, 1994].</text>
            <text lang="en">wblosum: Use the same clustering scheme that was used to weight data in calculating BLOSUM
          subsitution matrices [Henikoff and Henikoff, Proc. Natl. Acad. Sci 89:10915,
          1992]. Sequences are single-linkage clustered at an identity threshold (default
          0.62; see --wid) and within each cluster of c sequences, each sequence gets relative
          weight 1/c.</text>
            <text lang="en">wnone: No relative weights. All sequences are assigned uniform weight
            </text>
          </comment>
        </parameter>
        <parameter>
          <name>wgiven</name>
          <prompt lang="en">Personal weights in file </prompt>
          <type>
            <datatype>
              <class>MSAFile</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$relativeWeight eq 'infoWgiven'</code>
            <code proglang="python">relativeWeight == 'infoWgiven'</code>
          </precond>
          <format>
            <code proglang="perl">(defined $value) ? " --wgiven $value" : ""  </code>
            <code proglang="python">("", "--wgiven " + str( value ))[value is not None] </code>
          </format>
        </parameter>
        <parameter>
          <name>wid</name>
          <prompt lang="en">Set identity cutoff for BLOSUM filtering algorithm option (wid)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$wblosum eq 'wblosum' and not $eset</code>
            <code proglang="python">relativeWeight == 'wblosum' and not eset</code>
          </precond>
          <vdef>
            <value>0.62</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " --wid $value" : ""</code>
            <code proglang="python">("", " --wid " + str(value))[value is not None and value != vdef]</code>
          </format>
          <comment>
            <text lang="en">Sets the identity threshold used by single-linkage clustering when using --wblosum.
            Invalid with any other weighting scheme. Default is 0.62. Enter a value &gt;= 0 and &lt;= 1 </text>
          </comment>
          <ctrl>
            <message>
              <text lang="en">Enter a value &gt;= 0 and &lt;= 1</text>
            </message>
            <code proglang="perl">0 &lt;= $value and $value &lt;= 1 </code>
            <code proglang="python">0 &lt;= value and value &lt;= 1 </code>
          </ctrl>
        </parameter>
        <parameter>
          <name>effectiveWeight</name>
          <prompt lang="en">Alternate effective sequence weighting strategies</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">not $eset</code>
            <code proglang="python">not eset</code>
          </precond>
          <vdef>
            <value>--eent</value>
          </vdef>
          <vlist>
            <velem>
              <value>--eent</value>
              <label>Adjust effective sequence weighting to achieve relative entropy target (eent)</label>
            </velem>
            <velem>
              <value>--eclust</value>
              <label>Effective sequence weighting is weighting of single linkage clusters (eclust)</label>
            </velem>
            <velem>
              <value>--enone</value>
              <label>No effective sequence weighting: just use number of sequence (enone)</label>
            </velem>
            <velem>
              <value>turnOff</value>
              <label>Personal setting of effective sequence weighting for all models to value</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">($value ne $vdef and $value ne 'turnOff') ? " $value" : ""</code>
            <code proglang="python">( "" , " " +str(value) )[ value != vdef and value != 'turnOff']</code>
          </format>
          <comment>
            <text lang="en">After relative weights are determined, they are normalized to sum to a total effective sequence number,
eff nseq. This number may be the actual number of sequences in the alignment, but it is almost always
smaller than that. The default entropy weighting method (--eent) reduces the effective sequence number
to reduce the information content (relative entropy, or average expected score on true homologs) per
consensus position. The target relative entropy is controlled by a two-parameter function, where the two
parameters are settable with --ere and --esigma.</text>
            <text lang="en">--eent: Adjust effective sequence number to achieve a specific relative entropy per position
(see --ere). This is the default.</text>
            <text lang="en">--eclust: Set effective sequence number to the number of single-linkage clusters at a specific
identity threshold (see --eid). This option is not recommended; it's for experiments
evaluating how much better --eent is.</text>
            <text lang="en">--enone: Turn off effective sequence number determination and just use the actual number
of sequences. One reason you might want to do this is to try to maximize the
relative entropy/position of your model, which may be useful for short models</text>
          </comment>
        </parameter>
        <parameter>
          <name>eset</name>
          <prompt lang="en">Set personal effective sequence weighting for all models to value (eset)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$effectiveWeight eq 'turnOff'</code>
            <code proglang="python">effectiveWeight == 'turnOff'</code>
          </precond>
          <format>
            <code proglang="perl">(defined $value) ? " --eset $value" : ""</code>
            <code proglang="python">( "" , " --eset " + str(value) )[ value is not None ]</code>
          </format>
          <comment>
            <text lang="en">Explicitly set the effective sequence number for all models to value</text>
          </comment>
        </parameter>
        <parameter>
          <name>ere</name>
          <prompt lang="en">For personal adjustment of effective sequence weighting: set minimum relative entropy/position to value (ere)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$effectiveWeight eq "--eent"</code>
            <code proglang="python">effectiveWeight == "--eent"</code>
          </precond>
          <format>
            <code proglang="perl">(defined $value) ? " --ere $value" : ""</code>
            <code proglang="python">( "" , " --ere " + str(value) )[ value is not None ]</code>
          </format>
          <comment>
            <text lang="en">Set the minimum relative entropy/position target to value. Requires --eent. Default
              depends on the sequence alphabet; for protein sequences, it is 0.59 bits/position.</text>
          </comment>
        </parameter>
        <parameter>
          <name>esigma</name>
          <prompt lang="en">For personal adjustment of effective sequence weighting: set sigma parameter to value (esigma)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$effectiveWeight eq "--eent"</code>
            <code proglang="python">effectiveWeight == "--eent"</code>
          </precond>
          <vdef>
            <value>45.0</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value!=$vdef) ? " --esigma $value" : ""</code>
            <code proglang="python">( "" , " --esigma " + str(value) )[ value is not None and value !=vdef ]</code>
          </format>
          <comment>
            <text lang="en">Sets the minimum relative entropy contributed by an entire model alignment, over
            its whole length. This has the effect of making short models have higher relative
            entropy per position than --ere alone would give. The default is 45.0 bits.</text>
          </comment>
        </parameter>
        <parameter>
          <name>eid</name>
          <prompt lang="en">For single linkage clustering: set fractional identity cutoff to value (eid)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$effectiveWeight eq "--eclust"  and not $eset</code>
            <code proglang="python">effectiveWeight == "--eclust"  and not eset</code>
          </precond>
          <vdef>
            <value>0.62</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value!=$vdef) ? " --eid $value" : ""</code>
            <code proglang="python">( "" , " --eid " + str(value) )[ value is not None and value !=vdef ]</code>
          </format>
          <comment>
            <text lang="en">Enter a value &gt;= 0 and &lt;= 1. Sets the fractional pairwise identity cutoff used 
           by single linkage clustering with the --eclust option. The default is 0.62.</text>
          </comment>
          <ctrl>
            <message>
              <text lang="en">Enter a value &gt;= 0 and &lt;= 1</text>
            </message>
            <code proglang="perl">0 &lt;= $value &lt;= 1 </code>
            <code proglang="python">0 &lt;= value &lt;= 1 </code>
          </ctrl>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>ECalibration</name>
      <prompt lang="en">Control of E-value calibration</prompt>
      <argpos>1</argpos>
      <comment>
        <text lang="en">The location parameters for the expected score distributions for MSV filter scores, 
        Viterbi filter scores, and Forward scores require three short random sequence simulations.</text>
      </comment>
      <parameters>
        <parameter>
          <name>EmL</name>
          <prompt lang="en">Lengt of sequences for MSV Gumbel mu fit (EmL)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>200</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value!=$vdef) ? " --EmL $value" : ""</code>
            <code proglang="python">( "" , " --EmL " + str(value) )[ value is not None and value !=vdef ]</code>
          </format>
          <comment>
            <text lang="en">Enter a value &gt; 0. Sets the sequence length in simulation that estimates the location parameter mu
for MSV filter E-values. Default is 200.</text>
          </comment>
          <ctrl>
            <message>
              <text lang="en">Enter a value &gt; 0 </text>
            </message>
            <code proglang="perl">$value &gt; 0 </code>
            <code proglang="python">value &gt; 0 </code>
          </ctrl>
        </parameter>
        <parameter>
          <name>EmN</name>
          <prompt lang="en">Number of sequences for MSV Gumbel mu fit (EmN)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>200</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value!=$vdef) ? " --EmN $value" : ""</code>
            <code proglang="python">( "" , " --EmN " + str(value) )[ value is not None and value !=vdef ]</code>
          </format>
          <comment>
            <text lang="en">Enter a value &gt; 0. Sets the number of sequences in simulation that estimates the location parameter
mu for MSV filter E-values. Default is 200.</text>
          </comment>
          <ctrl>
            <message>
              <text lang="en">Enter a value &gt; 0. </text>
            </message>
            <code proglang="perl">$value &gt; 0 </code>
            <code proglang="python">value &gt; 0 </code>
          </ctrl>
        </parameter>
        <parameter>
          <name>EvL</name>
          <prompt lang="en">Lengt of sequences for Viterbi Gumbel mu fit (EvL)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>200</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value!=$vdef) ? " --EvL $value" : ""</code>
            <code proglang="python">( "" , " --EvL " + str(value) )[ value is not None and value !=vdef ]</code>
          </format>
          <comment>
            <text lang="en">Enter a value &gt; 0. Sets the sequence length in simulation that estimates the location parameter mu
for Viterbi filter E-values. Default is 200.</text>
          </comment>
          <ctrl>
            <message>
              <text lang="en">Enter a value &gt; 0 </text>
            </message>
            <code proglang="perl">$value &gt; 0 </code>
            <code proglang="python">value &gt; 0 </code>
          </ctrl>
        </parameter>
        <parameter>
          <name>EvN</name>
          <prompt lang="en">Number of sequences for Viterbi Gumbel mu fit (EvN)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>200</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value!=$vdef) ? " --EvN $value" : ""</code>
            <code proglang="python">( "" , " --EvN " + str(value) )[ value is not None and value !=vdef ]</code>
          </format>
          <comment>
            <text lang="en">Enter a value &gt; 0. Sets the number of sequences in simulation that estimates the location parameter
mu for Viterbi filter E-values. Default is 200.</text>
          </comment>
          <ctrl>
            <message>
              <text lang="en">Enter a value &gt; 0.</text>
            </message>
            <code proglang="perl">$value &gt; 0 </code>
            <code proglang="python">value &gt; 0 </code>
          </ctrl>
        </parameter>
        <parameter>
          <name>EfL</name>
          <prompt lang="en">Lengt of sequences for Forward exp tail tau fit (EfL)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>100</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value!=$vdef) ? " --EfL $value" : ""</code>
            <code proglang="python">( "" , " --EfL " + str(value) )[ value is not None and value !=vdef ]</code>
          </format>
          <comment>
            <text lang="en">Enter a value &gt; 0. Sets the sequence length in simulation that estimates the location parameter tau
for Forward E-values. Default is 100.</text>
          </comment>
          <ctrl>
            <message>
              <text lang="en">Enter a value &gt; 0 </text>
            </message>
            <code proglang="perl">$value &gt; 0 </code>
            <code proglang="python">value &gt; 0 </code>
          </ctrl>
        </parameter>
        <parameter>
          <name>EfN</name>
          <prompt lang="en">Number of sequences for Forward exp tail tau fit (EfN)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>200</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value!=$vdef) ? " --EfN $value" : ""</code>
            <code proglang="python">( "" , " --EfN " + str(value) )[ value is not None and value !=vdef ]</code>
          </format>
          <comment>
            <text lang="en">Enter a value &gt; 0. Sets the number of sequences in simulation that estimates the location parameter
tau for Forward E-values. Default is 200.</text>
          </comment>
          <ctrl>
            <message>
              <text lang="en">Enter a value &gt; 0 </text>
            </message>
            <code proglang="perl">$value &gt; 0 </code>
            <code proglang="python">value &gt; 0 </code>
          </ctrl>
        </parameter>
        <parameter>
          <name>Eft</name>
          <prompt lang="en">Tail mass for Forward exponential tail tau fit (Eft)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.04</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value!=$vdef) ? " --Eft $value" : ""</code>
            <code proglang="python">( "" , " --Eft " + str(value) )[ value is not None and value !=vdef ]</code>
          </format>
          <comment>
            <text lang="en">Enter a value &gt; 0 and &lt; 1. Sets the tail mass fraction to fit in the simulation that estimates the location parameter
tau for Forward evalues. Default is 0.04.</text>
          </comment>
          <ctrl>
            <message>
              <text lang="en">Enter a value &gt; 0 and &lt; 1</text>
            </message>
            <code proglang="perl">$value &gt; 0 and $value &lt; 1</code>
            <code proglang="python">value &gt; 0 and value &lt; 1</code>
          </ctrl>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>other</name>
      <prompt lang="en">Other options</prompt>
      <argpos>1</argpos>
      <parameters>
        <parameter>
          <name>seed</name>
          <prompt lang="en">Set random number seed  (seed)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>42</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " --seed $value" : ""</code>
            <code proglang="python">( "" , " --seed " + str(value))[ value is not None and value != vdef ]</code>
          </format>
          <comment>
            <text lang="en">Seed the random number generator with the value, an integer &gt;= 0. If the value is nonzero,
            any stochastic simulations will be reproducible; the same command will give the
            same results. If the number is 0, the random number generator is seeded arbitrarily, and
            stochastic simulations will vary from run to run of the same command. The default
            seed is 42.</text>
          </comment>
        </parameter>
        <parameter>
          <name>laplace</name>
          <prompt lang="en">Use a Laplace +1 prior</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " --laplace" : ""</code>
            <code proglang="python">( "" , " --laplace " )[ value ]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
    <parameter isout="1">
      <name>hmmfile_res</name>
      <prompt lang="en">Hmm profile</prompt>
      <type>
        <datatype>
          <class>HmmProfile</class>
          <superclass>AbstractText</superclass>
        </datatype>
        <dataFormat>HMMER3</dataFormat>
      </type>
      <filenames>
        <code proglang="perl">*.hmm</code>
        <code proglang="python">"*.hmm"</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>re_save_file</name>
      <prompt lang="en">Alignment file</prompt>
      <type>
        <datatype>
          <class>Alignment</class>
        </datatype>
        <dataFormat>STOCKHOLM</dataFormat>
      </type>
      <filenames>
        <code proglang="perl">$re_save</code>
        <code proglang="python">re_save</code>
      </filenames>
    </parameter>
  </parameters>
</program>

<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>makeprotseq</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>makeprotseq</title>
      <description>
        <text lang="en">Create random protein sequences</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/makeprotseq.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:edit</category>
    <command>makeprotseq</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter>
          <name>e_pepstatsfile</name>
          <prompt lang="en">Pepstats program output file (optional)</prompt>
          <type>
            <datatype>
              <class>PepstatsReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -pepstatsfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
          <comment>
            <text lang="en">This file should be a pepstats output file. Protein sequences will be created with the composition in the pepstats  output file.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_required</name>
      <prompt lang="en">Required section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_amount</name>
          <prompt lang="en">Number of sequences created (value greater than or equal to 1)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>100</value>
          </vdef>
          <format>
            <code proglang="python">("", " -amount=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <argpos>2</argpos>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_length</name>
          <prompt lang="en">Length of each sequence (value greater than or equal to 1)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>100</value>
          </vdef>
          <format>
            <code proglang="python">("", " -length=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <argpos>3</argpos>
        </parameter>

        <parameter>
          <name>e_useinsert</name>
          <prompt lang="en">Do you want to make an insert</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -useinsert")[ bool(value) ]</code>
          </format>
          <argpos>4</argpos>
        </parameter>

        <parameter>
          <name>e_insert</name>
          <prompt lang="en">Inserted string</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_useinsert</code>
          </precond>
          <format>
            <code proglang="python">("", " -insert=" + str(value))[value is not None]</code>
          </format>
          <argpos>5</argpos>
          <comment>
            <text lang="en">String that is inserted into sequence</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_start</name>
          <prompt lang="en">Start point of inserted sequence (value greater than or equal to 1)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_useinsert</code>
          </precond>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">("", " -start=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <argpos>6</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outseq</name>
          <prompt lang="en">Name of the output sequence file (e_outseq)</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>makeprotseq.e_outseq</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outseq=" + str(value))[value is not None]</code>
          </format>
          <argpos>7</argpos>
        </parameter>

        <parameter>
          <name>e_osformat_outseq</name>
          <prompt lang="en">Choose the sequence output format</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>FASTA</value>
          </vdef>
          <vlist>
            <velem>
              <value>EMBL</value>
              <label>Embl</label>
            </velem>
            <velem>
              <value>FASTA</value>
              <label>Fasta</label>
            </velem>
            <velem>
              <value>GCG</value>
              <label>Gcg</label>
            </velem>
            <velem>
              <value>GENBANK</value>
              <label>Genbank</label>
            </velem>
            <velem>
              <value>NBRF</value>
              <label>Nbrf</label>
            </velem>
            <velem>
              <value>CODATA</value>
              <label>Codata</label>
            </velem>
            <velem>
              <value>RAW</value>
              <label>Raw</label>
            </velem>
            <velem>
              <value>SWISSPROT</value>
              <label>Swissprot</label>
            </velem>
            <velem>
              <value>GFF</value>
              <label>Gff</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -osformat=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>8</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outseq_out</name>
          <prompt lang="en">outseq_out option</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>
              <ref param="e_osformat_outseq">
              </ref>
            </dataFormat>
          </type>
          <filenames>
            <code proglang="python">e_outseq</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>9</argpos>
    </parameter>
  </parameters>
</program>

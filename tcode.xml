<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>tcode</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>tcode</title>
      <description>
        <text lang="en">Identify protein-coding regions using Fickett TESTCODE statistic</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/tcode.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:nucleic:gene_finding</category>
    <command>tcode</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>

        <parameter>
          <name>e_datafile</name>
          <prompt lang="en">Testcode data file</prompt>
          <type>
            <datatype>
              <class>TestcodeData</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -datafile=" + str(value))[value is not None ]</code>
          </format>
          <argpos>2</argpos>
          <comment>
            <text lang="en">The default data file is Etcode.dat and contains coding probabilities for each base. The probabilities are for both  positional and compositional information.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_required</name>
      <prompt lang="en">Required section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_window</name>
          <prompt lang="en">Length of sliding window (value greater than or equal to 200)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>200</value>
          </vdef>
          <format>
            <code proglang="python">("", " -window=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 200 is required</text>
            </message>
            <code proglang="python">value &gt;= 200</code>
          </ctrl>
          <argpos>3</argpos>
          <comment>
            <text lang="en">This is the number of nucleotide bases over which the TESTCODE statistic will be performed each time. The window will  then slide along the sequence, covering the same number of bases  each time.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_advanced</name>
      <prompt lang="en">Advanced section</prompt>

      <parameters>

        <parameter>
          <name>e_step</name>
          <prompt lang="en">Stepping increment for the window (value greater than or equal to 1)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>3</value>
          </vdef>
          <format>
            <code proglang="python">("", " -step=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <argpos>4</argpos>
          <comment>
            <text lang="en">The selected window will, by default, slide along the nucleotide sequence by three bases at a time, retaining the frame  (although the algorithm is not frame sensitive). This may be  altered to increase or decrease the increment of the slide.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_plot</name>
          <prompt lang="en">Graphical display</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -plot")[ bool(value) ]</code>
          </format>
          <argpos>5</argpos>
          <comment>
            <text lang="en">On selection a graph of the sequence (X axis) plotted against the coding score (Y axis) will be displayed. Sequence  above the green line is coding, that below the red line is  non-coding.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the report file</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">not e_plot</code>
          </precond>
          <vdef>
            <value>tcode.report</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>6</argpos>
        </parameter>

        <parameter>
          <name>e_rformat_outfile</name>
          <prompt lang="en">Choose the report output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">not e_plot</code>
          </precond>
          <vdef>
            <value>TABLE</value>
          </vdef>
          <vlist>
            <velem>
              <value>DASGFF</value>
              <label>Dasgff</label>
            </velem>
            <velem>
              <value>DBMOTIF</value>
              <label>Dbmotif</label>
            </velem>
            <velem>
              <value>DIFFSEQ</value>
              <label>Diffseq</label>
            </velem>
            <velem>
              <value>EMBL</value>
              <label>Embl</label>
            </velem>
            <velem>
              <value>EXCEL</value>
              <label>Excel</label>
            </velem>
            <velem>
              <value>FEATTABLE</value>
              <label>Feattable</label>
            </velem>
            <velem>
              <value>GENBANK</value>
              <label>Genbank</label>
            </velem>
            <velem>
              <value>GFF</value>
              <label>Gff</label>
            </velem>
            <velem>
              <value>LISTFILE</value>
              <label>Listfile</label>
            </velem>
            <velem>
              <value>MOTIF</value>
              <label>Motif</label>
            </velem>
            <velem>
              <value>NAMETABLE</value>
              <label>Nametable</label>
            </velem>
            <velem>
              <value>CODATA</value>
              <label>Codata</label>
            </velem>
            <velem>
              <value>REGIONS</value>
              <label>Regions</label>
            </velem>
            <velem>
              <value>SEQTABLE</value>
              <label>Seqtable</label>
            </velem>
            <velem>
              <value>SIMPLE</value>
              <label>Simple</label>
            </velem>
            <velem>
              <value>SRS</value>
              <label>Srs</label>
            </velem>
            <velem>
              <value>SWISS</value>
              <label>Swiss</label>
            </velem>
            <velem>
              <value>TABLE</value>
              <label>Table</label>
            </velem>
            <velem>
              <value>TAGSEQ</value>
              <label>Tagseq</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -rformat=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>7</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
            <dataFormat>
              <ref param="e_rformat_outfile">
              </ref>
            </dataFormat>
          </type>
          <precond>
            <code proglang="python">e_rformat_outfile in ['DASGFF', 'DBMOTIF', 'DIFFSEQ', 'EMBL', 'EXCEL', 'FEATTABLE', 'GENBANK', 'GFF', 'LISTFILE', 'MOTIF', 'NAMETABLE', 'CODATA', 'REGIONS', 'SEQTABLE', 'SIMPLE', 'SRS', 'SWISS', 'TABLE', 'TAGSEQ']</code>
          </precond>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>

        <parameter>
          <name>e_graph</name>
          <prompt lang="en">Choose the e_graph output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot</code>
          </precond>
          <vdef>
            <value>png</value>
          </vdef>
          <vlist>
            <velem>
              <value>png</value>
              <label>Png</label>
            </velem>
            <velem>
              <value>gif</value>
              <label>Gif</label>
            </velem>
            <velem>
              <value>cps</value>
              <label>Cps</label>
            </velem>
            <velem>
              <value>ps</value>
              <label>Ps</label>
            </velem>
            <velem>
              <value>meta</value>
              <label>Meta</label>
            </velem>
            <velem>
              <value>data</value>
              <label>Data</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">(" -graph=" + str(vdef), " -graph=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>8</argpos>
        </parameter>

        <parameter>
          <name>xy_goutfile</name>
          <prompt lang="en">Name of the output graph</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot</code>
          </precond>
          <vdef>
            <value>tcode_xygraph</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -goutfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>9</argpos>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_png</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "png"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.png"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_gif</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "gif"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.gif"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_ps</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>PostScript</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "ps" or e_graph == "cps"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.ps"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_meta</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "meta"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.meta"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_data</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "data"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.dat"</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>10</argpos>
    </parameter>
  </parameters>
</program>

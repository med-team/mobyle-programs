<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>blast2taxonomy</name>
    <version>2.1</version>
    <doc>
      <title>blast2taxonomy</title>
      <description>
        <text lang="en">Blast Taxonomy report</text>
      </description>
      <authors>C. Maufrais</authors>
      	<homepagelink>http://sourceforge.net/p/krona/home/krona/</homepagelink>
	<reference>Krona-2.0: Ondov BD, Bergman NH, and Phillippy AM. Interactive metagenomic visualization in a Web browser. BMC Bioinformatics. 2011 Sep 30; 12(1):385.</reference>
      
    </doc>
    <category>database:search:display</category>
    <command>blast2taxonomy</command>
  </head>
  <parameters>
    <parameter ismandatory="1" issimple="1">
      <name>infile</name>
      <prompt lang="en">Blast output file</prompt>
      <type>
        <datatype>
          <class>BlastTextReport</class>
          <superclass>Report</superclass>
        </datatype>
      </type>
      <format>
        <code proglang="perl">" $value"</code>
        <code proglang="python">" " + str(value)</code>
      </format>
      <argpos>10</argpos>
    </parameter>
    <paragraph>
      <name>display</name>
      <prompt lang="en">Display options</prompt>
      <argpos>1</argpos>
      <parameters>
      
        <parameter>
          <name>single</name>
          <prompt lang="en">Report one branch per organism (-s)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -s" : ""</code>
            <code proglang="python">("" , " -s") [value]</code>
          </format>
          <comment>
            <text lang="en">All hit are display in tree by default.</text>
          </comment>
        </parameter>
        <parameter>
          <name>acc</name>
          <prompt lang="en">Report accession number (-a)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -a" : ""</code>
            <code proglang="python">("" , " -a") [value]</code>
          </format>
        </parameter>
        <parameter>
          <name>node_name</name>
          <prompt lang="en">Lowest common ancestor name (-n).</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">($value) ? " -n $value" : ""</code>
            <code proglang="python">("" , " -n " + str(value)) [value is not None]</code>
          </format>
        </parameter>
        <parameter>
          <name>filterevalue</name>
          <prompt lang="en">Select hit blast with e-value lower than value (-E).</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>10.0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " - $value" : ""</code>
            <code proglang="python">("" , " -E " + str(value)) [value and value != vdef]</code>
          </format>
        </parameter>
        
        <parameter>
          <name>evalue</name>
          <prompt lang="en">Report score and e-value Blast hit (-e).</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -e" : ""</code>
            <code proglang="python">("" , " -e") [value]</code>
          </format>
        </parameter>
        <parameter>
          <name>perlen</name>
          <prompt lang="en">Report ratio of Blast hit length per query length (-l).</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -l" : ""</code>
            <code proglang="python">("" , " -l") [value]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>output</name>
      <prompt lang="en">Output option</prompt>
        <parameters>
           
           <parameter issimple="1">
	        <name>htmlKronaOutput</name>
	        <prompt>krona.2-0 representation of HSPs</prompt>
	           <type>
	             <datatype>
	                <class>Boolean</class>
	             </datatype>
			  </type>
			  <vdef>
		         <value>1</value>
		       </vdef>
			  <comment>
			    <text lang="en">Abundance is report in html file with krona Specification and Krona javascript library (-k).</text>
			  </comment>
			  <format>
			    <code proglang="python">('', ' -k blast_kronaView.html'  )[value]</code>
			  </format>
				  <argpos>30</argpos>
			</parameter>
		
		<parameter isout="1">
          <name>kronahtmloutfile</name>
          <prompt>HTML Output file(s)</prompt>
          <type>
            <datatype>
              <class>KronaHtmlReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">htmlKronaOutput</code>
          </precond>
          <filenames>
            <code proglang="python">"blast_kronaView.html"</code>
          </filenames>
        </parameter>
        	
        <parameter>
          <name>xlsoutput</name>
          <prompt lang="en">Tabular output (-x)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -x" : ""</code>
            <code proglang="python">("" , " -x") [value]</code>
          </format>
        </parameter>
        <parameter>
          <name>htmloutput</name>
          <prompt lang="en">Html output (-w)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -w" : ""</code>
            <code proglang="python">("" , " -w") [value]</code>
          </format>
        </parameter>
        <parameter>
          <name>dndoutput</name>
          <prompt lang="en">Taxonomy report in Newick format (-t)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -t" : ""</code>
            <code proglang="python">("" , " -t") [value]</code>
          </format>
        </parameter>
        <parameter>
          <name>outputfile</name>
          <prompt lang="en">Output file name (-o)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value)? " -o $value" : ""</code>
            <code proglang="python">(""  , " -o  " + str(value))  [value is not None ]</code>
          </format>
        </parameter>
        <parameter isout="1">
          <name>outfile_name</name>
          <prompt>Output file</prompt>
          <type>
            <datatype>
              <class>Blast2taxonomyReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">not $htmloutput and $outputfile</code>
            <code proglang="python">not htmloutput and outputfile</code>
          </precond>
          <filenames>
            <code proglang="perl">$outputfile</code>
            <code proglang="python">str(outputfile)</code>
          </filenames>
        </parameter>
        <parameter isstdout="1">
          <name>outfile</name>
          <prompt>Output file</prompt>
          <type>
            <datatype>
              <class>Blast2taxonomyReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">not ($htmloutput and $outputfile)</code>
            <code proglang="python">not (htmloutput and outputfile)</code>
          </precond>
          <filenames>
            <code proglang="perl">"blast2taxonomy.out"</code>
            <code proglang="python">"blast2taxonomy.out"</code>
          </filenames>
        </parameter>
        <parameter isout="1">
          <name>htmloutfile</name>
          <prompt>Html output file</prompt>
          <type>
            <datatype>
              <class>Blast2taxonomyHtmlReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$htmloutput</code>
            <code proglang="python">htmloutput</code>
          </precond>
          <filenames>
            <code proglang="perl">(defined $outputfile)? "$outputfile.html": "blast2taxonomy.html"</code>
            <code proglang="python">("blast2taxonomy.html", str(outputfile)+".html")[outputfile is not None]</code>
          </filenames>
        </parameter>
        <parameter isout="1">
          <name>htmloutfilealn</name>
          <prompt>Alignment Html output file</prompt>
          <type>
            <datatype>
              <class>AlnHtmlReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$htmloutput</code>
            <code proglang="python">htmloutput</code>
          </precond>
          <filenames>
            <code proglang="perl">"alignment.html"</code>
            <code proglang="python">"alignment.html"</code>
          </filenames>
        </parameter>
        <parameter isout="1">
          <name>dndoutfile</name>
          <prompt>Newick tree file</prompt>
          <type>
            <datatype>
              <class>Tree</class>
            </datatype>
            <dataFormat>NEWICK</dataFormat>
          </type>
          <precond>
            <code proglang="perl">$dndoutput</code>
            <code proglang="python">dndoutput</code>
          </precond>
          <filenames>
            <code proglang="perl">(defined $outputfile)? "$outputfile.dnd": "blast2taxonomy.dnd"</code>
            <code proglang="python">("blast2taxonomy.dnd", str(outputfile)+".dnd")[outputfile is not None]</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>
  </parameters>
</program>


<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>quicktree</name>
    <version>1.1</version>
    <doc>
      <title>QuickTree</title>
      <description>
        <text lang="en">Rapid reconstruction of phylogenies by the Neighbor-Joining method</text>
      </description>
      <authors>Kevin Howe, Alex Bateman, Richard Durbin</authors>
      <reference>Kevin Howe, Alex Bateman and Richard Durbin (2002).
	QuickTree: building huge Neighbour-Joining trees of protein
	sequences. Bioinformatics 18(11):1546-1547.</reference>
      <comment>
        <text lang="en">QuickTree is an efficient implementation of
	  the Neighbor-Joining algorithm, capable of reconstructing
	  phylogenies from huge alignments in time less than the age of
	  the universe.</text>
      </comment>
      <homepagelink>http://www.sanger.ac.uk/Software/analysis/quicktree/</homepagelink>
      <sourcelink>http://www.sanger.ac.uk/Software/analysis/quicktree/</sourcelink>
    </doc>
    <category>phylogeny:distance</category>
    <command>quicktree</command>
  </head>
  <parameters>
    <paragraph>
      <name>inpufile</name>
      <prompt lang="en">Input file</prompt>
      <comment>
        <text lang="en">You must enter either a distance matrix or a alignment.</text>
      </comment>
      <parameters>
        <parameter ismandatory="1">
          <name>aligfile</name>
          <prompt lang="en">Alignment file (-in a)</prompt>
          <type>
            <datatype>
              <class>Alignment</class>
            </datatype>
            <dataFormat>STOCKHOLM</dataFormat>
          </type>
          <precond>
            <code proglang="perl">not defined $distfile or (defined $aligfile and defined $distfile)</code>
            <code proglang="python">distfile is None or (distfile and aligfile)</code>
          </precond>
          <format>
            <code proglang="perl">(defined $value) ? " -in a $value" : ""</code>
            <code proglang="python">( "" , " -in a " + str(value) )[ value is not None ]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">You must enter either a distance matrix or a alignment.</text>
            </message>
            <code proglang="perl">not $distfile</code>
            <code proglang="python">distfile is None</code>
          </ctrl>
          <argpos>10</argpos>
        </parameter>
        <parameter ismandatory="1">
          <name>distfile</name>
          <prompt lang="en">or Distance matrix (-in m)</prompt>
          <type>
            <datatype>
              <class>PhylipDistanceMatrix</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">not defined $aligfile</code>
            <code proglang="python">aligfile is None</code>
          </precond>
          <format>
            <code proglang="perl">(defined $value) ? " -in m $value" : ""</code>
            <code proglang="python">( "" , " -in m " + str(value) )[ value is not None ]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">You must enter either a distance matrix or a alignment.</text>
            </message>
            <code proglang="perl">not defined $aligfile</code>
            <code proglang="python">aligfile is None</code>
          </ctrl>
          <argpos>10</argpos>
          <comment>
            <text lang="en">Give a file containing a distance matrix
      obtained by distance matrix programs like prodist or
      dnadist, ...</text>
          </comment>
          <example>
    5
Alpha       0.000000  0.330447  0.625670  1.032032  1.354086
Beta        0.330447  0.000000  0.375578  1.096290  0.677616
Gamma       0.625670  0.375578  0.000000  0.975798  0.861634
Delta       1.032032  1.096290  0.975798  0.000000  0.226703
Epsilon     1.354086  0.677616  0.861634  0.226703  0.000000
      </example>
        </parameter>
      </parameters>
    </paragraph>
    <parameter>
      <name>out</name>
      <prompt lang="en">Output (-out)</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>t</value>
      </vdef>
      <vlist>
        <velem>
          <value>m</value>
          <label>Distance matrix in Phylip format(m)</label>
        </velem>
        <velem>
          <value>t</value>
          <label>Tree in New Hampshire format (t)</label>
        </velem>
      </vlist>
      <format>
        <code proglang="perl">(defined $value and $value ne $vdef) ? " -out $value" : ""</code>
        <code proglang="python">( "" , " -out " + str(value) )[ value is not None and value != vdef]</code>
      </format>
      <argpos>3</argpos>
    </parameter>
    <paragraph>
      <name>treeopt</name>
      <prompt lang="en">Tree output options</prompt>
      <precond>
        <code proglang="perl">$out ne "m"</code>
        <code proglang="python">out != "m"</code>
      </precond>
      <parameters>
        <parameter>
          <name>upgma</name>
          <prompt lang="en">Use the UPGMA method to construct the tree (-upgma)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -upgma" : ""</code>
            <code proglang="python">( "" , " -upgma" )[ value ]</code>
          </format>
          <argpos>3</argpos>
          <comment>
            <text lang="en">Bootstrapping is not available for a matrix output</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>aligopt</name>
      <prompt lang="en">Alignment options</prompt>
      <parameters>
        <parameter>
          <name>boot</name>
          <prompt lang="en">Calculate bootstrap values with n iterations (-boot)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">not defined $distfile and $out ne "m"</code>
            <code proglang="python">distfile is None and out != "m"</code>
          </precond>
          <format>
            <code proglang="perl">(defined $value) ? " -boot $value" : ""</code>
            <code proglang="python">( "" , " -boot " + str(value))[ value is not None ]</code>
          </format>
          <argpos>3</argpos>
          <comment>
            <text lang="en">Bootstrapping is not available for a matrix output</text>
          </comment>
        </parameter>
        <parameter>
          <name>kimura</name>
          <prompt lang="en">Use the kimura translation for pairwise distances (-kimura)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">defined $aligfile</code>
            <code proglang="python">aligfile is not None</code>
          </precond>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -kimura" : ""</code>
            <code proglang="python">( "" , " -kimura" )[ value ]</code>
          </format>
          <argpos>3</argpos>
        </parameter>
      </parameters>
    </paragraph>
    <parameter isstdout="1">
      <name>treefile</name>
      <prompt>Tree file</prompt>
      <type>
        <datatype>
          <class>Tree</class>
        </datatype>
        <dataFormat>NEWICK</dataFormat>
      </type>
      <precond>
        <code proglang="perl">$out ne "m"</code>
        <code proglang="python">out != "m"</code>
      </precond>
      <filenames>
        <code proglang="perl">"quicktree.out"</code>
        <code proglang="python">"quicktree.out"</code>
      </filenames>
    </parameter>
    <parameter isstdout="1">
      <name>distoutfile</name>
      <prompt>Distance matrix</prompt>
      <type>
        <datatype>
          <class>PhylipDistanceMatrix</class>
          <superclass>AbstractText</superclass>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$out eq "m"</code>
        <code proglang="python">out == "m"</code>
      </precond>
      <filenames>
        <code proglang="perl">"quicktree.out"</code>
        <code proglang="python">"quicktree.out"</code>
      </filenames>
    </parameter>
  </parameters>
</program>


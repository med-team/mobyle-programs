<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>mspcrunch</name>
    <version>2.5</version>
    <doc>
      <title>MSPcrunch</title>
      <description>
        <text lang="en">A BLAST post-processing filter</text>
      </description>
      <authors>Sonnhammer, Durbin</authors>
      <doclink>http://bioweb2.pasteur.fr/docs/mspcrunch/MSPcrunch2.pdf</doclink>
      <homepagelink>http://sonnhammer.sbc.su.se/MSPcrunch.html</homepagelink>
      <sourcelink>http://sonnhammer.sbc.su.se/download/software/MSPcrunch+Blixem/</sourcelink>
    </doc>
    <category>database:search:display</category>
  </head>
  <parameters>
    <parameter iscommand="1" ishidden="1">
      <name>mspcrunch</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"MSPcrunch"</code>
        <code proglang="python">"MSPcrunch"</code>
      </format>
      <argpos>0</argpos>
    </parameter>
    <paragraph>
      <name>input_options</name>
      <prompt lang="en">Input Options</prompt>
      <argpos>1</argpos>
      <parameters>
        <parameter ismandatory="1" issimple="1">
          <name>blast_output</name>
          <prompt lang="en">BLAST output File</prompt>
          <type>
            <datatype>
              <class>BlastTextReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="perl">" $value"</code>
            <code proglang="python">" "+str(value)</code>
          </format>
          <argpos>2</argpos>
        </parameter>
        <parameter>
          <name>force_blastp</name>
          <prompt lang="en">Force Blastp mode (default Blastx) (-p)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -p" : "" </code>
            <code proglang="python">( ""  , " -p" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>force_blastn</name>
          <prompt lang="en">Force Blastn mode (default Blastx) (-n)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -n" : "" </code>
            <code proglang="python">( ""  , " -n" )[ value ]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>analyse_options</name>
      <prompt lang="en">Control options</prompt>
      <argpos>1</argpos>
      <parameters>
        <parameter>
          <name>gapped</name>
          <prompt lang="en">Make gapped alignment of ungapped-MSP contigs (-G)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -G" : "" </code>
            <code proglang="python">( ""  , " -G" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>cov_limit</name>
          <prompt lang="en">Set coverage limit (-l)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>10</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -l $value" : "" </code>
            <code proglang="python">( ""  , " -l "+str(value) )[ value is not None  and value !=vdef]</code>
          </format>
          <comment>
            <text lang="en">0 = No coverage rejection</text>
          </comment>
        </parameter>
        <parameter>
          <name>old_cutoff</name>
          <prompt lang="en">Use old step cutoffs for adjacency instead of the new continuous system. (-O)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -O" : "" </code>
            <code proglang="python">( ""  , " -O" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>dont_reject</name>
          <prompt lang="en">Don't reject any MSPs (-w)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -w" : "" </code>
            <code proglang="python">( ""  , " -w" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>report_rejected</name>
          <prompt lang="en">Report only rejected MSPs (-r)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -r" : "" </code>
            <code proglang="python">( ""  , " -r" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>threshold_id</name>
          <prompt lang="en">Reject all matches with less than this % identity (-I)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -I $value" : "" </code>
            <code proglang="python">( ""  , " -I " + str(value) )[ value is not None ]</code>
          </format>
        </parameter>
        <parameter>
          <name>threshold_length</name>
          <prompt lang="en">Reject all matches with length less than this value (-L)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -L $value" : "" </code>
            <code proglang="python">( ""  , " -L " + str(value) )[ value is not None ]</code>
          </format>
        </parameter>
        <parameter>
          <name>expect</name>
          <prompt lang="en">Reject all matches with E-value higher than this value (-e)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -e $value" : ""</code>
            <code proglang="python">( "" , " -e " + str(value))[ value is not None ]</code>
          </format>
        </parameter>
        <!-- core dumped

	<parameter>
	  <name>matrix</name>
	  <prompt lang="en">Scoring matrix (PAM or BLOSUM) (-m)</prompt>
	  <type>
	    <datatype>
	      <class>ScoringMatrix</class>
	      <superclass>AbstractText</superclass>
	    </datatype>
	  </type>
	  <format>
	    <code proglang="perl"> ($value) ? " -m $value" : ""</code>
	    <code proglang="python">( "" , " -m " + str(value) )[ value is not None ]</code>
	  </format>
	</parameter>
-->
        <parameter>
          <name>query</name>
          <prompt lang="en">Read in query seq (for rereading .seqbl files) (-Q)</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>FASTA</dataFormat>
          </type>
          <format>
            <code proglang="perl">(defined $value)? " -Q $query" : "" </code>
            <code proglang="python">( ""  , " -Q " + str(value) )[ value is not None ]</code>
          </format>
        </parameter>
        <parameter>
          <name>whole_contig</name>
          <prompt lang="en">Coverage limitation requires whole contig to be covered (always for Blastp) (-a)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -a" : "" </code>
            <code proglang="python">( ""  , " -a" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>hits_to_self</name>
          <prompt lang="en">Accept hits to self (-s)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -s" : "" </code>
            <code proglang="python">( ""  , " -s" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>no_hits_to_earlier</name>
          <prompt lang="en">Ignore hits to earlier seqnames (for All-vs-All) (-A)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -A" : "" </code>
            <code proglang="python">( ""  , " -A" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>stats_without_X</name>
          <prompt lang="en">Recalculate percentage identity, ignoring X residues. (-j)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -j" : "" </code>
            <code proglang="python">( ""  , " -j" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>stats_without_end</name>
          <prompt lang="en">Recalculate percentage identity, ignoring mismatches at ends. (-J)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -J" : "" </code>
            <code proglang="python">( ""  , " -J" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>silent_mutations</name>
          <prompt lang="en">Do Statistics of Silent mutations (only cDNA!) (-S)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -S" : "" </code>
            <code proglang="python">( ""  , " -S" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>matrix_stats</name>
          <prompt lang="en">Print statistics on used matrices (-E)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -E" : "" </code>
            <code proglang="python">( ""  , " -E" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>all_expected</name>
          <prompt lang="en">Print all Expected scores (default only when positive) (-X)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -X" : "" </code>
            <code proglang="python">( ""  , " -X" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>line_length</name>
          <prompt lang="en">Line length of Wrapped alignment (-W)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? " -W $value" : "" </code>
            <code proglang="python">( ""  , " -W " +str(value) )[ value is not None ]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>output_options</name>
      <prompt lang="en">Output Options</prompt>
      <argpos>1</argpos>
      <parameters>
        <parameter isstdout="1">
          <name>outfile</name>
          <prompt lang="en">Result file</prompt>
          <type>
            <datatype>
              <class>Report</class>
            </datatype>
          </type>
          <filenames>
            <code proglang="perl">"mspcrunch.out" </code>
            <code proglang="python">"mspcrunch.out" </code>
          </filenames>
        </parameter>
        <parameter>
          <name>big_pict</name>
          <prompt lang="en">Big Picture output (-P)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -P" : "" </code>
            <code proglang="python">( ""  , " -P" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>matches_one_line</name>
          <prompt lang="en">For Big Picture output, force all matches to the same subject on one line (-F)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$big_pict</code>
            <code proglang="python">big_pict</code>
          </precond>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -F" : "" </code>
            <code proglang="python">( ""  , " -F" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>sfs</name>
          <prompt lang="en">Produce SFS output (-H)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -H" : ""</code>
            <code proglang="python">( "" , " -H" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>seqbl</name>
          <prompt lang="en">Produce seqbl  output for Blixem (-q)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -q" : "" </code>
            <code proglang="python">( ""  , " -q" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>wublast_numbered</name>
          <prompt lang="en">Indicate query insertions with numbers (For seqbl output from Wublast) (-N)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$seqbl</code>
            <code proglang="python">seqbl</code>
          </precond>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -N" : "" </code>
            <code proglang="python">( ""  , " -N" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>ace</name>
          <prompt lang="en">Produce .ace output (for ACEDB 4) (-4)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -4" : "" </code>
            <code proglang="python">( ""  , " -4" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>dont_mirror</name>
          <prompt lang="en">Don't mirror (i.e. print the subject object) in ACE4 format (-M)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$ace</code>
            <code proglang="python">ace</code>
          </precond>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -M" : "" </code>
            <code proglang="python">( ""  , " -M" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>exblx</name>
          <prompt lang="en">Produce exblx output (for easy parsing) (-x)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -x" : "" </code>
            <code proglang="python">( ""  , " -x" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>exbldb</name>
          <prompt lang="en">Produce exbldb output (as exblx with query names) (-d)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -d" : "" </code>
            <code proglang="python">( ""  , " -d" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>fasta</name>
          <prompt lang="en">Produce fasta output (unaligned, for mult.alignm.) (-2)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -2" : "" </code>
            <code proglang="python">( ""  , " -2" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>three_frame</name>
          <prompt lang="en">Print 3 frame translation (blastn only) (-3)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -3" : "" </code>
            <code proglang="python">( ""  , " -3" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>footer</name>
          <prompt lang="en">Print footer with parameters and stats (-f)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -f" : "" </code>
            <code proglang="python">( ""  , " -f" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>percentage_id</name>
          <prompt lang="en">Print percentage identity (seqbl output only) (-i)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$seqbl</code>
            <code proglang="python">seqbl</code>
          </precond>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -i" : "" </code>
            <code proglang="python">( ""  , " -i" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>stats</name>
          <prompt lang="en">Output coverage stats?</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">""</code>
            <code proglang="python">""</code>
          </format>
        </parameter>
        <parameter isout="1">
          <name>stats_file</name>
          <prompt lang="en">Coverage stats outputfile</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$stats</code>
            <code proglang="python">stats</code>
          </precond>
          <format>
            <code proglang="perl">($value) ? " -o mspcrunch.stats" : "" </code>
            <code proglang="python">( ""  , " -o mspcrunch.stats" )[ value is not None ]</code>
          </format>
          <filenames>
            <code proglang="perl">"mspcrunch.stats"</code>
            <code proglang="python">"mspcrunch.stats"</code>
          </filenames>
        </parameter>
        <parameter>
          <name>domainer</name>
          <prompt lang="en">Produce output for Domainer (trim overlaps) (-D)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -D" : "" </code>
            <code proglang="python">( ""  , " -D" )[ value ]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
  </parameters>
</program>


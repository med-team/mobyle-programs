<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>toppred</name>
    <version>1.10</version>
    <doc>
      <title>TopPred</title>
      <description>
        <text lang="en">Topology prediction of membrane proteins</text>
      </description>
      <authors>Heijne, Wallin, Claros, Deveaud, Schuerer</authors>
      <reference>von Heijne, G. (1992) Membrane Protein Structure Prediction: Hydrophobicity Analysis and the 'Positive Inside' Rule. J.Mol.Biol. 225, 487-494.</reference>
      <reference>Claros, M.G., and von Heijne, G. (1994) TopPred II: An Improved Software For Membrane Protein Structure Predictions. CABIOS 10, 685-686.</reference>
      <reference>Deveaud and Schuerer (Institut Pasteur) new implementation of the original toppred program, based on G. von Heijne algorithm.</reference>
      <doclink>http://bioweb2.pasteur.fr/docs/toppred/toppred.pdf</doclink>
      <sourcelink>ftp://ftp.pasteur.fr/pub/gensoft/projects/toppred/</sourcelink>
    </doc>
    <category>sequence:protein:2D_structure</category>
    <category>structure:2D_structure</category>
  </head>
  <parameters>
    <parameter iscommand="1" ishidden="1">
      <name>toppred</name>
      <prompt lang="en">Command</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"toppred"</code>
        <code proglang="python">"toppred"</code>
      </format>
      <argpos>0</argpos>
    </parameter>
    <parameter ismandatory="1" issimple="1" ismaininput="1">
      <name>query</name>
      <prompt lang="en">Sequence</prompt>
      <type>
        <biotype>Protein</biotype>
        <datatype>
          <class>Sequence</class>
        </datatype>
        <dataFormat>FASTA</dataFormat>
        <card>1,n</card>
      </type>
      <format>
        <code proglang="perl">" $value"</code>
        <code proglang="python">" "+ str( value )</code>
      </format>
      <argpos>10</argpos>
    </parameter>
    <parameter issimple="1">
      <name>graph_output</name>
      <prompt lang="en">Produce hydrophobicity graph image (-g)</prompt>
      <type>
        <datatype>
          <class>Boolean</class>
        </datatype>
      </type>
      <vdef>
        <value>1</value>
      </vdef>
    </parameter>
    <parameter issimple="1">
      <name>topo_output</name>
      <prompt lang="en">Produce image of each topology (-t)</prompt>
      <type>
        <datatype>
          <class>Boolean</class>
        </datatype>
      </type>
      <vdef>
        <value>0</value>
      </vdef>
      <format>
        <code proglang="perl">($value) ? "" : " -t none"</code>
        <code proglang="python">(" -t none","")[ value ]</code>
      </format>
      <argpos>7</argpos>
    </parameter>
    <paragraph>
      <name>control</name>
      <prompt lang="en">Control options</prompt>
      <parameters>
        <parameter>
          <name>scale</name>
          <prompt lang="en">Hydrophobicity scale (-H)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>GES-scale</value>
          </vdef>
          <vlist>
            <velem>
              <value>KD-scale</value>
              <label>KD-scale (Kyte and Doolittle)</label>
            </velem>
            <velem>
              <value>GVH-scale</value>
              <label>GVH-scale (Gunnar von Heijne)</label>
            </velem>
            <velem>
              <value>GES-scale</value>
              <label>GES-scale (Goldman Engelman Steitz)</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? "-H $value" : ""</code>
            <code proglang="python">( "" , " -H " + str( value ) )[ value is not None and value != vdef ]</code>
          </format>
          <argpos>1</argpos>
        </parameter>
        <parameter>
          <name>organism</name>
          <prompt lang="en">Organism: eukaryot (default is prokaryot) (-e)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -e" : ""</code>
            <code proglang="python">( "" , " -e" )[ value ]</code>
          </format>
          <argpos>1</argpos>
        </parameter>
        <parameter>
          <name>certain</name>
          <prompt lang="en">Certain cutoff (-c)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>1.0</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -c $value" : ""</code>
            <code proglang="python">( "" , " -c " + str(value) ) [ value is not None and value!= vdef ]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Certain cutoff must be greater than putative cutoff</text>
            </message>
            <code proglang="perl">$certain &gt; $putative</code>
            <code proglang="python">certain &gt; putative</code>
          </ctrl>
          <argpos>2</argpos>
        </parameter>
        <parameter>
          <name>putative</name>
          <prompt lang="en">Putative cutoff (-p)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.6</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? "-p $value" : ""</code>
            <code proglang="python">( "" , " -p " + str(value) )[ value is not None and value != vdef ]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Putative cutoff must be not be greater than certain cutoff</text>
            </message>
            <code proglang="perl">$certain &gt; $putative</code>
            <code proglang="python">certain &gt; putative</code>
          </ctrl>
          <argpos>2</argpos>
        </parameter>
        <parameter>
          <name>core</name>
          <prompt lang="en">Core window size: (-n)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>10</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -n $value" : ""</code>
            <code proglang="python">( "" , " -n " + str(value) )[ value is not None and value != vdef ]</code>
          </format>
          <argpos>2</argpos>
        </parameter>
        <parameter>
          <name>triangle</name>
          <prompt lang="en">Wedge window size: (-q)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>5</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -q $value" : ""</code>
            <code proglang="python">( "" , " -q " + str(value) )[ value is not None and value != vdef ]</code>
          </format>
          <argpos>2</argpos>
        </parameter>
        <parameter>
          <name>loop_length</name>
          <prompt lang="en">Critical loop length (-s)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>60</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -s $value" : ""</code>
            <code proglang="python">( "" , " -s " + str(value) )[ value is not None and value != vdef ]</code>
          </format>
          <argpos>2</argpos>
        </parameter>
        <parameter>
          <name>Segment_distance</name>
          <prompt lang="en">Critical transmembrane spacer (-d)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>2</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -d $value" : ""</code>
            <code proglang="python">( "" , " -d " + str( value ) )[ value is not None and value!= vdef ]</code>
          </format>
          <argpos>2</argpos>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>output_options</name>
      <prompt lang="en">Output options</prompt>
      <parameters>
        <parameter>
          <name>outformat</name>
          <prompt lang="en">Output format (-O)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>new</value>
          </vdef>
          <vlist>
            <velem>
              <value>new</value>
              <label>New text output format (new)</label>
            </velem>
            <velem>
              <value>html</value>
              <label>HTML</label>
            </velem>
            <velem>
              <value>old</value>
              <label>Output format of the old toppred implementation (old)</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef) ? " -O $value" : ""</code>
            <code proglang="python">( "" , " -O " + str(value) )[ value is not None and value != vdef ]</code>
          </format>
          <argpos>5</argpos>
        </parameter>
        <parameter>
          <name>profile_format</name>
          <prompt lang="en">Hydrophobicity Profile file format (-g)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$graph_output </code>
            <code proglang="python">graph_output </code>
          </precond>
          <vdef>
            <value>png</value>
          </vdef>
          <vlist>
            <velem>
              <value>png</value>
              <label>PNG output (png)</label>
            </velem>
            <velem>
              <value>ps</value>
              <label>PostScript output (ps)</label>
            </velem>
            <velem>
              <value>ppm</value>
              <label>PPM output (ppm)</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">($graph_output) ? " -g $value" : " -g none"</code>
            <code proglang="python">( " -g none" , " -g " + str(value) )[ graph_output ]</code>
          </format>
          <argpos>7</argpos>
        </parameter>
      </parameters>
    </paragraph>
    <parameter isout="1">
      <name>graphicfiles</name>
      <prompt lang="en">Graphic output files</prompt>
      <type>
        <datatype>
          <class>Picture</class>
          <superclass>Binary</superclass>
        </datatype>
        <dataFormat>
          <ref param="profile_format"/>
        </dataFormat>
        <card>0,n</card>
      </type>
      <precond>
        <code proglang="perl">$graph_output </code>
        <code proglang="python">graph_output </code>
      </precond>
      <filenames>
        <code proglang="perl">*.$profile_format</code>
        <code proglang="python">'*.' + profile_format</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>hydrophobicity_files</name>
      <prompt lang="en">Hydrophobicity output files</prompt>
      <type>
        <datatype>
          <class>Text</class>
        </datatype>
        <card>1,n</card>
      </type>
      <filenames>
        <code proglang="perl">*.hydro*</code>
        <code proglang="python">'*.hydro*'</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>html_file</name>
      <prompt lang="en">Output file in html format</prompt>
      <type>
        <datatype>
          <class>ToppredHtmlReport</class>
          <superclass>Report</superclass>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">outformat eq 'html'</code>
        <code proglang="python">outformat == 'html'</code>
      </precond>
      <filenames>
        <code proglang="perl">*.html</code>
        <code proglang="python">'*.html'</code>
      </filenames>
    </parameter>
  </parameters>
</program>


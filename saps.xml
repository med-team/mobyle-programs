<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>saps</name>
    <version>07-01-1995</version>
    <doc>
      <title>SAPS</title>
      <description>
        <text lang="en">Statistical Analysis of Protein Sequences</text>
      </description>
      <authors>V. Brendel</authors>
      <reference>Brendel, V., Bucher, P., Nourbakhsh, I., Blaisdell, B.E., Karlin, S. (1992) Methods and algorithms for statistical analysis of protein sequences. Proc. Natl. Acad. Sci. USA 89: 2002-2006.</reference>
    </doc>
    <category>sequence:protein:composition</category>
    <command>saps</command>
  </head>
  <parameters>
    <parameter ismandatory="1" issimple="1">
      <name>seq</name>
      <prompt lang="en">Protein sequence(s) File</prompt>
      <type>
        <biotype>Protein</biotype>
        <datatype>
          <class>Sequence</class>
        </datatype>
        <dataFormat>EMBL</dataFormat>
      </type>
      <format>
        <code proglang="perl">" $value"</code>
        <code proglang="python">" "+str(value)</code>
      </format>
      <argpos>2</argpos>
    </parameter>
    <paragraph>
      <name>control</name>
      <prompt lang="en">Control options</prompt>
      <argpos>1</argpos>
      <parameters>
        <parameter>
          <name>specie</name>
          <prompt lang="en">Use this specie for quantile comparisons (-s)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>swp23s</value>
          </vdef>
          <vlist>
            <velem>
              <value>BACSU</value>
              <label>Bacillus subtilis (BACSU)</label>
            </velem>
            <velem>
              <value>DROME</value>
              <label>Drosophila melanogaster (DROME)</label>
            </velem>
            <velem>
              <value>HUMAN</value>
              <label>Human (HUMAN)</label>
            </velem>
            <velem>
              <value>RAT</value>
              <label>Rat (RAT)</label>
            </velem>
            <velem>
              <value>YEAST</value>
              <label>Saccharomyces cerevisiae (YEAST)</label>
            </velem>
            <velem>
              <value>CHICK</value>
              <label>Chicken (CHICK)</label>
            </velem>
            <velem>
              <value>ECOLI</value>
              <label>Escherichia coli (ECOLI)</label>
            </velem>
            <velem>
              <value>MOUSE</value>
              <label>Mouse (MOUSE)</label>
            </velem>
            <velem>
              <value>XENLA</value>
              <label>Frog (XENLA)</label>
            </velem>
            <velem>
              <value>swp23s</value>
              <label>Random sample of proteins from SWISS-PROT 23.0 (swp23s)</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value and $value ne $vdef)? " -s $value" : "" </code>
            <code proglang="python">( ""  , " -s " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>H_positive</name>
          <prompt lang="en">Count H as positive charge (-H)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl"> ($value)? " -H":""</code>
            <code proglang="python">("" , " -H")[ value ]</code>
          </format>
          <comment>
            <text lang="en">By default, SAPS treats only lysine (K) and arginine (R) as positively charged residues. If the command line flag -H is set, then histidine (H) is also treated as positively charged in all parts of the program involving the charge alphabet.</text>
          </comment>
        </parameter>
        <parameter>
          <name>analyze</name>
          <prompt lang="en">Analyze spacings of amino acids X, Y, .... (-a)</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl"> (defined $value)? " -a $value":""</code>
            <code proglang="python">("" , " -a " + str(value))[ value is not None ]</code>
          </format>
          <comment>
            <text lang="en">Clusters of particular amino acid types may be evaluated by means of the same tests that are used to detect clustering of charged residues (binomial model and scoring statistics). These tests are invoked by setting this flag; for example, to test (separately) for clusters of alanine (A) and serine (S), set this parameter to AS. The binomial test is also programmed for certain combinations of amino acids: AG (flag -a a), PEST (flag -a p), QP (flag -a q), ST (flag -a s).</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>output</name>
      <prompt lang="en">Output options</prompt>
      <argpos>1</argpos>
      <parameters>
        <parameter>
          <name>documented</name>
          <prompt lang="en">Generate documented output (-d)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl"> ($value)? " -d":""</code>
            <code proglang="python">("" , " -d")[ value ]</code>
          </format>
          <comment>
            <text lang="en">The output will come with documentation that annotates each part of the program; this flag should be set when SAPS is used for the first time as it provides helpful explanations with respect to the statistics being used and the layout of the output.</text>
          </comment>
        </parameter>
        <parameter>
          <name>terse</name>
          <prompt lang="en">Generate terse output (-t)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl"> ($value)? " -t":""</code>
            <code proglang="python">("" , " -t")[ value ]</code>
          </format>
          <comment>
            <text lang="en">This flag specifies terse output that is limited to the analysis of the charge distribution and of high scoring segments.</text>
          </comment>
        </parameter>
        <parameter>
          <name>verbose</name>
          <prompt lang="en">Generate verbose output (-v)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl"> ($value)? " -v":""</code>
            <code proglang="python">("" , " -v")[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>table</name>
          <prompt lang="en">Append computer-readable table summary output (-T)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl"> ($value)? " -T":""</code>
            <code proglang="python">("" , " -T")[ value ]</code>
          </format>
          <comment>
            <text lang="en">This flag is used in conjunction with the analysis of sets of proteins ; if specified, the file saps.table is appended with computer-readable lines describing the input files and their significant features.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>
    <parameter isout="1">
      <name>tablefile</name>
      <prompt lang="en">Summary table output</prompt>
      <type>
        <datatype>
          <class>Text</class>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$table</code>
        <code proglang="python">table</code>
      </precond>
      <filenames>
        <code proglang="perl">"*.table"</code>
        <code proglang="python">"*.table"</code>
      </filenames>
    </parameter>
  </parameters>
</program>


<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Sandrine Larroude 	                                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>growthpred</name>
    <version>v1.07</version>
    <doc>
      <title>growthpred</title>
      <description>
        <text lang="en">Sequence-based Prediction of Minimum Generation Times for Bacteria and Archaea</text>
      </description>
      <authors>S. Vieira-Silva, E. Rocha</authors>
      <reference>
        <a xmlns="http://www.w3.org/1999/xhtml" target="ftp" href="ftp://ftp.pasteur.fr/pub/gensoft/projects/growthpred/"> The program sources and Example files are downloadable here. </a>
      </reference>
      <reference>Vieira-Silva S,  Rocha EPC, 2010 The Systemic Imprint of Growth and Its Uses in Ecological (Meta)Genomics. PLoS Genet 6(1): e1000808. doi:10.1371/journal.pgen.1000808</reference>
      <doclink>http://bioweb2.pasteur.fr/docs/growthpred/growthpred.pdf</doclink>
      <comment>
        <text lang="en">This application predicts the minimum generation time for a bacterial or archaeal organism based on  its codon usage bias intensity (CUB). 
    	The CUB index is calculated given two input sets of sequences: 1) highly expressed genes 2) other genes. 
    	The application runs 1000 bootstraps and outputs the average and the standard deviation of the predictions.
    	</text>
      </comment>
      <sourcelink>ftp://ftp.pasteur.fr/pub/GenSoft/projects/growthpred/</sourcelink>
    </doc>
    <category>sequence:nucleic:prediction</category>
    <category>sequence:nucleic:codon_usage</category>
    <command>growthpred</command>
  </head>
  <parameters>
    <parameter>
      <name>example</name>
      <prompt lang="en">Run with example data (-e)</prompt>
      <argpos>4</argpos>
      <type>
        <datatype>
          <class>Boolean</class>
        </datatype>
      </type>
      <vdef>
        <value>0</value>
      </vdef>
      <format>
        <code proglang="python">( "" , " -e " )[value]</code>
      </format>
      <comment>
        <text lang="en">Use a set of example files (E. coli K12) to run the program.</text>
        <text lang="en">The expected results depending on the choosen option and the example files are shown in the program help pages (end of the form).</text>
      </comment>
    </parameter>
    <paragraph>
      <name>input</name>
      <prompt lang="en">Input section</prompt>
      <argpos>1</argpos>
      <parameters>
        <parameter ismandatory="1">
          <name>hsequence</name>
          <prompt lang="en">Enter sequences of highly expressed genes (-f)</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>FASTA</dataFormat>
            <card>1,n</card>
          </type>
          <precond>
            <code proglang="python">not example and not b</code>
          </precond>
          <format>
            <code proglang="python">( "" , " -f " + str( value ) + " " )[value is not None]</code>
          </format>
          <comment>
            <text lang="en">Set of genes under purifying selection for codon usage.</text>
          </comment>
        </parameter>
        <parameter>
          <name>b</name>
          <prompt lang="en">Retrieve ribosomal protein genes by blast (-b)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">( "" , " -b")[value]</code>
          </format>
        </parameter>
        <parameter ismandatory="1">
          <name>nhsequence</name>
          <prompt lang="en">Enter sequences of non-highly expressed genes/Complete genome (-g)</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>FASTA</dataFormat>
            <card>1,n</card>
          </type>
          <precond>
            <code proglang="python">not example</code>
          </precond>
          <format>
            <code proglang="python">( "" , " -g " + str( value ) + " " )[value is not None]</code>
          </format>
          <comment>
            <text lang="en">Set of control genes with near random codon usage.</text>
          </comment>
        </parameter>
        <parameter>
          <name>typeg</name>
          <prompt lang="en">Mixed organisms sequences</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">( "" , " -m" )[value]</code>
          </format>
          <comment>
            <text lang="en">You need to precise to the program if your sequences are metagenome or mixed organisms sequences.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>codon_remove</name>
      <prompt lang="en">Remove from sequences</prompt>
      <argpos>3</argpos>
      <parameters>
        <parameter>
          <name>fc</name>
          <prompt lang="en">First codon (-s)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">( " -s " , "" )[value!=vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>lc</name>
          <prompt lang="en">Last codon (-S)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">( " -S " , "" )[value]</code>
          </format>
        </parameter>
      </parameters>
      <layout>
        <hbox>
          <box>fc</box>
          <box>lc</box>
        </hbox>
      </layout>
    </paragraph>
    <paragraph>
      <name>options</name>
      <prompt lang="en">Options</prompt>
      <argpos>2</argpos>
      <parameters>
        <parameter>
          <name>geneticcode</name>
          <prompt lang="en">Choose genetic code (-c)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <vlist>
            <velem>
              <value>0</value>
              <label>Universal</label>
            </velem>
            <velem>
              <value>1</value>
              <label>Yeast Mitochondrial</label>
            </velem>
            <velem>
              <value>2</value>
              <label>Vertebrate Mitochondrial</label>
            </velem>
            <velem>
              <value>3</value>
              <label>Mold/Protozoan/Mycoplasma/Spiroplasma</label>
            </velem>
            <velem>
              <value>4</value>
              <label>Invertebrate Mitochondrial</label>
            </velem>
            <velem>
              <value>5</value>
              <label>Candida cylindracea</label>
            </velem>
            <velem>
              <value>6</value>
              <label>Ciliate</label>
            </velem>
            <velem>
              <value>7</value>
              <label>Euplotes</label>
            </velem>
            <velem>
              <value>8</value>
              <label>Echinoderm/Flatworm Mitochondrial</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">" -c " + str( value )</code>
          </format>
        </parameter>
        <parameter>
          <name>rfiles</name>
          <prompt lang="en">Recover file with ribosomal protein genes retrieved by blast or given as input (-r)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">( "" , " -r")[value]</code>
          </format>
        </parameter>
        <parameter>
          <name>ifiles</name>
          <prompt lang="en">Recover file with codon usage bias indexes for each gene (-i)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">( "" , " -i")[value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>autotemp</name>
          <prompt lang="en">Estimate optimal growth temperature (-t)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">( "" , " -t")[value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>temp</name>
          <prompt lang="en">Enter optimal growth temperature (Celsius) (-T)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>36</value>
          </vdef>
          <precond>
            <code proglang="perl">not $autotemp</code>
            <code proglang="python">not autotemp</code>
          </precond>
          <format>
            <code proglang="python">( "" , " -T " + str( value ) )[value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>outfile</name>
          <prompt lang="en">Outfile name (-o)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>outfile</value>
          </vdef>
          <format>
            <code proglang="python">( "" , " -o " + str(value))[ value is not None ]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
    <parameter isout="1">
      <name>res</name>
      <type>
        <datatype>
          <class>Text</class>
        </datatype>
      </type>
      <prompt lang="en">Prediction results</prompt>
      <filenames>
        <code proglang="python">"*.results"</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>err</name>
      <type>
        <datatype>
          <class>Text</class>
        </datatype>
      </type>
      <prompt lang="en">Prediction error(s)</prompt>
      <filenames>
        <code proglang="python">"*.errors"</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>cubs</name>
      <type>
        <datatype>
          <class>Text</class>
        </datatype>
      </type>
      <prompt lang="en">Codon usage bias indexes for each gene</prompt>
      <precond>
        <code proglang="python">ifiles</code>
      </precond>
      <filenames>
        <code proglang="python">"*.cub"</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>ribs</name>
      <type>
        <datatype>
          <class>Text</class>
        </datatype>
      </type>
      <prompt lang="en">Ribosomal protein genes retrieved by blast</prompt>
      <precond>
        <code proglang="python">rfiles</code>
      </precond>
      <filenames>
        <code proglang="python">"*.ribs"</code>
      </filenames>
    </parameter>
  </parameters>
</program>


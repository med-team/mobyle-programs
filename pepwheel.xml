<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>pepwheel</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>pepwheel</title>
      <description>
        <text lang="en">Draw a helical wheel diagram for a protein sequence</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/pepwheel.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>display:protein:2D_structure</category>
    <category>structure:2D_structure</category>
    <command>pepwheel</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,1</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_wheel</name>
          <prompt lang="en">Plot the wheel</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -nowheel", "")[ bool(value) ]</code>
          </format>
          <argpos>2</argpos>
        </parameter>

        <parameter>
          <name>e_steps</name>
          <prompt lang="en">Number of steps (value from 2 to 100)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>18</value>
          </vdef>
          <format>
            <code proglang="python">("", " -steps=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 2 is required</text>
            </message>
            <code proglang="python">value &gt;= 2</code>
          </ctrl>
          <ctrl>
            <message>
              <text lang="en">Value less than or equal to 100 is required</text>
            </message>
            <code proglang="python">value &lt;= 100</code>
          </ctrl>
          <argpos>3</argpos>
          <comment>
            <text lang="en">The number of residues plotted per turn is this value divided by the 'turns' value.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_turns</name>
          <prompt lang="en">Number of turns (value from 1 to 100)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>5</value>
          </vdef>
          <format>
            <code proglang="python">("", " -turns=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <ctrl>
            <message>
              <text lang="en">Value less than or equal to 100 is required</text>
            </message>
            <code proglang="python">value &lt;= 100</code>
          </ctrl>
          <argpos>4</argpos>
          <comment>
            <text lang="en">The number of residues plotted per turn is the 'steps' value divided by this value.</text>
          </comment>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_graph</name>
          <prompt lang="en">Choose the e_graph output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>png</value>
          </vdef>
          <vlist>
            <velem>
              <value>png</value>
              <label>Png</label>
            </velem>
            <velem>
              <value>gif</value>
              <label>Gif</label>
            </velem>
            <velem>
              <value>cps</value>
              <label>Cps</label>
            </velem>
            <velem>
              <value>ps</value>
              <label>Ps</label>
            </velem>
            <velem>
              <value>meta</value>
              <label>Meta</label>
            </velem>
            <velem>
              <value>data</value>
              <label>Data</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">(" -graph=" + str(vdef), " -graph=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>5</argpos>
        </parameter>

        <parameter>
          <name>e_goutfile</name>
          <prompt lang="en">Name of the output graph</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>pepwheel_graph</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -goutfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>6</argpos>
        </parameter>

        <parameter isout="1">
          <name>outgraph_png</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "png"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.png"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>outgraph_gif</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "gif"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.gif"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>outgraph_ps</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>PostScript</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "ps" or e_graph == "cps"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.ps"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>outgraph_meta</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "meta"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.meta"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>outgraph_data</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "data"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.dat"</code>
          </filenames>
        </parameter>

        <paragraph>
          <name>e_markupsection</name>
          <prompt lang="en">Markup section</prompt>

          <parameters>

            <parameter>
              <name>e_amphipathic</name>
              <prompt lang="en">Prompt for amphipathic residue marking</prompt>
              <type>
                <datatype>
                  <class>Boolean</class>
                </datatype>
              </type>
              <vdef>
                <value>0</value>
              </vdef>
              <format>
                <code proglang="python">("", " -amphipathic")[ bool(value) ]</code>
              </format>
              <argpos>7</argpos>
              <comment>
                <text lang="en">If this is true then the residues ACFGILMVWY are marked as squares and all other residues are unmarked. This overrides  any other markup that you may have specified using the  qualifiers '-squares', '-diamonds' and '-octags'.</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_squares</name>
              <prompt lang="en">Mark as squares</prompt>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <precond>
                <code proglang="python">not e_amphipathic</code>
              </precond>
              <vdef>
                <value>ILVM</value>
              </vdef>
              <format>
                <code proglang="python">("", " -squares=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>8</argpos>
              <comment>
                <text lang="en">By default the aliphatic residues ILVM are marked with squares.</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_diamonds</name>
              <prompt lang="en">Mark as diamonds</prompt>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <precond>
                <code proglang="python">not e_amphipathic</code>
              </precond>
              <vdef>
                <value>DENQST</value>
              </vdef>
              <format>
                <code proglang="python">("", " -diamonds=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>9</argpos>
              <comment>
                <text lang="en">By default the residues DENQST are marked with diamonds.</text>
              </comment>
            </parameter>

            <parameter>
              <name>e_octags</name>
              <prompt lang="en">Mark as octagons</prompt>
              <type>
                <datatype>
                  <class>String</class>
                </datatype>
              </type>
              <precond>
                <code proglang="python">not e_amphipathic</code>
              </precond>
              <vdef>
                <value>HKR</value>
              </vdef>
              <format>
                <code proglang="python">("", " -octags=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>10</argpos>
              <comment>
                <text lang="en">By default the positively charged residues HKR are marked with octagons.</text>
              </comment>
            </parameter>
          </parameters>
        </paragraph>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>11</argpos>
    </parameter>
  </parameters>
</program>

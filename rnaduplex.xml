<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>rnaduplex</name>
    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="Entities/ViennaRNA_package.xml"/>
    <doc>
      <title>RNAduplex</title>
      <description>
        <text lang="en">Compute the structure upon hybridization of two RNA strands</text>
      </description>
      <authors>Ivo Hofacker</authors>
      <comment>
        <text lang="en">RNAduplex reads  two RNA  sequences from  file and
       computes optimal and suboptimal secondary structures for their hybridization. The calculation is simplified  by  allowing  only inter-molecular base pairs.</text>
      </comment>
    </doc>
    <category>sequence:nucleic:2D_structure</category>
    <category>structure:2D_structure</category>
    <command>RNAduplex</command>
  </head>
  <parameters>
    <parameter ismandatory="1" issimple="1">
      <name>seq</name>
      <prompt lang="en">RNA Sequence File</prompt>
      <type>
        <biotype>DNA</biotype>
        <datatype>
          <class>Sequence</class>
        </datatype>
        <dataFormat>FASTA</dataFormat>
      </type>
      <format>
        <code proglang="perl">" &lt; $value" </code>
        <code proglang="python">" &lt; "+ str(value) </code>
      </format>
      <argpos>1000</argpos>
    </parameter>
    <paragraph>
      <name>control</name>
      <prompt lang="en">Control options</prompt>
      <argpos>2</argpos>
      <parameters>
        <parameter>
          <name>suboptimal</name>
          <prompt lang="en">Suboptimal structures (-e)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value!=$vdef)? " -e $value" : ""</code>
            <code proglang="python">( "" , " -e " + str(value))[ value is not None and value != vdef ]</code>
          </format>
          <comment>
            <text lang="en">Compute suboptimal structures with energy with range kcal/mol of the optimum. Default is calculation of mfe structure only.</text>
          </comment>
        </parameter>
        <parameter>
          <name>temperature</name>
          <prompt lang="en">Rescale energy parameters to a temperature of temp C. (-T)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>37</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef)? " -T $value" : ""</code>
            <code proglang="python">( "" , " -T " + str(value) )[ value is not None and value != vdef]</code>
          </format>
        </parameter>
        <parameter>
          <name>tetraloops</name>
          <prompt lang="en">Do not include special stabilizing energies for certain tetraloops (-4)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -4" : ""</code>
            <code proglang="python">( "" , " -4" )[ value ]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>input</name>
      <prompt lang="en">Input parameters</prompt>
      <argpos>2</argpos>
      <parameters>
        <parameter>
          <name>noGU</name>
          <prompt lang="en">Do not allow GU pairs (-noGU)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -noGU" : ""</code>
            <code proglang="python">( "" , " -noGU" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>noCloseGU</name>
          <prompt lang="en">Do not allow GU pairs at the end of helices (-noCloseGU)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -noCloseGU" : ""</code>
            <code proglang="python">( "" , " -noCloseGU" )[ value ]</code>
          </format>
        </parameter>
        <parameter>
          <name>parameter</name>
          <prompt lang="en">Energy parameter file (-P)</prompt>
          <type>
            <datatype>
              <class>EnergyParameterFile</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value)? " -P $value" : ""</code>
            <code proglang="python">( "" , " -P " + str(value) )[ value is not None ]</code>
          </format>
          <comment>
            <text lang="en">Read energy parameters from paramfile, instead of using the default parameter set. A sample parameterfile should accompany your distribution. See the RNAlib documentation for details on the file format.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>
    <parameter ishidden="1">
      <name>readseq</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="../../Local/Services/Programs/Env/ViennaRNA_readseq.xml" xpointer="xpointer(/readseq_path/text())"><xi:fallback/></xi:include>readseq  -f=19 -a $seq &gt; $seq.tmp &amp;&amp; (cp $seq $seq.orig &amp;&amp; mv $seq.tmp $seq) ; "</code>
        <code proglang="python">"<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="../../Local/Services/Programs/Env/ViennaRNA_readseq.xml" xpointer="xpointer(/readseq_path/text())"><xi:fallback/></xi:include>readseq  -f=19 -a "+ str(seq) + " &gt; "+ str(seq) +".tmp &amp;&amp; (cp "+ str(seq) +" "+ str(seq) +".orig &amp;&amp; mv "+ str(seq) +".tmp "+ str(seq) +") ; "</code>
      </format>
      <argpos>-10</argpos>
    </parameter>
    <parameter isout="1">
      <name>psfiles</name>
      <prompt>Postscript file</prompt>
      <type>
        <datatype>
          <class>PostScript</class>
          <superclass>Binary</superclass>
        </datatype>
      </type>
      <filenames>
        <code proglang="perl">"*.ps"</code>
        <code proglang="python">"*.ps"</code>
      </filenames>
    </parameter>
  </parameters>
</program>

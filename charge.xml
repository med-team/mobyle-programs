<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>charge</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>charge</title>
      <description>
        <text lang="en">Draw a protein charge plot</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/charge.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:protein:composition</category>
    <command>charge</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_seqall</name>
          <prompt lang="en">seqall option</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -seqall=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>

        <parameter>
          <name>e_aadata</name>
          <prompt lang="en">Amino acids properties and molecular weight data file</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>AminoAcidProperties</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -aadata=" + str(value))[value is not None ]</code>
          </format>
          <argpos>2</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_additional</name>
      <prompt lang="en">Additional section</prompt>

      <parameters>

        <parameter>
          <name>e_window</name>
          <prompt lang="en">Window length (value greater than or equal to 1)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>5</value>
          </vdef>
          <format>
            <code proglang="python">("", " -window=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <argpos>3</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_plot</name>
          <prompt lang="en">Produce graphic</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -plot")[ bool(value) ]</code>
          </format>
          <argpos>4</argpos>
        </parameter>

        <parameter>
          <name>e_graph</name>
          <prompt lang="en">Choose the e_graph output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot</code>
          </precond>
          <vdef>
            <value>png</value>
          </vdef>
          <vlist>
            <velem>
              <value>png</value>
              <label>Png</label>
            </velem>
            <velem>
              <value>gif</value>
              <label>Gif</label>
            </velem>
            <velem>
              <value>cps</value>
              <label>Cps</label>
            </velem>
            <velem>
              <value>ps</value>
              <label>Ps</label>
            </velem>
            <velem>
              <value>meta</value>
              <label>Meta</label>
            </velem>
            <velem>
              <value>data</value>
              <label>Data</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">(" -graph=" + str(vdef), " -graph=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>5</argpos>
        </parameter>

        <parameter>
          <name>xy_goutfile</name>
          <prompt lang="en">Name of the output graph</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot</code>
          </precond>
          <vdef>
            <value>charge_xygraph</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -goutfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>6</argpos>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_png</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "png"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.png"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_gif</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "gif"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.gif"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_ps</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>PostScript</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "ps" or e_graph == "cps"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.ps"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_meta</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "meta"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.meta"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_data</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "data"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.dat"</code>
          </filenames>
        </parameter>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the output file (e_outfile)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">not e_plot</code>
          </precond>
          <vdef>
            <value>charge.e_outfile</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>7</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>ChargeReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>8</argpos>
    </parameter>
  </parameters>
</program>

<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>abiview</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>abiview</title>
      <description>
        <text lang="en">Display the trace in an ABI sequencer file</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/abiview.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>display</category>
    <command>abiview</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_infile</name>
          <prompt lang="en">Abi sequencing trace file</prompt>
          <type>
            <datatype>
              <class>Binary</class>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -infile=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outseq</name>
          <prompt lang="en">Name of the output sequence file (e_outseq)</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>abiview.e_outseq</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outseq=" + str(value))[value is not None]</code>
          </format>
          <argpos>2</argpos>
        </parameter>

        <parameter>
          <name>e_osformat_outseq</name>
          <prompt lang="en">Choose the sequence output format</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>FASTA</value>
          </vdef>
          <vlist>
            <velem>
              <value>EMBL</value>
              <label>Embl</label>
            </velem>
            <velem>
              <value>FASTA</value>
              <label>Fasta</label>
            </velem>
            <velem>
              <value>GCG</value>
              <label>Gcg</label>
            </velem>
            <velem>
              <value>GENBANK</value>
              <label>Genbank</label>
            </velem>
            <velem>
              <value>NBRF</value>
              <label>Nbrf</label>
            </velem>
            <velem>
              <value>CODATA</value>
              <label>Codata</label>
            </velem>
            <velem>
              <value>RAW</value>
              <label>Raw</label>
            </velem>
            <velem>
              <value>SWISSPROT</value>
              <label>Swissprot</label>
            </velem>
            <velem>
              <value>GFF</value>
              <label>Gff</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -osformat=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>3</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outseq_out</name>
          <prompt lang="en">outseq_out option</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>
              <ref param="e_osformat_outseq">
              </ref>
            </dataFormat>
          </type>
          <filenames>
            <code proglang="python">e_outseq</code>
          </filenames>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_graph</name>
          <prompt lang="en">Choose the e_graph output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>png</value>
          </vdef>
          <vlist>
            <velem>
              <value>png</value>
              <label>Png</label>
            </velem>
            <velem>
              <value>gif</value>
              <label>Gif</label>
            </velem>
            <velem>
              <value>cps</value>
              <label>Cps</label>
            </velem>
            <velem>
              <value>ps</value>
              <label>Ps</label>
            </velem>
            <velem>
              <value>meta</value>
              <label>Meta</label>
            </velem>
            <velem>
              <value>data</value>
              <label>Data</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">(" -graph=" + str(vdef), " -graph=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>4</argpos>
        </parameter>

        <parameter>
          <name>xy_goutfile</name>
          <prompt lang="en">Name of the output graph</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>abiview_xygraph</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -goutfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>5</argpos>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_png</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "png"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.png"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_gif</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "gif"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.gif"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_ps</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>PostScript</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "ps" or e_graph == "cps"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.ps"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_meta</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "meta"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.meta"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_data</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "data"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.dat"</code>
          </filenames>
        </parameter>

        <parameter>
          <name>e_startbase</name>
          <prompt lang="en">First base to report or display (value greater than or equal to 0)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -startbase=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 0 is required</text>
            </message>
            <code proglang="python">value &gt;= 0</code>
          </ctrl>
          <argpos>6</argpos>
        </parameter>

        <parameter>
          <name>e_endbase</name>
          <prompt lang="en">Last base to report or display</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -endbase=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>7</argpos>
          <comment>
            <text lang="en">Last sequence base to report or display. If the default is set to zero then the value of this is taken as the maximum number  of bases.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_separate</name>
          <prompt lang="en">Separate the trace graphs for the 4 bases</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -separate")[ bool(value) ]</code>
          </format>
          <argpos>8</argpos>
        </parameter>

        <parameter>
          <name>e_yticks</name>
          <prompt lang="en">Display y-axis ticks</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -yticks")[ bool(value) ]</code>
          </format>
          <argpos>9</argpos>
        </parameter>

        <parameter>
          <name>e_sequence</name>
          <prompt lang="en">Display the sequence on the graph</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -nosequence", "")[ bool(value) ]</code>
          </format>
          <argpos>10</argpos>
        </parameter>

        <parameter>
          <name>e_window</name>
          <prompt lang="en">Sequence display window size</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>40</value>
          </vdef>
          <format>
            <code proglang="python">("", " -window=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>11</argpos>
        </parameter>

        <parameter>
          <name>e_bases</name>
          <prompt lang="en">Base graphs to be displayed</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <vdef>
            <value>GATC</value>
          </vdef>
          <format>
            <code proglang="python">("", " -bases=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>12</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>12</argpos>
    </parameter>
  </parameters>
</program>

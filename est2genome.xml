<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>est2genome</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>est2genome</title>
      <description>
        <text lang="en">Align EST sequences to genomic DNA sequence</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/est2genome.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>alignment:pairwise:global</category>
    <command>est2genome</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_estsequence</name>
          <prompt lang="en">Spliced est nucleotide sequence(s)</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -estsequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_genomesequence</name>
          <prompt lang="en">Additional section</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,1</card>
          </type>
          <format>
            <code proglang="python">("", " -genomesequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>2</argpos>
        </parameter>

        <parameter>
          <name>e_match</name>
          <prompt lang="en">Score for matching two bases</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">("", " -match=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>3</argpos>
        </parameter>

        <parameter>
          <name>e_mismatch</name>
          <prompt lang="en">Cost for mismatching two bases</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">("", " -mismatch=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>4</argpos>
        </parameter>

        <parameter>
          <name>e_gappenalty</name>
          <prompt lang="en">Gap penalty</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>2</value>
          </vdef>
          <format>
            <code proglang="python">("", " -gappenalty=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>5</argpos>
          <comment>
            <text lang="en">Cost for deleting a single base in either sequence, excluding introns</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_intronpenalty</name>
          <prompt lang="en">Intron penalty</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>40</value>
          </vdef>
          <format>
            <code proglang="python">("", " -intronpenalty=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>6</argpos>
          <comment>
            <text lang="en">Cost for an intron, independent of length.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_splicepenalty</name>
          <prompt lang="en">Splice site penalty</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>20</value>
          </vdef>
          <format>
            <code proglang="python">("", " -splicepenalty=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>7</argpos>
          <comment>
            <text lang="en">Cost for an intron, independent of length and starting/ending on donor-acceptor sites</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_minscore</name>
          <prompt lang="en">Minimum accepted score</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>30</value>
          </vdef>
          <format>
            <code proglang="python">("", " -minscore=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>8</argpos>
          <comment>
            <text lang="en">Exclude alignments with scores below this threshold score.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_advanced</name>
      <prompt lang="en">Advanced section</prompt>

      <parameters>

        <parameter>
          <name>e_reverse</name>
          <prompt lang="en">Reverse orientation</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -reverse")[ bool(value) ]</code>
          </format>
          <argpos>9</argpos>
          <comment>
            <text lang="en">Reverse the orientation of the EST sequence</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_usesplice</name>
          <prompt lang="en">Use donor and acceptor splice sites</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -nousesplice", "")[ bool(value) ]</code>
          </format>
          <argpos>10</argpos>
          <comment>
            <text lang="en">Use donor and acceptor splice sites. If you want to ignore donor-acceptor sites then set this to be false.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_mode</name>
          <prompt lang="en">Comparison mode</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>both</value>
          </vdef>
          <vlist>
            <velem>
              <value>both</value>
              <label>Both strands</label>
            </velem>
            <velem>
              <value>forward</value>
              <label>Forward strand only</label>
            </velem>
            <velem>
              <value>reverse</value>
              <label>Reverse strand only</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -mode=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>11</argpos>
          <comment>
            <text lang="en">This determines the comparison mode. The default value is 'both', in which case both strands of the est are compared  assuming a forward gene direction (ie GT/AG splice sites), and the  best comparison redone assuming a reversed (CT/AC) gene splicing  direction. The other allowed modes are 'forward', when just the  forward strand is searched, and 'reverse', ditto for the reverse  strand.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_best</name>
          <prompt lang="en">Print out only best alignment</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -nobest", "")[ bool(value) ]</code>
          </format>
          <argpos>12</argpos>
          <comment>
            <text lang="en">You can print out all comparisons instead of just the best one by setting this to be false.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_space</name>
          <prompt lang="en">Space threshold (in megabytes)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>10.0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -space=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>13</argpos>
          <comment>
            <text lang="en">For linear-space recursion. If product of sequence lengths divided by 4 exceeds this then a divide-and-conquer strategy is  used to control the memory requirements. In this way very long  sequences can be aligned. 
  If you have a machine with plenty of memory you can raise this  parameter (but do not exceed the machine's physical RAM)</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_shuffle</name>
          <prompt lang="en">Shuffle</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -shuffle=" + str(value))[value is not None]</code>
          </format>
          <argpos>14</argpos>
        </parameter>

        <parameter>
          <name>e_seed</name>
          <prompt lang="en">Random number seed</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>20825</value>
          </vdef>
          <format>
            <code proglang="python">("", " -seed=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>15</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the output file (e_outfile)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>est2genome.e_outfile</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>16</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>Est2genomeReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>

        <parameter>
          <name>e_align</name>
          <prompt lang="en">Show the alignment</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -align")[ bool(value) ]</code>
          </format>
          <argpos>17</argpos>
          <comment>
            <text lang="en">Show the alignment. The alignment includes the first and last 5 bases of each intron, together with the intron width. The  direction of splicing is indicated by angle brackets (forward or  reverse) or ???? (unknown).</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_width</name>
          <prompt lang="en">Alignment width</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>50</value>
          </vdef>
          <format>
            <code proglang="python">("", " -width=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>18</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>19</argpos>
    </parameter>
  </parameters>
</program>

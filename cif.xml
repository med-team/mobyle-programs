<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Sandrine Larroude                                              -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>cif</name>
    <version>0.2.2</version>
    <doc>
      <title>CIF</title>
      <description>
        <text lang="en">Cut DNA regions in frame</text>
      </description>
      <authors>E. Quevillon, B. Boeda</authors>
      <doclink>http://bioweb2.pasteur.fr/docs/cif/cif.html</doclink>
      <doclink>http://bioweb2.pasteur.fr/docs/cif/compatible_cohesive_ends.txt</doclink>
      <doclink>http://bioweb2.pasteur.fr/docs/cif/paillasse_liste.txt</doclink>
      <comment>
        <text lang="en">cif (for Cut In Frame) is a tool that works with DNA sequences. It is used to digest
      your sequences with a pool of restriction enzymes and to search which enzymes cut your
      sequence keeping your reading frame after ligation without any frame shift produced
      due to the digestion.</text>
        <text lang="en">This helps users to work with a gene of interest to localize potential region(s) that
      could be removed from the final protein to check if regions have an impact or not on
      the final gene product. It can also allow to identify vital region(s) for the gene.</text>
      </comment>
    </doc>
    <category>sequence:enzyme:analysis</category>
    <category>sequence:nucleic:restriction</category>
    <category>display:nucleic:restriction</category>
    <command>cif</command>
  </head>
  <parameters>
    <paragraph>
      <name>input</name>
      <prompt lang="en">Input section</prompt>
      <parameters>
        <parameter ismandatory="1" issimple="1">
          <name>sequence</name>
          <prompt lang="en">Sequence</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>FASTA</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="perl">" -i $value "</code>
            <code proglang="python">( "" , " -i " + str( value ) + " " )[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>options</name>
      <prompt lang="en">Options</prompt>
      <argpos>2</argpos>
      <parameters>
        <parameter issimple="1">
          <name>enztype</name>
          <prompt lang="en">Type of enzymes (-T)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <vlist>
            <velem undef="1">
              <value>0</value>
              <label>blunt + cohesive + klenow</label>
            </velem>
            <velem>
              <value>blunt</value>
              <label>blunt</label>
            </velem>
            <velem>
              <value>cohesive</value>
              <label>cohesive</label>
            </velem>
            <velem>
              <value>klenow</value>
              <label>klenow</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">($value) ?"-T $value ": ""</code>
            <code proglang="python">( "" , " -T " + str( value ) )[value != vdef]</code>
          </format>
          <comment>
            <text lang="en">You can choose between:</text>
            <text lang="en">- blunt : Use blunt cutters</text>
            <text lang="en">- cohesive : Use cohesive cutters</text>
            <text lang="en">- klenow  : Use only 5' enzymes for Klenow fill-in.</text>
            <text lang="en">[Default all three]</text>
          </comment>
        </parameter>
        <parameter issimple="1">
          <name>strand</name>
          <prompt lang="en">Cohesive enzyme strand (-S)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <vlist>
            <velem undef="1">
              <value>0</value>
              <label>5' + 3'</label>
            </velem>
            <velem>
              <value>5</value>
              <label>5'</label>
            </velem>
            <velem>
              <value>3</value>
              <label>3'</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">($value) ?"-S $value ": ""</code>
            <code proglang="python">( "" , " -S " + str( value ) )[value != vdef]</code>
          </format>
          <comment>
            <text lang="en">Cohesive enzyme strand to use: </text>
            <text lang="en"> - 5' </text>
            <text lang="en"> - 3' </text>
            <text lang="en"> - both : 5' and 3' (Default value).</text>
            <text lang="en">(No effect if you choose to use blunt enzymes)</text>
          </comment>
        </parameter>
        <parameter issimple="1">
          <name>digestionmod</name>
          <prompt lang="en"> Digestion mode (-D)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <vlist>
            <velem undef="1">
              <value>0</value>
              <label>simple + double</label>
            </velem>
            <velem>
              <value>double</value>
              <label>double</label>
            </velem>
            <velem>
              <value>simple</value>
              <label>simple</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">($value) ?"-D $value ": ""</code>
            <code proglang="python">( "" , " -D " + str( value ) )[value != vdef]</code>
          </format>
          <comment>
            <text lang="en">Digestion mode:</text>
            <text lang="en">- double: Report couple of enzymes that digest sequence</text>
            <text lang="en">- simple: Report enzyme name that cut more than one time</text>
            <text lang="en">- both: simple + double (Default value).</text>
          </comment>
        </parameter>
        <parameter>
          <name>Length</name>
          <prompt lang="en">Minimum length of recognition site (-L)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>6</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ?"-L $value ": ""</code>
            <code proglang="python">( "" , " -L " + str( value ) )[value is not None and value!=vdef]</code>
          </format>
          <comment>
            <text lang="en">Use enzymes with minimum length for DNA recognition site.</text>
            <text lang="en">By default, 6.</text>
          </comment>
        </parameter>
        <parameter>
          <name>variant</name>
          <prompt lang="en">Use enzymes with variant recognition site (-V)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ?"-V": ""</code>
            <code proglang="python">( "" , " -V " )[value]</code>
          </format>
          <comment>
            <text lang="en">Some cohesive enzymes have variant recognition site like 'GDGCH^C' for Bsp1286I, where:</text>
            <text lang="en">  D = not C (A or G or T) </text>
            <text lang="en">  H = not G (A or C or T)</text>
            <text lang="en">This option, when set, use of these type of enzymes..</text>
            <text lang="en">By default, this option is off.</text>
          </comment>
        </parameter>
        <parameter>
          <name>exotic</name>
          <prompt lang="en">Report digestions in frame without ends compatibilities (-X)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <precond>
            <code proglang="perl">$variant == 1</code>
            <code proglang="python">variant == 1</code>
          </precond>
          <format>
            <code proglang="perl">($value) ?"-X": ""</code>
            <code proglang="python">( "" , " -X " )[value]</code>
          </format>
          <comment>
            <text lang="en">Some cohesive enzymes have variant recognition site like 'GDGCH^C' for Bsp1286I, where:</text>
            <text lang="en">  D = not C (A or G or T) </text>
            <text lang="en">  H = not G (A or C or T)</text>
            <text lang="en">Thus, using thoses enzymes may produce a cut in frame but the produced ends may not be 
                compatible together regarding DNA sequence.</text>
            <text lang="en">Requires the '[-V | --variant]' option to work. By default, this option is off.</text>
          </comment>
        </parameter>
        <parameter>
          <name>compat</name>
          <prompt lang="en">List of compatible cohesive ends (-C)</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">($value) ?"-C $value ": ""</code>
            <code proglang="python">( "" , " -C " + str( value ) )[value is not None]</code>
          </format>
          <comment>
            <text lang="en">File with list of compatible cohesive ends.</text>
            <text lang="en">The default list used is given in the program help pages (compatible_cohesive_ends.txt).</text>
            <text lang="en">If you want to give your own list, the format must be as follow:</text>
            <text lang="en">"Enzyme_name:compatEnz1_name,compatEnz2_name,..."</text>
          </comment>
          <example>
Acc65I:BanI,BsiWI,BsrGI
AccI:AciI,AclI,BsaHI,HinP1I,HpaII,NarI,ClaI,BstBI,TaqI          	
          	</example>
        </parameter>
        <parameter>
          <name>enzlist</name>
          <prompt lang="en">Enzyme list to work with (-E)</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">($value) ?"-E $value ": ""</code>
            <code proglang="python">( "" , " -E " + str( value ) )[value is not None]</code>
          </format>
          <comment>
            <text lang="en">By default, the program works with a list of enzymes commonly used in laboratory 
          		given in the program help pages (paillasse_liste.txt).</text>
            <text lang="en">If you want to give your own list, the format is one enzyme per line.</text>
          </comment>
          <example>
AatII
Acc651
...
          	</example>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>outputopt</name>
      <prompt lang="en">Output parameters</prompt>
      <argpos>3</argpos>
      <parameters>
        <parameter>
          <name>stop</name>
          <prompt lang="en">Show stop codon (-P)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <precond>
            <code proglang="perl">$enztype == 0 or $enztype eq 'blunt'</code>
            <code proglang="python">enztype == 0 or enztype == 'blunt'</code>
          </precond>
          <format>
            <code proglang="perl">($value) ?"-P": ""</code>
            <code proglang="python">( "" , " -P " )[value]</code>
          </format>
          <comment>
            <text lang="en">Sometimes, blunt digestion, after linkage, can produce new codon around the cutting site 
            that leads to stop codon.</text>
            <text lang="en">This option displays such digestions with a tag 'stopCodon' in the output line results.</text>
            <text lang="en">NOTE: This option only works if 'blunt' type is set. By default this option is off, thus 
            if such case happened no results are reported for enzymes digestion.</text>
          </comment>
        </parameter>
        <parameter issimple="1">
          <name>cut_pos</name>
          <prompt lang="en">Show cut positions (-N)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ?"-N": ""</code>
            <code proglang="python">( "" , " -N " )[value]</code>
          </format>
          <comment>
            <text lang="en"> Enzymes may cut your sequence more than once.This option 
            reports the number of time enzyme(s) cut your sequence. [Default off]</text>
          </comment>
        </parameter>
        <parameter>
          <name>mod_aa</name>
          <prompt lang="en">Show new generated amino acid (-A)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <precond>
            <code proglang="perl">$enztype ne 'cohesive'</code>
            <code proglang="python">enztype != 'cohesive'</code>
          </precond>
          <format>
            <code proglang="perl">($value) ?"-A": ""</code>
            <code proglang="python">( "" , " -A " )[value]</code>
          </format>
          <comment>
            <text lang="en">Experimental option. [Default off]</text>
            <text lang="en">This option allows, for 'blunt' or 'klenow' analysis to
            show, in such case, the Amino acid that have been changed due to the ligation between 
            the 2 parts of the DNA after the digestion.</text>
            <text lang="en">It will be shown as OldAA&gt;NewAA (e.g.: G&gt;N).</text>
          </comment>
        </parameter>
        <parameter issimple="1">
          <name>outputstyle</name>
          <prompt lang="en">Output style format (-F)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>text</value>
          </vdef>
          <vlist>
            <velem undef="1">
              <value>text</value>
              <label>Text output</label>
            </velem>
            <velem>
              <value>gff</value>
              <label>GFF3 output</label>
            </velem>
            <velem>
              <value>image</value>
              <label>Image (png)</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">($value) ?"-F $value ": ""</code>
            <code proglang="python">( "" , " -F " + str( value ) )[value != vdef]</code>
          </format>
          <comment>
            <text lang="en">Choose the output type that you prefer.</text>
            <text lang="en">- Text output (Default value)</text>
            <text lang="en">- GFF3 output</text>
            <text lang="en">- Image (png): Creates image (png).</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>
    <parameter isout="1">
      <name>img</name>
      <prompt>Output image</prompt>
      <type>
        <datatype>
          <class>Picture</class>
          <superclass>Binary</superclass>
        </datatype>
        <card>1,n</card>
      </type>
      <precond>
        <code proglang="perl">$outputstyle eq 'image'</code>
        <code proglang="python">outputstyle == 'image'</code>
      </precond>
      <filenames>
        <code proglang="perl">*.png</code>
        <code proglang="python">"*.png"</code>
      </filenames>
    </parameter>
  </parameters>
</program>


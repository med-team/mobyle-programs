<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>cpgplot</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>cpgplot</title>
      <description>
        <text lang="en">Identify and plot CpG islands in nucleotide sequence(s)</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/cpgplot.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:nucleic:cpg_islands</category>
    <command>cpgplot</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_required</name>
      <prompt lang="en">Required section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_window</name>
          <prompt lang="en">Window size (value greater than or equal to 1)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>100</value>
          </vdef>
          <format>
            <code proglang="python">("", " -window=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <argpos>2</argpos>
          <comment>
            <text lang="en">The percentage CG content and the Observed frequency of CG is calculated within a window whose size is set by this parameter.  The window is moved down the sequence and these statistics are  calculated at each position that the window is moved to.</text>
          </comment>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_minlen</name>
          <prompt lang="en">Minimum length of an island (value greater than or equal to 1)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>200</value>
          </vdef>
          <format>
            <code proglang="python">("", " -minlen=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <argpos>3</argpos>
          <comment>
            <text lang="en">This sets the minimum length that a CpG island has to be before it is reported.</text>
          </comment>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_minoe</name>
          <prompt lang="en">Minimum observed/expected (value from 0. to 10.)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.6</value>
          </vdef>
          <format>
            <code proglang="python">("", " -minoe=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 0. is required</text>
            </message>
            <code proglang="python">value &gt;= 0.</code>
          </ctrl>
          <ctrl>
            <message>
              <text lang="en">Value less than or equal to 10. is required</text>
            </message>
            <code proglang="python">value &lt;= 10.</code>
          </ctrl>
          <argpos>4</argpos>
          <comment>
            <text lang="en">This sets the minimum average observed to expected ratio of C plus G to CpG in a set of 10 windows that are required before a  CpG island is reported.</text>
          </comment>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_minpc</name>
          <prompt lang="en">Minimum percentage (value from 0. to 100.)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>50.</value>
          </vdef>
          <format>
            <code proglang="python">("", " -minpc=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 0. is required</text>
            </message>
            <code proglang="python">value &gt;= 0.</code>
          </ctrl>
          <ctrl>
            <message>
              <text lang="en">Value less than or equal to 100. is required</text>
            </message>
            <code proglang="python">value &lt;= 100.</code>
          </ctrl>
          <argpos>5</argpos>
          <comment>
            <text lang="en">This sets the minimum average percentage of G plus C a set of 10 windows that are required before a CpG island is reported.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the output file (e_outfile)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>cpgplot.e_outfile</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>6</argpos>
          <comment>
            <text lang="en">This sets the name of the file holding the report of the input sequence name, CpG island parameters and the output details  of any CpG islands that are found.</text>
          </comment>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>CpgplotReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>

        <parameter>
          <name>e_plot</name>
          <prompt lang="en">Plot cpg island score</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -noplot", "")[ bool(value) ]</code>
          </format>
          <argpos>7</argpos>
        </parameter>

        <parameter>
          <name>e_graph</name>
          <prompt lang="en">Choose the e_graph output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot</code>
          </precond>
          <vdef>
            <value>png</value>
          </vdef>
          <vlist>
            <velem>
              <value>png</value>
              <label>Png</label>
            </velem>
            <velem>
              <value>gif</value>
              <label>Gif</label>
            </velem>
            <velem>
              <value>cps</value>
              <label>Cps</label>
            </velem>
            <velem>
              <value>ps</value>
              <label>Ps</label>
            </velem>
            <velem>
              <value>meta</value>
              <label>Meta</label>
            </velem>
            <velem>
              <value>data</value>
              <label>Data</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">(" -graph=" + str(vdef), " -graph=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>8</argpos>
        </parameter>

        <parameter>
          <name>xy_goutfile</name>
          <prompt lang="en">Name of the output graph</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot</code>
          </precond>
          <vdef>
            <value>cpgplot_xygraph</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -goutfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>9</argpos>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_png</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "png"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.png"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_gif</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "gif"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.gif"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_ps</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>PostScript</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "ps" or e_graph == "cps"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.ps"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_meta</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "meta"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.meta"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_data</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_plot and e_graph == "data"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.dat"</code>
          </filenames>
        </parameter>

        <parameter>
          <name>e_obsexp</name>
          <prompt lang="en">Show observed/expected threshold line</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -noobsexp", "")[ bool(value) ]</code>
          </format>
          <argpos>10</argpos>
          <comment>
            <text lang="en">If this is set to true then the graph of the observed to expected ratio of C plus G to CpG within a window is displayed.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_cg</name>
          <prompt lang="en">Show cpg rich regions</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -nocg", "")[ bool(value) ]</code>
          </format>
          <argpos>11</argpos>
          <comment>
            <text lang="en">If this is set to true then the graph of the regions which have been determined to be CpG islands is displayed.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_pc</name>
          <prompt lang="en">Show percentage line</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -nopc", "")[ bool(value) ]</code>
          </format>
          <argpos>12</argpos>
          <comment>
            <text lang="en">If this is set to true then the graph of the percentage C plus G within a window is displayed.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_outfeat</name>
          <prompt lang="en">Name of the output feature file (e_outfeat)</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>cpgplot.e_outfeat</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfeat=" + str(value))[value is not None]</code>
          </format>
          <argpos>13</argpos>
          <comment>
            <text lang="en">File for output features</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_offormat_outfeat</name>
          <prompt lang="en">Choose the feature output format</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>GFF</value>
          </vdef>
          <vlist>
            <velem>
              <value>GFF</value>
              <label>Gff</label>
            </velem>
            <velem>
              <value>EMBL</value>
              <label>Embl</label>
            </velem>
            <velem>
              <value>SWISSPROT</value>
              <label>Swissprot</label>
            </velem>
            <velem>
              <value>NBRF</value>
              <label>Nbrf</label>
            </velem>
            <velem>
              <value>CODATA</value>
              <label>Codata</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -offormat=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>14</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfeat_out</name>
          <prompt lang="en">outfeat_out option</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Feature</class>
              <superclass>AbstractText</superclass>
            </datatype>
            <dataFormat>
              <ref param="e_offormat_outfeat">
              </ref>
            </dataFormat>
          </type>
          <filenames>
            <code proglang="python">e_outfeat</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>15</argpos>
    </parameter>
  </parameters>
</program>

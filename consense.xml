<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>consense</name>
    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="Entities/phylip_package.xml"/>
    <doc>
      <title>consense</title>
      <description>
        <text lang="en">Consensus tree program</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/phylip/doc/consense.html</doclink>
      <comment>
        <text lang="en">CONSENSE reads a  file of  computer-readable trees
	and prints out (and may also write out onto a file) a consensus tree.</text>
      </comment>
    </doc>
    <category>phylogeny:tree_analyser</category>
  </head>
  <parameters>
    <parameter iscommand="1" ishidden="1">
      <name>consense</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"consense &lt;consense.params"</code>
        <code proglang="python">"consense &lt;consense.params"</code>
      </format>
      <argpos>0</argpos>
    </parameter>
    <parameter ismandatory="1" issimple="1" ismaininput="1">
      <name>infile</name>
      <prompt lang="en">Series of trees in file (intree)</prompt>
      <type>
        <datatype>
          <class>Tree</class>
        </datatype>
        <dataFormat>NEWICK</dataFormat>
      </type>
      <format>
        <code proglang="perl">"ln -s $infile intree &amp;&amp; "</code>
        <code proglang="python">"ln -s "+ str( infile )+ " intree &amp;&amp;"</code>
      </format>
      <argpos>-10</argpos>
      <comment>
        <text lang="en">Input is a tree file which contains a series of trees in the Newick standard form</text>
      </comment>
      <example>(A,(B,(H,(D,(J,(((G,E),(F,I)),C))))));
(A,(B,(D,((J,H),(((G,E),(F,I)),C)))));
(A,(B,(D,(H,(J,(((G,E),(F,I)),C))))));
(A,(B,(E,(G,((F,I),((J,(H,D)),C))))));
(A,(B,(E,(G,((F,I),(((J,H),D),C))))));
(A,(B,(E,((F,I),(G,((J,(H,D)),C))))));
(A,(B,(E,((F,I),(G,(((J,H),D),C))))));
(A,(B,(E,((G,(F,I)),((J,(H,D)),C)))));
(A,(B,(E,((G,(F,I)),(((J,H),D),C)))));</example>
    </parameter>
    <parameter>
      <name>type</name>
      <prompt lang="en">Consensus type</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>MRE</value>
      </vdef>
      <flist>
        <felem>
          <value>MRE</value>
          <label>Majority rule (extended)</label>
          <code proglang="perl">""</code>
          <code proglang="python">""</code>
        </felem>
        <felem>
          <value>S</value>
          <label>Strict</label>
          <code proglang="perl">"C\\n"</code>
          <code proglang="python">"C\n"</code>
        </felem>
        <felem>
          <value>MR</value>
          <label>Majority rule</label>
          <code proglang="perl">"C\\nC\\n"</code>
          <code proglang="python">"C\nC\n"</code>
        </felem>
        <felem>
          <value>ML</value>
          <label>Ml (M-sub-L)</label>
          <code proglang="perl">"C\\nC\\nC\\n"</code>
          <code proglang="python">"C\nC\nC\n"</code>
        </felem>
      </flist>
      <paramfile>consense.params</paramfile>
    </parameter>
    <paragraph>
      <name>output</name>
      <prompt lang="en">Output options</prompt>
      <parameters>
        <parameter>
          <name>print_tree</name>
          <prompt lang="en">Print out tree (3)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? "" : "3\\n"</code>
            <code proglang="python">( "3\n" , "" )[ value ]</code>
          </format>
          <argpos>1</argpos>
          <comment>
            <text lang="en">Tells the program to print a semi-graphical picture of the tree in the outfile.</text>
          </comment>
          <paramfile>consense.params</paramfile>
        </parameter>
        <parameter>
          <name>print_treefile</name>
          <prompt lang="en">Write out trees onto tree file (4)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? "" : "4\\n"</code>
            <code proglang="python">( "4\n" , "" )[ value ]</code>
          </format>
          <argpos>1</argpos>
          <comment>
            <text lang="en">Tells the program to save the tree in a treefile (a standard representation of trees where the tree is specified by a nested pairs of parentheses, enclosing names and separated by commas).</text>
          </comment>
          <paramfile>consense.params</paramfile>
        </parameter>
        <parameter>
          <name>printdata</name>
          <prompt lang="en">Print out the data at start of run (1)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? "1\\n" : ""</code>
            <code proglang="python">( "" , "1\n" )[ value ]</code>
          </format>
          <argpos>1</argpos>
          <paramfile>consense.params</paramfile>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>other_options</name>
      <prompt lang="en">Other options</prompt>
      <parameters>
        <parameter>
          <name>outgroup</name>
          <prompt lang="en">Outgroup species (O)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? "O\\n$value\\n" : ""</code>
            <code proglang="python">( "" , "O\n"+ str( value ) +"\n" )[ value is not None and value != vdef ]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Please enter a value greater than 0</text>
            </message>
            <code proglang="perl">$value &gt; 0</code>
            <code proglang="python">value &gt; 0</code>
          </ctrl>
          <argpos>1</argpos>
          <paramfile>consense.params</paramfile>
        </parameter>
        <parameter>
          <name>rooted</name>
          <prompt lang="en">Trees to be treated as rooted (R)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? "R\\n" : ""</code>
            <code proglang="python">( "" , "R\n" )[ value ]</code>
          </format>
          <argpos>1</argpos>
          <paramfile>consense.params</paramfile>
        </parameter>
      </parameters>
    </paragraph>
    <parameter isout="1">
      <name>outfile</name>
      <prompt>Consense output file</prompt>
      <type>
        <datatype>
          <class>Text</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">" &amp;&amp; mv outfile consense.outfile"</code>
        <code proglang="python">" &amp;&amp; mv outfile consense.outfile "</code>
      </format>
      <argpos>40</argpos>
      <filenames>
        <code proglang="perl">"consense.outfile"</code>
        <code proglang="python">"consense.outfile"</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>treefile</name>
      <prompt>Consense tree file</prompt>
      <type>
        <datatype>
          <class>Tree</class>
        </datatype>
        <dataFormat>NEWICK</dataFormat>
      </type>
      <precond>
        <code proglang="perl">$print_treefile</code>
        <code proglang="python">print_treefile</code>
      </precond>
      <format>
        <code proglang="perl">" &amp;&amp; mv outtree consense.outtree"</code>
        <code proglang="python">" &amp;&amp; mv outtree consense.outtree"</code>
      </format>
      <argpos>50</argpos>
      <filenames>
        <code proglang="perl">"consense.outtree"</code>
        <code proglang="python">"consense.outtree"</code>
      </filenames>
    </parameter>
    <parameter ishidden="1">
      <name>confirm</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"Y\\n"</code>
        <code proglang="python">"Y\n"</code>
      </format>
      <argpos>1000</argpos>
      <paramfile>consense.params</paramfile>
    </parameter>
    <parameter ishidden="1">
      <name>terminal_type</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"T\\n"</code>
        <code proglang="python">"T\n"</code>
      </format>
      <argpos>-1</argpos>
      <paramfile>consense.params</paramfile>
    </parameter>
  </parameters>
</program>

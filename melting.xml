<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>melting</name>
    <version>4.2g</version>
    <doc>
      <title>MELTING</title>
      <description>
        <text lang="en">enthalpy, entropy and melting temperature</text>
      </description>
      <authors>N. Le Novere</authors>
      <reference>Nicolas Le Novere (2001), MELTING, computing the melting temperature of nucleic acid duplex. Bioinformatics 17(12), 1226-1227</reference>
      <doclink>http://bioweb2.pasteur.fr/docs/melting/melting.pdf</doclink>
      <homepagelink>http://www.ebi.ac.uk/~lenov/meltinghome.html</homepagelink>
      <sourcelink>http://www.ebi.ac.uk/~lenov/SOFTWARES/</sourcelink>
    </doc>
    <category>sequence:nucleic:composition</category>
  </head>
  <parameters>
    <parameter iscommand="1" ishidden="1">
      <name>melting</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"melting -q -v"</code>
        <code proglang="python">"melting -q -v"</code>
      </format>
      <argpos>0</argpos>
    </parameter>
    <parameter ismandatory="1" issimple="1">
      <name>hybridation_type</name>
      <prompt lang="en">Hybridisation type (-H)</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>null</value>
      </vdef>
      <vlist>
        <velem undef="1">
          <value>null</value>
          <label>Choose an hybidation type</label>
        </velem>
        <velem>
          <value>dnadna</value>
          <label>Dna/Dna</label>
        </velem>
        <velem>
          <value>dnarna</value>
          <label>Dna/Rna</label>
        </velem>
        <velem>
          <value>rnarna</value>
          <label>Rna/Rna</label>
        </velem>
      </vlist>
      <format>
        <code proglang="perl">" -H$value"</code>
        <code proglang="python">" -H" + str(value)</code>
      </format>
      <argpos>1</argpos>
    </parameter>
    <parameter ismandatory="1" issimple="1">
      <name>nnfile</name>
      <prompt lang="en">Nearest Neighbor parameters set (-A)</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>default</value>
      </vdef>
      <vlist>
        <velem>
          <value>default</value>
          <label>Default</label>
        </velem>
        <velem>
          <value>all97a.nn</value>
          <label>DNA/DNA hybridisation of Allawi et al 1997</label>
        </velem>
        <velem>
          <value>bre86a.nn</value>
          <label>DNA/DNA hybridisation of Breslauer et al 1986</label>
        </velem>
        <velem>
          <value>san96a.nn</value>
          <label>DNA/DNA hybridisation of SantaLucia et al 1996</label>
        </velem>
        <velem>
          <value>sug96a.nn</value>
          <label>DNA/DNA hybridisation of Sugimoto et al 1996</label>
        </velem>
        <velem>
          <value>fre86a.nn</value>
          <label>RNA/RNA hybridisation of Freier et al 1986</label>
        </velem>
        
        <velem>
          <value>xia98a.nn</value>
          <label>RNA/RNA hybridisation of Xia et al 1998</label>
        </velem>
        <velem>
          <value>sug95a.nn</value>
          <label>DNA/RNA hybridisation of Sugimoto et al 1995</label>
        </velem>
      </vlist>
      <format>
        <code proglang="perl">(defined $value and $value ne $vdef) ? " -A$value" : ""</code>
        <code proglang="python">( "" , " -A" + str(value) )[ value is not None and value != vdef]</code>
      </format>
      <comment>
        <div xmlns="http://www.w3.org/1999/xhtml">
            Informs the program to use file.nn as an alternative set of nearest-neighbor parameters, 
            rather than the default for the specified hybridisation type (option -H).
            melting provides some files ready-to-use:
            <ul>
               <li>all97a.nn (DNA/DNA hybridisation of Allawi and SantaLucia(1997). Biochemistry 36 : 10581-10594)</li>
               <li>bre86a.nn (DNA/DNA hybridisation of Breslauer et al. (1986). Proc Natl Acad Sci USA 83 : 3746-3750)</li>
               <li>san96a.nn (DNA/DNA hybridisation of SantaLucia et al.(1996). Biochemistry 35 : 3555-3562)</li>
               <li>sug96a.nn (DNA/DNA hybridisation of Sugimoto et al.(1996). Nuc Acids Res 24 : 4501-4505)</li>
               <li>fre86a.nn (RNA/RNA hybridisation of Freier et al (1986) Proc Natl Acad Sci USA 83: 9373-9377)</li>
               <li>xia98a.nn (RNA/RNA hybridisation of Xia et al (1998) Biochemistry 37: 14719-14735)</li>
               <li>sug95a.nn (DNA/RNA hybridisation of Sugimoto et al. (1995). Biochemistry 34 : 11211-11216)</li>
            </ul>
            Be careful, the option -A changes the default parameter set defined by the option -H.
        </div>
      </comment>
      <argpos>1</argpos>
    </parameter>
    <parameter ismandatory="1" issimple="1">
      <name>sequence</name>
      <prompt lang="en">Sequence string (-S)</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">" -S$value"</code>
        <code proglang="python">" -S" + str(value)</code>
      </format>
      <argpos>1</argpos>
    </parameter>
    <parameter>
      <name>complement_string</name>
      <prompt lang="en">Complementary sequence (-C)</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">(defined $value) ? " -C$value" : ""</code>
        <code proglang="python">( "" , " -C" + str(value) )[ value is not None ]</code>
      </format>
      <comment>
        <div  xmlns="http://www.w3.org/1999/xhtml">
          Enters the complementary sequence, from 3’ to 5’. 
          This option is mandatory if there are mismatches between the two strands. 
          If it is not used, the program will compute it as the complement of the
          sequence entered with the option -S.
          <pre>
            5' GTGAGCTCAT 3'
            3' CACTCGAGTA 5'
         </pre>   
        </div>
      </comment>
      <argpos>1</argpos>
    </parameter>
    <parameter ismandatory="1" issimple="1">
      <name>salt_concentration</name>
      <prompt lang="en">Salt concentration (-N)</prompt>
      <type>
        <datatype>
          <class>Float</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">" -N$value"</code>
        <code proglang="python">" -N" + str(value)</code>
      </format>
      <ctrl>
        <message>
          <text lang="en">Must be greater than 0.0 and lower than 10.0</text>
        </message>
        <code proglang="perl">$value &gt; 0.0 and $value &lt; 10.0</code>
        <code proglang="python">value &gt; 0.0 and value &lt; 10.0</code>
      </ctrl>
      <argpos>1</argpos>
      <comment>
        <text lang="en">Value must be greater than 0.0 and lower than 10.0</text>
      </comment>
    </parameter>
    <parameter ismandatory="1" issimple="1">
      <name>nucacid_concentration</name>
      <prompt lang="en">Nucleic acid concentration in excess (-P)</prompt>
      <type>
        <datatype>
          <class>Float</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">(defined $value) ? " -P$value" : ""</code>
        <code proglang="python">("", " -P" + str(value))[value is not None]</code>
      </format>
      <ctrl>
        <message>
          <text lang="en">Must be greater than 0.0 and lower than 0.1</text>
        </message>
        <code proglang="perl">$value &gt; 0.0 and $value &lt; 0.1</code>
        <code proglang="python">value &gt; 0.0 and value &lt; 0.1</code>
      </ctrl>
      <argpos>1</argpos>
      <comment>
        <text lang="en">Value must be greater than 0.0 and lower than 0.1</text>
      </comment>
    </parameter>
    <parameter>
      <name>correction_factor</name>
      <prompt lang="en">Nucleic acid correction factor (-F)</prompt>
      <type>
        <datatype>
          <class>Float</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">(defined $value) ? " -F$value" : ""</code>
        <code proglang="python">( "" , " -F" + str(value) )[ value is not None ]</code>
      </format>
      <argpos>1</argpos>
    </parameter>
    <parameter>
      <name>salt_correction</name>
      <prompt lang="en">Salt correction (-K)</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>null</value>
      </vdef>
      <vlist>
        <velem undef="1">
          <value>null</value>
          <label/>
        </velem>
        <velem>
          <value>wet91a</value>
          <label>Wetmur 1991</label>
        </velem>
        <velem>
          <value>san96a</value>
          <label>SantaLucia et al. 1996</label>
        </velem>
        <velem>
          <value>san98a</value>
          <label>SantaLucia 1998</label>
        </velem>
      </vlist>
      <format>
        <code proglang="perl">(defined $value) ? " -K$value" : ""</code>
        <code proglang="python">( "" , " -K" + str(value) )[ value is not None ]</code>
      </format>
      <argpos>1</argpos>
    </parameter>
    <parameter>
      <name>approx</name>
      <prompt lang="en">Force approximative temperature computation (-x)</prompt>
      <type>
        <datatype>
          <class>Boolean</class>
        </datatype>
      </type>
      <vdef>
        <value>0</value>
      </vdef>
      <format>
        <code proglang="perl">($value) ? " -x" : ""</code>
        <code proglang="python">( "" , " -x" )[ value ]</code>
      </format>
      <argpos>1</argpos>
    </parameter>
    <parameter>
      <name>dangling_ends</name>
      <prompt lang="en">Use parameters for dangling ends (dnadnade.nn) (-D)?</prompt>
      <type>
        <datatype>
          <class>Boolean</class>
        </datatype>
      </type>
      <vdef>
        <value>0</value>
      </vdef>
      <format>
        <code proglang="perl">($value) ? " -Ddnadnade.nn " : ""</code>
        <code proglang="python">( "" , " -Ddnadnade.nn " )[ value ]</code>
      </format>
      <argpos>1</argpos>
    </parameter>
    <parameter>
      <name>mismatches</name>
      <prompt lang="en">Use parameters for mismatches (dnadnamm.nn) (-M)?</prompt>
      <type>
        <datatype>
          <class>Boolean</class>
        </datatype>
      </type>
      <vdef>
        <value>0</value>
      </vdef>
      <format>
        <code proglang="perl">($value) ? " -Mdnadnamm.nn " : ""</code>
        <code proglang="python">( "" , " -Mdnadnamm.nn " )[ value ]</code>
      </format>
      <argpos>1</argpos>
    </parameter>
  </parameters>
</program>


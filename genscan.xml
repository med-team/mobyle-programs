<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>genscan</name>
    <version>1.0</version>
    <doc>
      <title>GENSCAN</title>
      <description>
        <text lang="en">Gene Identification</text>
      </description>
      <authors>C. Burge</authors>
      <reference>Burge, C., Karlin, S. (1997) Prediction of complete gene structures in human genomic DNA. J. Mol. Biol. 268, 78-94.</reference>
      <reference>Burge, C., Karlin, S. (1997) Gene structure, exon prediction, and alternative splicing. (in preparation).</reference>
      <reference>Burge, C. (1997) Identification of genes in human genomic DNA. PhD thesis, Stanford University, Stanford, CA.</reference>
      <reference>Burset, M., Guigo, R. (1996) Evaluation of gene structure prediction programs. Genomics 34, 353-367.</reference>
      <homepagelink>http://genes.mit.edu/GENSCANinfo.html</homepagelink>
    </doc>
    <category>sequence:nucleic:gene_finding</category>
    <command>genscan</command>
  </head>
  <parameters>
    <parameter ismandatory="1" issimple="1">
      <name>seq</name>
      <prompt lang="en">DNA Sequence File</prompt>
      <type>
        <biotype>DNA</biotype>
        <datatype>
          <class>Sequence</class>
        </datatype>
        <dataFormat>FASTA</dataFormat>
      </type>
      <format>
        <code proglang="perl">" $value"</code>
        <code proglang="python">" " + str(value)</code>
      </format>
      <argpos>2</argpos>
    </parameter>
    <parameter ismandatory="1" issimple="1">
      <name>organism</name>
      <prompt lang="en">Organism</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>null</value>
      </vdef>
      <vlist>
        <velem undef="1">
          <value>null</value>
          <label>Choose an organism</label>
        </velem>
        <velem>
          <value>Arabidopsis.smat</value>
          <label>Arabidopsis</label>
        </velem>
        <velem>
          <value>HumanIso.smat</value>
          <label>HumanIso</label>
        </velem>
        <velem>
          <value>Maize.smat</value>
          <label>Maize</label>
        </velem>
      </vlist>
      <format>
        <code proglang="perl">" $value"</code>
        <code proglang="python">" "+str(value)</code>
      </format>
      <argpos>1</argpos>
      <comment>
        <text lang="en">Currently available parameter files are:</text>
        <text lang="en">HumanIso.smat for human/vertebrate sequences (also Drosophila)</text>
        <text lang="en">Arabidopsis.smat for Arabidopsis thaliana sequences</text>
        <text lang="en">Maize.smat for Zea mays sequences</text>
      </comment>
    </parameter>
    <paragraph>
      <name>output</name>
      <prompt lang="en">Output parameters</prompt>
      <argpos>3</argpos>
      <parameters>
        <parameter>
          <name>verbose</name>
          <prompt lang="en">Verbose output (-v)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -v" : ""</code>
            <code proglang="python">("" , " -v")[ value ]</code>
          </format>
          <argpos>3</argpos>
          <comment>
            <text lang="en">Add some extra explanatory information to the text output. This information may be helpful the first few times you run the program but will soon become tiresome (that's why its optional).</text>
          </comment>
        </parameter>
        <parameter>
          <name>cds</name>
          <prompt lang="en">Print predicted coding sequences (-cds)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -cds" : ""</code>
            <code proglang="python">("" , " -cds")[ value ]</code>
          </format>
          <argpos>3</argpos>
        </parameter>
        <parameter>
          <name>subopt</name>
          <prompt lang="en">Identify suboptimal exons (-subopt)?</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value and defined $cutoff ) ? " -subopt $cutoff" : ""</code>
            <code proglang="python">("" , " -subopt " + str( cutoff ))[ value and cutoff is not None]</code>
          </format>
          <argpos>3</argpos>
          <comment>
            <text lang="en">The default output of the program is the optimal 'parse' of the sequence, i.e. the highest probability gene structure(s) which is present: the exons in this optimal parse are referred to as 'optimal exons' and are always printed out by GENSCAN.</text>
            <text lang="en">Suboptimal exons, on the other hand, are defined as potential exons which have probability above a certian threshold but which are not contained in the optimal parse of the sequence.</text>
            <text lang="en">Suboptimal exons have a variety of potential uses.</text>
            <text lang="en">First, suboptimal exons sometimes correspond to real exons which were missed for whatever reason by the optimal parse of the sequence.</text>
            <text lang="en">Second, regions of a prediction which contain multiple overlapping and/or incompatible optimal and suboptimal exons may in some cases indicate alternatively spliced regions of a gene (Burge &amp; Karlin, in preparation).</text>
          </comment>
        </parameter>
        <parameter ismandatory="1">
          <name>cutoff</name>
          <prompt lang="en">Cutoff value for suboptimal exons</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$subopt</code>
            <code proglang="python">subopt</code>
          </precond>
          <ctrl>
            <message>
              <text lang="en">Choose a cutoff value for identify suboptimal exon</text>
            </message>
            <code proglang="perl">($subopt  and defined $value) or ( not $subopt)</code>
            <code proglang="python">(subopt and value is not None) or ( not subopt)</code>
          </ctrl>
          <argpos>3</argpos>
          <comment>
            <text lang="en">The cutoff is the probability cutoff used to determine which potential exons qualify as suboptimal exons.</text>
            <text lang="en">This argument should be a number between 0.01 and 0.99.</text>
            <text lang="en">For most applications, a cutoff value of about 0.10 is recommended.</text>
            <text lang="en">Setting the value much lower than 0.10 will often lead to an explosion in the number of suboptimal exons, most of which will probably not be useful.</text>
            <text lang="en">On the other hand, if the value is set much higher than 0.10, then potentially interesting suboptimal exons may be missed.</text>
          </comment>
        </parameter>
        <parameter>
          <name>ps</name>
          <prompt lang="en">Create Postscript output (-ps)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? (defined $scale) ? " -ps $psfname $scale" : " -ps $psfname" : ""</code>
            <code proglang="python">( "" , (" -ps " + str( psfname ), " -ps " + str( psfname ) + " " + str( scale ))[scale is not None] )[ value ]</code>
          </format>
          <argpos>3</argpos>
        </parameter>
        <parameter>
          <name>scale</name>
          <prompt lang="en">Scale for PostScript output</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$ps</code>
            <code proglang="python">ps</code>
          </precond>
        </parameter>
        <parameter ismandatory="1">
          <name>psfname</name>
          <prompt lang="en">Filename for PostScript output</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$ps</code>
            <code proglang="python">ps</code>
          </precond>
          <vdef>
            <value>genscan.ps</value>
          </vdef>
        </parameter>
        <parameter isout="1">
          <name>psFile</name>
          <prompt lang="en">PostScript output</prompt>
          <type>
            <datatype>
              <class>PostScript</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$ps</code>
            <code proglang="python">ps</code>
          </precond>
          <filenames>
            <code proglang="perl">$psfname</code>
            <code proglang="python">str( psfname )</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>
  </parameters>
</program>


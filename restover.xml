<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>restover</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>restover</title>
      <description>
        <text lang="en">Find restriction enzymes producing a specific overhang</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/restover.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:nucleic:restriction</category>
    <command>restover</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>

        <parameter>
          <name>e_datafile</name>
          <prompt lang="en">Restriction enzyme data file (optional)</prompt>
          <type>
            <datatype>
              <class>RestrictionEnzymeData</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -datafile=" + str(value))[value is not None]</code>
          </format>
          <argpos>2</argpos>
        </parameter>

        <parameter>
          <name>e_mfile</name>
          <prompt lang="en">Restriction enzyme methylation data file</prompt>
          <type>
            <datatype>
              <class>RestrictionEnzymeMethylationData</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -mfile=" + str(value))[value is not None ]</code>
          </format>
          <argpos>3</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_required</name>
      <prompt lang="en">Required section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_seqcomp</name>
          <prompt lang="en">Overlap sequence</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -seqcomp=" + str(value))[value is not None]</code>
          </format>
          <argpos>4</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_advanced</name>
      <prompt lang="en">Advanced section</prompt>

      <parameters>

        <parameter>
          <name>e_min</name>
          <prompt lang="en">Minimum cuts per re (value from 1 to 1000)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">("", " -min=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <ctrl>
            <message>
              <text lang="en">Value less than or equal to 1000 is required</text>
            </message>
            <code proglang="python">value &lt;= 1000</code>
          </ctrl>
          <argpos>5</argpos>
        </parameter>

        <parameter>
          <name>e_max</name>
          <prompt lang="en">Maximum cuts per re</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>2000000000</value>
          </vdef>
          <format>
            <code proglang="python">("", " -max=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>6</argpos>
        </parameter>

        <parameter>
          <name>e_single</name>
          <prompt lang="en">Force single site only cuts</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -single")[ bool(value) ]</code>
          </format>
          <argpos>7</argpos>
        </parameter>

        <parameter>
          <name>e_threeprime</name>
          <prompt lang="en">Use 3' overhang e.g. bamhi has ctag as a 5' overhang, and apai has ccgg as 3' overhang.</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -threeprime")[ bool(value) ]</code>
          </format>
          <argpos>8</argpos>
        </parameter>

        <parameter>
          <name>e_blunt</name>
          <prompt lang="en">Allow blunt end cutters</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -noblunt", "")[ bool(value) ]</code>
          </format>
          <argpos>9</argpos>
        </parameter>

        <parameter>
          <name>e_sticky</name>
          <prompt lang="en">Allow sticky end cutters</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -nosticky", "")[ bool(value) ]</code>
          </format>
          <argpos>10</argpos>
        </parameter>

        <parameter>
          <name>e_ambiguity</name>
          <prompt lang="en">Allow ambiguous matches</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -noambiguity", "")[ bool(value) ]</code>
          </format>
          <argpos>11</argpos>
        </parameter>

        <parameter>
          <name>e_plasmid</name>
          <prompt lang="en">Allow circular dna</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -plasmid")[ bool(value) ]</code>
          </format>
          <argpos>12</argpos>
        </parameter>

        <parameter>
          <name>e_methylation</name>
          <prompt lang="en">Use methylation data</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -methylation")[ bool(value) ]</code>
          </format>
          <argpos>13</argpos>
          <comment>
            <text lang="en">If this is set then RE recognition sites will not match methylated bases.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_commercial</name>
          <prompt lang="en">Only enzymes with suppliers</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -nocommercial", "")[ bool(value) ]</code>
          </format>
          <argpos>14</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_html</name>
          <prompt lang="en">Create html output</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -html")[ bool(value) ]</code>
          </format>
          <argpos>15</argpos>
        </parameter>

        <parameter>
          <name>e_limit</name>
          <prompt lang="en">Limits reports to one isoschizomer</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">(" -nolimit", "")[ bool(value) ]</code>
          </format>
          <argpos>16</argpos>
        </parameter>

        <parameter>
          <name>e_alphabetic</name>
          <prompt lang="en">Sort output alphabetically</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -alphabetic")[ bool(value) ]</code>
          </format>
          <argpos>17</argpos>
        </parameter>

        <parameter>
          <name>e_fragments</name>
          <prompt lang="en">Show fragment lengths</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -fragments")[ bool(value) ]</code>
          </format>
          <argpos>18</argpos>
        </parameter>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the output file (e_outfile)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>restover.e_outfile</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>19</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>RestoverReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>20</argpos>
    </parameter>
  </parameters>
</program>

<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>xxr</name>
    <version>3.03</version>
    <doc>
      <title>xxr</title>
      <description>
        <text lang="en">Integrons Analysis and Cassette Identification</text>
      </description>
      <authors>P. Bouige</authors>
      <reference>Rowe-Magnus D.A., Guerout A.M., Biskri L., Bouige P., Mazel D. Comparative analysis of superintegrons: Engineering extensive genetic diversity in the Vibrionaceae. Genome Res. 2003;13:428-442.</reference>
      <comment>
        <text lang="en">This software is able to extract putative cassette structures that fulfill the criteria established from analysis of previously known cassettes from integrons and superintegrons.</text>
      </comment>
    </doc>
    <category>sequence:nucleic:prediction</category>
    <category>sequence:nucleic:gene_finding</category>
  </head>
  <parameters>
    <parameter iscommand="1" ishidden="1">
      <name>xxr</name>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"xxr &lt;xxr.params"</code>
        <code proglang="python">"xxr &lt;xxr.params"</code>
      </format>
    </parameter>
    <parameter ismandatory="1" issimple="1">
      <name>input</name>
      <prompt lang="en">Input sequence</prompt>
      <type>
        <biotype>DNA</biotype>
        <datatype>
          <class>Sequence</class>
        </datatype>
        <dataFormat>FASTA</dataFormat>
      </type>
      <format>
        <code proglang="perl">"$value\\n.\\n"</code>
        <code proglang="python">str( value )+"\n.\n"</code>
      </format>
      <paramfile>xxr.params</paramfile>
    </parameter>
    <paragraph>
      <name>input_opt</name>
      <prompt>Options</prompt>
      <parameters>
        <parameter>
          <name>outsuffix</name>
          <prompt lang="en">Extension to add to files</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="perl">(defined $value) ? "$value\\n" : "\\n"</code>
            <code proglang="python">( "\n" , str( value ) + "\n" )[value is not None]</code>
          </format>
          <paramfile>xxr.params</paramfile>
        </parameter>
        <parameter>
          <name>minsize</name>
          <prompt lang="en">Minimal core size</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>4</value>
          </vdef>
          <format>
            <code proglang="perl">($value != $vdef) ? "$value\\n" : "\\n"</code>
            <code proglang="python">( "\n" , str( value ) + "\n" )[value != vdef]</code>
          </format>
          <paramfile>xxr.params</paramfile>
        </parameter>
        <parameter>
          <name>maxsize</name>
          <prompt lang="en">Maximal core size</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>10</value>
          </vdef>
          <format>
            <code proglang="perl">($value != $vdef) ? "$value\\n" : "\\n"</code>
            <code proglang="python">( "\n" , str( value ) + "\n" )[value != vdef]</code>
          </format>
          <paramfile>xxr.params</paramfile>
        </parameter>
        <parameter>
          <name>maxxxr</name>
          <prompt lang="en">Maximal XXR size</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>200</value>
          </vdef>
          <format>
            <code proglang="perl">($value != $vdef) ? "$value\\n" : "\\n"</code>
            <code proglang="python">( "\n" , str( value ) + "\n" )[value != vdef]</code>
          </format>
          <paramfile>xxr.params</paramfile>
        </parameter>
        <parameter>
          <name>maxgene</name>
          <prompt lang="en">Maximal gene size</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>2800</value>
          </vdef>
          <format>
            <code proglang="perl">($value != $vdef) ? "$value\\n" : "\\n"</code>
            <code proglang="python">( "\n" , str( value ) + "\n" )[value != vdef]</code>
          </format>
          <paramfile>xxr.params</paramfile>
        </parameter>
        <parameter>
          <name>cs</name>
          <prompt>Core Site (CS) - Variable site part upstream GTT</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <vdef>
            <value>GC</value>
          </vdef>
          <format>
            <code proglang="perl">($value ne $vdef) ? "$value\\n" : "\\n"</code>
            <code proglang="python">( "\n" , str( value ) + "\n" )[value != vdef]</code>
          </format>
          <comment>
            <text lang="en">By default, Core Site is GCGTT.</text>
          </comment>
          <paramfile>xxr.params</paramfile>
        </parameter>
        <parameter>
          <name>ICS</name>
          <prompt>Inverted Core Site (ICS) - Variable site part downstream AAC</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <vdef>
            <value>AAA</value>
          </vdef>
          <format>
            <code proglang="perl">($value ne $vdef) ? "$value\\n" : "\\n"</code>
            <code proglang="python">( "\n" , str( value ) + "\n" )[value != vdef]</code>
          </format>
          <comment>
            <text lang="en">By default, Inverted Core Site is AACAAA.</text>
          </comment>
          <paramfile>xxr.params</paramfile>
        </parameter>
      </parameters>
    </paragraph>
    <parameter isout="1">
      <name>res</name>
      <prompt lang="en">XXR results report</prompt>
      <type>
        <datatype>
          <class>Text</class>
        </datatype>
      </type>
      <filenames>
        <code proglang="perl">"Resultat_*"</code>
        <code proglang="python">"Resultat_*"</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>sk7</name>
      <prompt lang="en">Cassette gene files</prompt>
      <type>
        <datatype>
          <class>Text</class>
        </datatype>
      </type>
      <filenames>
        <code proglang="perl">"1_*_XXR_*"</code>
        <code proglang="perl">"2_*_XXR_*"</code>
        <code proglang="perl">"3_*_XXR_*"</code>
        <code proglang="python">"1_*_XXR_*"</code>
        <code proglang="python">"2_*_XXR_*"</code>
        <code proglang="python">"3_*_XXR_*"</code>
      </filenames>
      <comment>
        <text lang="en">If they exist, the 3th cassette gene files ONLY are displayed but you will find ALL of them in the job archive.</text>
      </comment>
    </parameter>
    <parameter isout="1">
      <name>xxrfasta</name>
      <prompt lang="en">XXR fasta</prompt>
      <type>
        <datatype>
          <class>Sequence</class>
        </datatype>
        <dataFormat>FASTA</dataFormat>
      </type>
      <filenames>
        <code proglang="perl">"XXR.fasta_*"</code>
        <code proglang="python">"XXR.fasta_*"</code>
      </filenames>
    </parameter>
    <parameter isout="1" ishidden="1">
      <name>hk7</name>
      <prompt lang="en">Cassette gene files</prompt>
      <type>
        <datatype>
          <class>Filename</class>
        </datatype>
      </type>
      <filenames>
        <code proglang="perl">"*_XXR_*"</code>
        <code proglang="python">"*_XXR_*"</code>
      </filenames>
    </parameter>
  </parameters>
</program>


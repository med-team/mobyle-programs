<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>backtranambig</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>backtranambig</title>
      <description>
        <text lang="en">Back-translate a protein sequence to ambiguous nucleotide sequence</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/backtranambig.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:nucleic:translation</category>
    <category>sequence:protein:composition</category>
    <command>backtranambig</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_additional</name>
      <prompt lang="en">Additional section</prompt>

      <parameters>

        <parameter>
          <name>e_table</name>
          <prompt lang="en">Genetic codes</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <vlist>
            <velem>
              <value>0</value>
              <label>Standard</label>
            </velem>
            <velem>
              <value>1</value>
              <label>Standard (with alternative initiation codons)</label>
            </velem>
            <velem>
              <value>2</value>
              <label>Vertebrate mitochondrial</label>
            </velem>
            <velem>
              <value>3</value>
              <label>Yeast mitochondrial</label>
            </velem>
            <velem>
              <value>4</value>
              <label>Mold, protozoan, coelenterate mitochondrial and  mycoplasma/spiroplasma</label>
            </velem>
            <velem>
              <value>5</value>
              <label>Invertebrate mitochondrial</label>
            </velem>
            <velem>
              <value>6</value>
              <label>Ciliate  macronuclear and dasycladacean</label>
            </velem>
            <velem>
              <value>9</value>
              <label>Echinoderm mitochondrial</label>
            </velem>
            <velem>
              <value>10</value>
              <label>Euplotid nuclear</label>
            </velem>
            <velem>
              <value>11</value>
              <label>Bacterial</label>
            </velem>
            <velem>
              <value>12</value>
              <label>Alternative yeast nuclear</label>
            </velem>
            <velem>
              <value>13</value>
              <label>Ascidian mitochondrial</label>
            </velem>
            <velem>
              <value>14</value>
              <label>Flatworm mitochondrial</label>
            </velem>
            <velem>
              <value>15</value>
              <label>Blepharisma macronuclear</label>
            </velem>
            <velem>
              <value>16</value>
              <label>Chlorophycean mitochondrial</label>
            </velem>
            <velem>
              <value>21</value>
              <label>Trematode mitochondrial</label>
            </velem>
            <velem>
              <value>22</value>
              <label>Scenedesmus obliquus</label>
            </velem>
            <velem>
              <value>23</value>
              <label>Thraustochytrium mitochondrial</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -table=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>2</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the output file (e_outfile)</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>backtranambig.e_outfile</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>3</argpos>
        </parameter>

        <parameter>
          <name>e_osformat_outfile</name>
          <prompt lang="en">Choose the sequence output format</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>FASTA</value>
          </vdef>
          <vlist>
            <velem>
              <value>EMBL</value>
              <label>Embl</label>
            </velem>
            <velem>
              <value>FASTA</value>
              <label>Fasta</label>
            </velem>
            <velem>
              <value>GCG</value>
              <label>Gcg</label>
            </velem>
            <velem>
              <value>GENBANK</value>
              <label>Genbank</label>
            </velem>
            <velem>
              <value>NBRF</value>
              <label>Nbrf</label>
            </velem>
            <velem>
              <value>CODATA</value>
              <label>Codata</label>
            </velem>
            <velem>
              <value>RAW</value>
              <label>Raw</label>
            </velem>
            <velem>
              <value>SWISSPROT</value>
              <label>Swissprot</label>
            </velem>
            <velem>
              <value>GFF</value>
              <label>Gff</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -osformat=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>4</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>
              <ref param="e_osformat_outfile">
              </ref>
            </dataFormat>
          </type>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>5</argpos>
    </parameter>
  </parameters>
</program>

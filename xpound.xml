<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>xpound</name>
    <doc>
      <title>Xpound</title>
      <description>
        <text lang="en">Software for exon trapping</text>
      </description>
      <authors>Thomas and Skolnick</authors>
      <reference>A probabilistic model for detecting coding regions in DNA sequences. Alun Thomas and Mark H Skolnick, IMA Journal of Mathematics Applied in Medicine and Biology, 1994, 11, 149-160.</reference>
    </doc>
    <category>sequence:nucleic:gene_finding</category>
    <command>xpound</command>
  </head>
  <parameters>
    <parameter ismandatory="1" issimple="1">
      <name>seq</name>
      <prompt lang="en">DNA sequence File</prompt>
      <type>
        <datatype>
          <class>Sequence</class>
        </datatype>
        <dataFormat>RAW</dataFormat>
        <card>1,1</card>
      </type>
      <format>
        <code proglang="perl">" &lt;$value"</code>
        <code proglang="python">" &lt;"+str(value)</code>
      </format>
      <argpos>2</argpos>
      <comment>
        <text lang="en">Everything after a % on a line in the input
          file is ignored. Other than comment xpound expects only white
          space, which is also ignored, or IUPAC characters:</text>
        <text lang="en">A C M G R S V T W Y H K D B N</text>
        <text lang="en">in upper or lower case. Characters which do
          not uniquely determine a base, such as N, B, S and so on, are
          all interpreted as a C.</text>
        <text lang="en">Xpound will not accept the IUPAC character -,
          all occurences of which should be stripped from the input file
          beforehand.</text>
      </comment>
    </parameter>
    <parameter isstdout="1">
      <name>outfile</name>
      <prompt lang="en">Output file</prompt>
      <type>
        <datatype>
          <class>XpoundReport</class>
          <superclass>Report</superclass>
        </datatype>
      </type>
      <filenames>
        <code proglang="perl">"xpound.out"</code>
        <code proglang="python">"xpound.out"</code>
      </filenames>
    </parameter>
    <paragraph>
      <name>report_options</name>
      <prompt lang="en">Report options</prompt>
      <parameters>
        <parameter>
          <name>report</name>
          <prompt lang="en">Reports regions of bases for which the probability of coding is high (xreport)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " ; xreport &lt;xpound.out " : ""</code>
            <code proglang="python">( "" , " ; xreport &lt;xpound.out " )[ value ]</code>
          </format>
          <argpos>20</argpos>
        </parameter>
        <parameter>
          <name>cut_off</name>
          <prompt lang="en">Cut off value for report</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$report</code>
            <code proglang="python">report</code>
          </precond>
          <vdef>
            <value>0.75</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " $value " : ""</code>
            <code proglang="python">( "" , " " + str(value) )[ value is not None and value != vdef]</code>
          </format>
          <argpos>21</argpos>
        </parameter>
        <parameter>
          <name>min_length</name>
          <prompt lang="en">Minimum length value for report</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$report</code>
            <code proglang="python">report</code>
          </precond>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " $value " : ""</code>
            <code proglang="python">( "" , " " + str(value) )[ value is not None and value != vdef]</code>
          </format>
          <argpos>22</argpos>
        </parameter>
        <parameter isout="1">
          <name>report_file</name>
          <prompt lang="en">Report file</prompt>
          <type>
            <datatype>
              <class>XreportReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$report</code>
            <code proglang="python">report</code>
          </precond>
          <format>
            <code proglang="perl">" &gt;xreport.out "</code>
            <code proglang="python">"&gt;xreport.out "</code>
          </format>
          <argpos>25</argpos>
          <filenames>
            <code proglang="perl">"xreport.out"</code>
            <code proglang="python">"xreport.out"</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>postscript_options</name>
      <prompt lang="en">Postscript options</prompt>
      <parameters>
        <parameter>
          <name>postscript</name>
          <prompt lang="en">Produces a file of graphs in PostScript format (xpscript)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? "; xpscript xpound.out" : ""</code>
            <code proglang="python">( "" , "; xpscript xpound.out" )[ value ]</code>
          </format>
          <argpos>30</argpos>
        </parameter>
        <parameter>
          <name>orientation</name>
          <prompt lang="en">Orientation (-l)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$postscript</code>
            <code proglang="python">postscript</code>
          </precond>
          <vdef>
            <value>portrait</value>
          </vdef>
          <vlist>
            <velem>
              <value>portrait</value>
              <label>Portrait</label>
            </velem>
            <velem>
              <value>lanscape</value>
              <label>Lanscape</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">($value eq "lanscape") ? " -l " : ""</code>
            <code proglang="python">( "" , " -l " )[ value == "lanscape" ]</code>
          </format>
          <argpos>31</argpos>
        </parameter>
        <parameter>
          <name>rows</name>
          <prompt lang="en">Rows of plots per page (-r)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$postscript</code>
            <code proglang="python">postscript</code>
          </precond>
          <vdef>
            <value>5</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -r $value " : ""</code>
            <code proglang="python">( "" , " -r " + str(value) )[ value is not None and value != vdef]</code>
          </format>
          <argpos>32</argpos>
        </parameter>
        <parameter>
          <name>columns</name>
          <prompt lang="en">Columns of plots per page (-c)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$postscript</code>
            <code proglang="python">postscript</code>
          </precond>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -c $value " : ""</code>
            <code proglang="python">( "" , " -c " + str(value) )[ value is not None and value != vdef]</code>
          </format>
          <argpos>32</argpos>
        </parameter>
        <parameter>
          <name>high</name>
          <prompt lang="en">Draw a line at this level (-hi)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$postscript</code>
            <code proglang="python">postscript</code>
          </precond>
          <vdef>
            <value>0.75</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -hi $value " : ""</code>
            <code proglang="python">( "" , " -hi " + str(value) )[ value is not None and value != vdef]</code>
          </format>
          <argpos>33</argpos>
        </parameter>
        <parameter>
          <name>low</name>
          <prompt lang="en">Draw a line at this level (-lo)</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$postscript</code>
            <code proglang="python">postscript</code>
          </precond>
          <vdef>
            <value>0.5</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -lo $value " : ""</code>
            <code proglang="python">( "" , " -lo " + str(value) )[ value is not None and value != vdef]</code>
          </format>
          <argpos>34</argpos>
        </parameter>
        <parameter isout="1">
          <name>psfile</name>
          <prompt lang="en">PostScript file</prompt>
          <type>
            <datatype>
              <class>PostScript</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">$postscript</code>
            <code proglang="python">postscript</code>
          </precond>
          <format>
            <code proglang="perl">" &gt;xpound.ps"</code>
            <code proglang="python">" &gt;xpound.ps"</code>
          </format>
          <argpos>100</argpos>
          <filenames>
            <code proglang="perl">"xpound.ps"</code>
            <code proglang="python">"xpound.ps"</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>
  </parameters>
</program>


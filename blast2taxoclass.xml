<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head xmlns:xi="http://www.w3.org/2001/XInclude">
    <name>blast2taxoclass</name>
    <version>1.0</version>
    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="Entities/blastTaxoAnalysis_package.xml"/>
    <doc>
      <title>blast2taxoclass</title>
      <description>
        <text lang="en">Blast filtering with taxonomic hierarchy information</text>
      </description>
      <authors>C. Maufrais</authors>
    </doc>
    <category>database:search:filter</category>
    <command>blast2taxoclass</command>
  </head>
  <parameters>
    <parameter ismandatory="1" issimple="1">
      <name>infile</name>
      <prompt lang="en">Blast output file</prompt>
      <type>
        <datatype>
          <class>BlastTextReport</class>
          <superclass>Report</superclass>
        </datatype>
      </type>
      <format>
        <code proglang="perl">" -i $value"</code>
        <code proglang="python">" -i " + str(value)</code>
      </format>
      <argpos>20</argpos>
    </parameter>
   
    <parameter issimple="1">
      <name>blastfilter</name>
      <prompt lang="en">Find taxonomic classification of:</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>M</value>
      </vdef>
      <vlist>
        <velem>
          <value>M</value>
          <label>best hit</label>
        </velem>
        <velem>
          <value>F</value>
          <label>most frequent hit</label>
        </velem>
      </vlist>
      
      <format>
        <code proglang="perl">($value) ? " -$value" : ""</code>
        <code proglang="python">" -" + str(value)</code>
      </format>
    </parameter>
   
    <parameter>
      <name>nbofhit</name>
      <prompt lang="en">Number of hsp to consider (-x)</prompt>
      <type>
        <datatype>
          <class>Integer</class>
        </datatype>
      </type>
      <vdef>
        <value>10</value>
      </vdef>
      <format>
        <code proglang="perl">(defined $value) ? " -x $value" : ""</code>
        <code proglang="python">("", " -x " + str(value) )[value is not None and value != vdef]</code>
      </format>
      <comment>
         <text lang="en">0: all hsp </text>
      </comment>
       
    </parameter>
    
    
        
    <paragraph>
      <name>taxonomicfilter</name>
      <prompt lang="en">Taxonomic hierarchy filter option</prompt>
      <parameters>
    <parameter issimple="1">
      <name>position</name>
      <prompt lang="en">Relative position in taxonomic hierarchy (-p)</prompt>
      <type>
        <datatype>
          <class>Integer</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">(defined $value) ? " -p $value" : ""</code>
        <code proglang="python">("", " -p " + str(value) )[value is not None]</code>
      </format>
      <ctrl>
        <message>
          <text lang="en">Choose only one of taxonomic hierarchy filter option: Relative position, Taxonomic rank or Taxonomic name.</text>
        </message>
        <code proglang="perl">(defined $position and (not defined $taxonomic_name and not defined $rank))</code>
        <code proglang="python">(position is not None and (taxonomic_name is None and rank is None)) or (taxonomic_name is not None and (position is None and rank is None)) or (rank is not None and (taxonomic_name is None and position is None)) </code>
      </ctrl>
      <comment>
        <text lang="en">zero means: root of taxonomy, higher value: leaf or near </text>
      </comment>
    </parameter>
    
    <parameter issimple="1">
      <name>taxonomic_name</name>
      <prompt lang="en">Taxonomic Name (-n)</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">(defined $value) ? " -n $value" : ""</code>
        <code proglang="python">("", " -n " + str(value).replace(' ','_') )[value is not None]</code>
      </format>
      <ctrl>
        <message>
          <text lang="en">Choose only one of taxonomic hierarchy filter option: Relative position, Taxonomic rank or Taxonomic name.</text>
        </message>
        <code proglang="perl">(defined $position and (not defined $taxonomic_name and not defined $rank))</code>
        <code proglang="python">(position is not None and (taxonomic_name is None and rank is None)) or (taxonomic_name is not None and (position is None and rank is None)) or (rank is not None and (taxonomic_name is None and position is None)) </code>
      </ctrl>
    </parameter>
    
    <parameter issimple="1">
      <name>rank</name>
      <prompt lang="en">Taxonomic rank name (-r)</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>null</value>
      </vdef>
      <vlist>
        <velem undef="1">
          <value>null</value>
          <label></label>
        </velem>
        <velem>
          <value>superkingdom</value>
          <label>superkingdom</label>
        </velem>
        <velem>
          <value>kingdom</value>
          <label>kingdom</label>
        </velem>
        <velem>
          <value>subkingdom</value>
          <label>subkingdom</label>
        </velem>
        <velem>
          <value>superphylum</value>
          <label>superphylum</label>
        </velem>
        <velem>
          <value>phylum</value>
          <label>phylum</label>
        </velem>
        <velem>
          <value>subphylum</value>
          <label>subphylum</label>
        </velem>
        <velem>
          <value>superclass</value>
          <label>superclass</label>
        </velem>
        <velem>
          <value>class</value>
          <label>class</label>
        </velem>
        <velem>
          <value>subclass</value>
          <label>subclass</label>
        </velem>
        <velem>
          <value>infraclass</value>
          <label>infraclass</label>
        </velem>
        <velem>
          <value>superorder</value>
          <label>superorder</label>
        </velem>
        <velem>
          <value>order</value>
          <label>order</label>
        </velem>
        <velem>
          <value>suborder</value>
          <label>suborder</label>
        </velem>
        <velem>
          <value>infraorder</value>
          <label>infraorder</label>
        </velem>
        <velem>
          <value>parvorder</value>
          <label>parvorder</label>
        </velem>
        <velem>
          <value>superfamily</value>
          <label>superfamily</label>
        </velem>
        <velem>
          <value>family</value>
          <label>family</label>
        </velem>
        <velem>
          <value>subfamily</value>
          <label>subfamily</label>
        </velem>
        <velem>
          <value>tribe</value>
          <label>tribe</label>
        </velem>
        <velem>
          <value>subtribe</value>
          <label>subtribe</label>
        </velem>
        <velem>
          <value>genus</value>
          <label>genus</label>
        </velem>
        <velem>
          <value>subgenus</value>
          <label>subgenus</label>
        </velem>
        <velem>
          <value>species_group</value>
          <label>species_group</label>
        </velem>
        <velem>
          <value>species_subgroup</value>
          <label>species_subgroup</label>
        </velem>
        <velem>
          <value>species</value>
          <label>species</label>
        </velem>
        <velem>
          <value>subspecies</value>
          <label>subspecies</label>
        </velem>
        <velem>
          <value>varietas</value>
          <label>varietas</label>
        </velem>
        <velem>
          <value>forma</value>
          <label>forma</label>
        </velem>
      </vlist>
      <format>
        <code proglang="perl">(defined $value) ? " -r $value" : ""</code>
        <code proglang="python">("", " -r " + str(value) )[value is not None]</code>
      </format>
      <ctrl>
        <message>
          <text lang="en">Choose only one of taxonomic hierarchy filter option: Relative position, Taxonomic rank or Taxonomic name.</text>
        </message>
        <code proglang="perl">(defined $position and (not defined $taxonomic_name and not defined $rank))</code>
        <code proglang="python">(position is not None and (taxonomic_name is None and rank is None)) or (taxonomic_name is not None and (position is None and rank is None)) or (rank is not None and (taxonomic_name is None and position is None)) </code>
      </ctrl>
      <comment>
        <text lang="en">If Taxonomic rank is not defined for one hit, it is not treated.</text>
      </comment>
    </parameter>
    
     </parameters>
     </paragraph>
       
    <paragraph>
      <name>output</name>
      <prompt lang="en">Output option</prompt>
      <parameters>
        <parameter>
          <name>blastout</name>
          <prompt lang="en">Blast output file(s) sort/split by specific taxonomic hierarchy (-b)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -b" : ""</code>
            <code proglang="python">("" , " -b") [value]</code>
          </format>
        </parameter>
        
        <parameter>
          <name>queryout</name>
          <prompt lang="en">Query name write in file(s) sort/split by specific taxonomic hierarchy (-q)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value)? " -q" : ""</code>
            <code proglang="python">("" , " -q") [value]</code>
          </format>
        </parameter> 
        
        <parameter>
          <name>fastaExtract</name>
          <prompt lang="en">Extraction of fasta sequences.</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <ctrl>
            <message>
              <text lang="en">Query name write in file must be checked and query sequences must be done.</text>
            </message>
            <code proglang="perl">$fastaExtract == 1 and $queryout == 1 and defined $query_seq</code>
            <code proglang="python">(fastaExtract and (queryout and query_seq is not None)) or (not fastaExtract)</code>
          </ctrl>
          <comment>
            <text lang="en">Extract fasta sequence, matching specified taxonomic filter, from file containing query sequences witch are used to made blast.</text>
          </comment>
        </parameter>
        
        <parameter>
          <name>query_seq</name>
          <prompt lang="en">Query sequences witch are used to made blast.</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>FASTA</dataFormat>
            <card>1,n</card>
          </type>
          <precond>
            <code proglang="perl">defined $fastaExtract and defined $queryout</code>
            <code proglang="python">fastaExtract and queryout</code>
          </precond>
          <format>
            <code proglang="perl">(defined $value)? " &amp;&amp; extractfasta -i $query *.qry": ""</code>
            <code proglang="python">(""," &amp;&amp; extractfasta -i "+ str(value) + " *.qry") [value is not None]</code>
          </format>
          <argpos>100</argpos>
        </parameter>
        
        <parameter isstdout="1">
          <name>outfile</name>
          <prompt>Output file</prompt>
          <type>
            <datatype>
              <class>Blast2taxoclassReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="perl">"blast2taxoclass.out"</code>
            <code proglang="python">"blast2taxoclass.out"</code>
          </filenames>
        </parameter>
        
        <parameter isout="1">
          <name>blastoutfile</name>
          <prompt>Blast output file(s)</prompt>
          <type>
            <datatype>
              <class>BlastTextReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">defined $blastout</code>
            <code proglang="python">blastout</code>
          </precond>
          <filenames>
            <code proglang="perl">"*.blast"</code>
            <code proglang="python">"*.blast"</code>
          </filenames>
        </parameter>
        
        <parameter isout="1">
          <name>queryoutfile</name>
          <prompt>Query name file</prompt>
          <type>
            <datatype>
              <class>QueryNameReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">defined $queryout</code>
            <code proglang="python">queryout</code>
          </precond>
          <filenames>
            <code proglang="perl">"*.qry"</code>
            <code proglang="python">"*.qry"</code>
          </filenames>
        </parameter>
        
        <parameter isout="1">
          <name>fastafile</name>
          <prompt>Fasta file</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
          </type>
          <precond>
            <code proglang="perl">defined $fastaExtract</code>
            <code proglang="python">fastaExtract</code>
          </precond>
          <filenames>
            <code proglang="perl">"*.fasta"</code>
            <code proglang="python">"*.fasta"</code>
          </filenames>
        </parameter>
        
        
      </parameters>
    </paragraph>
  </parameters>
</program>

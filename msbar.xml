<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>msbar</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>msbar</title>
      <description>
        <text lang="en">Mutate a sequence</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/msbar.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:nucleic:mutation</category>
    <category>sequence:protein:mutation</category>
    <command>msbar</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>

        <parameter>
          <name>e_othersequence</name>
          <prompt lang="en">Other sequences that the mutated result should not match</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -othersequence=" + str(value))[value is not None ]</code>
          </format>
          <argpos>2</argpos>
          <comment>
            <text lang="en">If you require that the resulting mutated sequence should not match a set of other sequences, then you can specify that set  of sequences here. For example, if you require that the mutated  sequence should not be the same as the input sequence, enter the  input sequence here. If you want the result to be different to  previous results of this program, specify the previous result  sequences here. The program will check that the result does not  match the sequences specified here before writing it out. If a  match is found, then the mutation is started again with a fresh  copy of the input sequence. If, after 10 such retries, there is  still a match to the set of sequence given here, then the matching  mutated sequence is written with a warning message.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_required</name>
      <prompt lang="en">Required section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_count</name>
          <prompt lang="en">Number of times to perform the mutation operations (value greater than or equal to 0)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">("", " -count=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 0 is required</text>
            </message>
            <code proglang="python">value &gt;= 0</code>
          </ctrl>
          <argpos>3</argpos>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_point</name>
          <prompt lang="en">Point mutation operations (value from 1 to 4)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <vlist>
            <velem>
              <value>0</value>
              <label>None</label>
            </velem>
            <velem>
              <value>1</value>
              <label>Any of the following</label>
            </velem>
            <velem>
              <value>2</value>
              <label>Insertions</label>
            </velem>
            <velem>
              <value>3</value>
              <label>Deletions</label>
            </velem>
            <velem>
              <value>4</value>
              <label>Changes</label>
            </velem>
            <velem>
              <value>5</value>
              <label>Duplications</label>
            </velem>
            <velem>
              <value>6</value>
              <label>Moves</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -point=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>4</argpos>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_block</name>
          <prompt lang="en">Block mutation operations (value from 1 to 4)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <vlist>
            <velem>
              <value>0</value>
              <label>None</label>
            </velem>
            <velem>
              <value>1</value>
              <label>Any of the following</label>
            </velem>
            <velem>
              <value>2</value>
              <label>Insertions</label>
            </velem>
            <velem>
              <value>3</value>
              <label>Deletions</label>
            </velem>
            <velem>
              <value>4</value>
              <label>Changes</label>
            </velem>
            <velem>
              <value>5</value>
              <label>Duplications</label>
            </velem>
            <velem>
              <value>6</value>
              <label>Moves</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -block=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>5</argpos>
        </parameter>

        <parameter>
          <name>e_codon</name>
          <prompt lang="en">Codon mutation operations (value from 1 to 4)</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <vlist>
            <velem>
              <value>0</value>
              <label>None</label>
            </velem>
            <velem>
              <value>1</value>
              <label>Any of the following</label>
            </velem>
            <velem>
              <value>2</value>
              <label>Insertions</label>
            </velem>
            <velem>
              <value>3</value>
              <label>Deletions</label>
            </velem>
            <velem>
              <value>4</value>
              <label>Changes</label>
            </velem>
            <velem>
              <value>5</value>
              <label>Duplications</label>
            </velem>
            <velem>
              <value>6</value>
              <label>Moves</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -codon=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>6</argpos>
          <comment>
            <text lang="en">Types of codon mutations to perform. These are only done if the sequence is nucleic.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_additional</name>
      <prompt lang="en">Additional section</prompt>

      <parameters>

        <parameter>
          <name>e_inframe</name>
          <prompt lang="en">Do 'codon' and 'block' operations in frame</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -inframe")[ bool(value) ]</code>
          </format>
          <argpos>7</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_advanced</name>
      <prompt lang="en">Advanced section</prompt>

      <parameters>

        <parameter>
          <name>e_minimum</name>
          <prompt lang="en">Minimum size for a block mutation (value greater than or equal to 0)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">("", " -minimum=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 0 is required</text>
            </message>
            <code proglang="python">value &gt;= 0</code>
          </ctrl>
          <argpos>8</argpos>
        </parameter>

        <parameter>
          <name>e_maximum</name>
          <prompt lang="en">Maximum size for a block mutation</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>10</value>
          </vdef>
          <format>
            <code proglang="python">("", " -maximum=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>9</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outseq</name>
          <prompt lang="en">Name of the output sequence file (e_outseq)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>msbar.e_outseq</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outseq=" + str(value))[value is not None]</code>
          </format>
          <argpos>10</argpos>
        </parameter>

        <parameter>
          <name>e_osformat_outseq</name>
          <prompt lang="en">Choose the sequence output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>FASTA</value>
          </vdef>
          <vlist>
            <velem>
              <value>EMBL</value>
              <label>Embl</label>
            </velem>
            <velem>
              <value>FASTA</value>
              <label>Fasta</label>
            </velem>
            <velem>
              <value>GCG</value>
              <label>Gcg</label>
            </velem>
            <velem>
              <value>GENBANK</value>
              <label>Genbank</label>
            </velem>
            <velem>
              <value>NBRF</value>
              <label>Nbrf</label>
            </velem>
            <velem>
              <value>CODATA</value>
              <label>Codata</label>
            </velem>
            <velem>
              <value>RAW</value>
              <label>Raw</label>
            </velem>
            <velem>
              <value>SWISSPROT</value>
              <label>Swissprot</label>
            </velem>
            <velem>
              <value>GFF</value>
              <label>Gff</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -osformat=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>11</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outseq_out</name>
          <prompt lang="en">outseq_out option</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>
              <ref param="e_osformat_outseq">
              </ref>
            </dataFormat>
          </type>
          <filenames>
            <code proglang="python">e_outseq</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>12</argpos>
    </parameter>
  </parameters>
</program>

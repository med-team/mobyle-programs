<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Sandrine Larroude,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>ktreedist</name>
    <version>1.0</version>
    <doc>
      <title>Ktreedist</title>
      <description>
        <text lang="en">Calculation of the minimum branch length distance (K tree score) between phylogenetic trees</text>
      </description>
      <authors>Victor Soria-Carrasco, Jose Castresana</authors>
      <reference>Soria-Carrasco, V., Talavera, G., Igea, J., and Castresana, J. (2007). The K tree score: quantification of differences 
      		in the relative branch length and topology of phylogenetic trees. Bioinformatics 23, 2954-2956.</reference>
      <doclink>http://bioweb2.pasteur.fr/docs/ktreedist/</doclink>
      <homepagelink>http://molevol.cmima.csic.es/castresana/Ktreedist.html</homepagelink>
      <sourcelink>http://molevol.cmima.csic.es/castresana/Ktreedist.html</sourcelink>
    </doc>
    <category>phylogeny:tree_analyser</category>
    <command>ktreedist</command>
  </head>
  <parameters>
    <paragraph>
      <name>input</name>
      <prompt lang="en">Input</prompt>
      <comment>
        <text lang="en">The program is supposed to run with one reference tree and one or several comparison trees. If the reference file contains more than one tree, only the first one will be used.</text>
      </comment>
      <parameters>
        <parameter ismandatory="1" issimple="1">
          <name>ref_tree</name>
          <prompt lang="en">Reference Tree</prompt>
          <type>
            <datatype>
              <class>Tree</class>
            </datatype>
            <dataFormat>NEWICK</dataFormat>
            <dataFormat>NEXUS</dataFormat>
          </type>
          <format>
            <code proglang="perl">"-rt $value"</code>
            <code proglang="python">" -rt "+str(value)</code>
          </format>
          <argpos>2</argpos>
          <comment>
            <text lang="en">This is the file that contains the tree to which you want to compare the comparison tree/s.</text>
            <text lang="en"> Only NEWICK or NEXUS tree format are accepted by ktreedist.</text>
            <text lang="en">The input tree must be write in one line. for nexus tree the tree itself must be write in one line</text>
          </comment>
          <example>
#NEXUS
begin trees;
tree 'name' =(1:0.212481,8:0.297838,(9:0.222729,((6:0.201563,7:0.194547):0.282035,(4:1.146091,(3:1.008881,(10:0.384105,(2:0.235682,5:0.353432):0.323680):0.103875):0.413540):0.254687):0.095341):0.079254):0.000000;
end;
          </example>
        </parameter>
        <parameter ismandatory="1" issimple="1">
          <name>comp_tree</name>
          <prompt lang="en">Comparison Tree</prompt>
          <type>
            <datatype>
              <class>Tree</class>
            </datatype>
            <dataFormat>NEWICK</dataFormat>
            <dataFormat>NEXUS</dataFormat>
          </type>
          <format>
            <code proglang="perl">"-ct $value "</code>
            <code proglang="python">" -ct "+str(value)+" "</code>
          </format>
          <comment>
            <text lang="en">This is the file that contains the tree or the set of trees you want to compare to the reference tree. They will be scaled to match as much as possible the reference tree.</text>
            <text lang="en">Only NEWICK or NEXUS tree format are accepted by ktreedist.</text>
            <text lang="en">The input tree must be write in one line. for nexus tree the tree itself must be write in one line</text>
          </comment>
          <example>
(1:0.212481,8:0.297838,(9:0.222729,((6:0.201563,7:0.194547):0.282035,(4:1.146091,(3:1.008881,(10:0.384105,(2:0.235682,5:0.353432):0.323680):0.103875):0.413540):0.254687):0.095341):0.079254):0.000000;          
          </example>
          <argpos>3</argpos>
        </parameter>
      </parameters>
    </paragraph>
    <paragraph>
      <name>options</name>
      <prompt lang="en">Options</prompt>
      <argpos>4</argpos>
      <parameters>
        <parameter>
          <name>output_res</name>
          <prompt lang="en">Output file for table of results (-t)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -t"</code>
            <code proglang="python">("", " -t")[ value ]</code>
          </format>
          <comment>
            <text lang="en"> A file containing a table of results is generated.</text>
          </comment>
        </parameter>
        <parameter>
          <name>output_part</name>
          <prompt lang="en">Output file for table of partitions (-p)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -p"</code>
            <code proglang="python">("", " -p")[ value ]</code>
          </format>
          <comment>
            <text lang="en"> A file containing a table of partitions for each comparison tree is generated.</text>
          </comment>
        </parameter>
        <parameter>
          <name>output_comp</name>
          <prompt lang="en">Output file for comparison tree/s after scaling (-s)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -s"</code>
            <code proglang="python">("", " -s")[ value ]</code>
          </format>
          <comment>
            <text lang="en">A file containing the scaled comparison tree/s is generated.</text>
          </comment>
        </parameter>
        <parameter>
          <name>output_rf</name>
          <prompt lang="en">Output symmetric difference (Robinson-Foulds) (-r)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -r"</code>
            <code proglang="python">("", " -r")[ value ]</code>
          </format>
          <comment>
            <text lang="en">The symmetric difference is defined as the number of partitions that are not shared between two trees, 
						that is, the number of partitions of the first tree that are not present in the second tree plus the number of partitions 
						of the second tree that are not present in the first tree.</text>
          </comment>
        </parameter>
        <parameter>
          <name>output_nbpf</name>
          <prompt lang="en">Output number of partitions in the comparison tree/s (-n)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -n"</code>
            <code proglang="python">("", " -n")[ value ]</code>
          </format>
          <comment>
            <text lang="en">The knowledge of the number of partitions may be useful to detect trees with polytomies.</text>
          </comment>
        </parameter>
        <parameter>
          <name>output_all</name>
          <prompt lang="en">Equivalent to all options (-a)</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="perl">($value) ? " -a"</code>
            <code proglang="python">("", " -a")[ value ]</code>
          </format>
          <comment>
            <text lang="en">Equivalent to all options.</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>
    <parameter isout="1">
      <name>output_res_f</name>
      <prompt lang="en">Output file for table of results</prompt>
      <type>
        <datatype>
          <class>Text</class>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$output_res or $output_all</code>
        <code proglang="python">output_res or output_all</code>
      </precond>
      <filenames>
        <code proglang="perl">"*.tab"</code>
        <code proglang="python">"*.tab"</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>output_part_f</name>
      <prompt lang="en">Output file for table of partitions</prompt>
      <type>
        <datatype>
          <class>Text</class>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$output_part or $output_all</code>
        <code proglang="python">output_part or output_all</code>
      </precond>
      <filenames>
        <code proglang="perl">"*.part"</code>
        <code proglang="python">"*.part"</code>
      </filenames>
    </parameter>
    <parameter isout="1">
      <name>output_comp_f</name>
      <prompt lang="en">Output file for comparison tree/s after scaling</prompt>
      <type>
        <datatype>
          <class>Tree</class>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$output_comp or $output_all</code>
        <code proglang="python">output_comp or output_all</code>
      </precond>
      <filenames>
        <code proglang="perl">"*.scaled"</code>
        <code proglang="python">"*.scaled"</code>
      </filenames>
    </parameter>
  </parameters>
</program>


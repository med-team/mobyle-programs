<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>rnaplot</name>
    <version>1.8.4</version>
    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="Entities/ViennaRNA_package.xml"/>
    <doc>
      <title>RNAplot</title>
      <description>
        <text lang="en">Draw RNA Secondary Structures</text>
      </description>
      <authors>Hofacker, Fontana, Bonhoeffer, Stadler</authors>
      <reference>I.L. Hofacker, W. Fontana, P.F. Stadler, S. Bonhoeffer, M. Tacker, P. Schuster (1994) Fast Folding and Comparison of RNA Secondary Structures. Monatshefte f. Chemie 125: 167-188</reference>
      <reference>A. Walter, D Turner, J Kim, M Lyttle, P Muller, D Mathews, M Zuker Coaxial stacking of helices enhances binding of oligoribonucleotides. PNAS, 91, pp 9218-9222, 1994</reference>
      <reference>M. Zuker, P. Stiegler (1981) Optimal computer folding of large RNA sequences using thermodynamic and auxiliary information, Nucl Acid Res 9: 133-148</reference>
      <reference>J.S. McCaskill (1990) The equilibrium partition function and base pair binding probabilities for RNA secondary structures, Biopolymers 29: 11051119 D.H. Turner N. Sugimoto and S.M. Freier (1988) RNA structure prediction, Ann Rev Biophys Biophys Chem 17: 167-192</reference>
      <reference>D. Adams (1979) The hitchhiker's guide to the galaxy, Pan Books, London</reference>
      <doclink>http://bioweb2.pasteur.fr/gensoft/sequence/nucleic/2D_structure.html#ViennaRNa</doclink>
      <comment>
        <text lang="en">RNAplot  reads RNA sequences and structures from stdin in the format as
       produced by RNAfold and produces drawings of  the  secondary  structure
       graph.   The  coordinates  are  produced  using  either E. Bruccolerias
       naview routines, or a simple radial layout method.</text>
       </comment>
    </doc>
    <category>sequence:nucleic:2D_structure</category>
    <command>RNAplot</command>
  </head>
  <parameters>
    <parameter ismandatory="1" issimple="1">
      <name>seqin</name>
      <prompt lang="en">RNA sequences and structures from output in the format as
       produced by RNAfold </prompt>
      <type>
        <datatype>
          <class>RnafoldOutput</class>
          <superclass>AbstractText</superclass>
        </datatype>
      </type>
      <format>
        <code proglang="perl">" &lt;$value"</code>
        <code proglang="python">" &lt;"+str(value)</code>
      </format>
      
      <argpos>1000</argpos>
    </parameter>
    
    <parameter>
    <name>layout</name>
    <prompt lang="en">Choose  the  layout  algorithm</prompt>
    <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>1</value>
      </vdef>
      <vlist>
        <velem>
          <value>1</value>
          <label>E. Bruccolerias naview routine</label>
        </velem>
        <velem>
          <value>0</value>
          <label>Simple radial layout</label>
        </velem>
      </vlist>
      <format>
            <code proglang="perl">(defined $value and $value ne $vdef)? " -t $value" : ""</code>
            <code proglang="python">( "" , " -t " + str(value) )[ value is not None and value != vdef]</code>
      </format>
      <comment>
        <text lang="en">Choose  the  layout  algorithm.  Simple  radial  layout if 0, or
              naview if 1.  Default is 1.</text>
      </comment>
    </parameter>
    
    <parameter>
      <name>outformat</name>
      <prompt lang="en">Specify output format</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>ps</value>
      </vdef>
      <vlist>
        <velem>
          <value>ps</value>
          <label>PostScript</label>
        </velem>
        <velem>
          <value>gml</value>
          <label>Graph  Meta  Language</label>
        </velem>
        <velem>
          <value>xrna</value>
          <label>XRNA file</label>
        </velem>
        <velem>
          <value>svg</value>
          <label>Scalable Vector Graphics</label>
        </velem>
      </vlist>
      <format>
            <code proglang="perl">(defined $value and $value ne $vdef)? " -o $value" : ""</code>
            <code proglang="python">( "" , " -o " + str(value) )[ value is not None and value != vdef]</code>
      </format>
    </parameter>
    
    <paragraph>
      <name>output_options</name>
      <prompt lang="en">Output options</prompt>
      <parameters>
        
        <parameter isout="1">
          <name>outfile</name>
          <prompt lang="en">Result file</prompt>
          <type>
            <datatype>
              <class>RnaplotdOutput</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="perl">"rna.*"</code>
            <code proglang="python">"rna.*"</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>
  </parameters>
</program>

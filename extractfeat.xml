<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>extractfeat</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>extractfeat</title>
      <description>
        <text lang="en">Extract features from sequence(s)</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/extractfeat.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:edit:feature_table</category>
    <command>extractfeat</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_additional</name>
      <prompt lang="en">Additional section</prompt>

      <parameters>

        <parameter>
          <name>e_before</name>
          <prompt lang="en">Amount of sequence before feature to extract</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -before=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>2</argpos>
          <comment>
            <text lang="en">If this value is greater than 0 then that number of bases or residues before the feature are included in the extracted  sequence. This allows you to get the context of the feature. If  this value is negative then the start of the extracted sequence  will be this number of bases/residues before the end of the  feature. So a value of '10' will start the extraction 10  bases/residues before the start of the sequence, and a value of  '-10' will start the extraction 10 bases/residues before the end  of the feature. The output sequence will be padded with 'N' or 'X'  characters if the sequence starts after the required start of the  extraction.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_after</name>
          <prompt lang="en">Amount of sequence after feature to extract</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -after=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>3</argpos>
          <comment>
            <text lang="en">If this value is greater than 0 then that number of bases or residues after the feature are included in the extracted  sequence. This allows you to get the context of the feature. If  this value is negative then the end of the extracted sequence will  be this number of bases/residues after the start of the feature.  So a value of '10' will end the extraction 10 bases/residues after  the end of the sequence, and a value of '-10' will end the  extraction 10 bases/residues after the start of the feature. The  output sequence will be padded with 'N' or 'X' characters if the  sequence ends before the required end of the extraction.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_source</name>
          <prompt lang="en">Source of feature to display</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -source=" + str(value))[value is not None]</code>
          </format>
          <argpos>4</argpos>
          <comment>
            <text lang="en">By default any feature source in the feature table is shown. You can set this to match any feature source you wish to  show. 
  The source name is usually either the name of the program that  detected the feature or it is the feature table (eg: EMBL) that  the feature came from. 
  The source may be wildcarded by using '*'. 
  If you wish to show more than one source, separate their names  with the character '|', eg: 
  gene* | embl</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_type</name>
          <prompt lang="en">Type of feature to extract</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -type=" + str(value))[value is not None]</code>
          </format>
          <argpos>5</argpos>
          <comment>
            <text lang="en">By default every feature in the feature table is extracted. You can set this to be any feature type you wish to extract. 
  See http://www.ebi.ac.uk/embl/WebFeat/ for a list of the EMBL  feature types and see the Uniprot user manual in  http://www.uniprot.org/manual/sequence_annotation  for a list of the Uniprot feature types. 
  The type may be wildcarded by using '*'. 
  If you wish to extract more than one type, separate their names  with the character '|', eg: 
  *UTR | intron</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_sense</name>
          <prompt lang="en">Sense of feature to extract</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -sense=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>6</argpos>
          <comment>
            <text lang="en">By default any feature type in the feature table is extracted. You can set this to match any feature sense you wish. 0  - any sense, 1 - forward sense, -1 - reverse sense</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_minscore</name>
          <prompt lang="en">Minimum score of feature to extract</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -minscore=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>7</argpos>
          <comment>
            <text lang="en">Minimum score of feature to extract (see also maxscore)</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_maxscore</name>
          <prompt lang="en">Maximum score of feature to extract</prompt>
          <type>
            <datatype>
              <class>Float</class>
            </datatype>
          </type>
          <vdef>
            <value>0.0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -maxscore=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>8</argpos>
          <comment>
            <text lang="en">Maximum score of feature to extract. 
 If both minscore and maxscore are zero (the default),  then any score is ignored</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_tag</name>
          <prompt lang="en">Tag of feature to extract</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -tag=" + str(value))[value is not None]</code>
          </format>
          <argpos>9</argpos>
          <comment>
            <text lang="en">Tags are the types of extra values that a feature may have. For example in the EMBL feature table, a 'CDS' type of feature  may have the tags '/codon', '/codon_start', '/db_xref',  '/EC_number', '/evidence', '/exception', '/function', '/gene',  '/label', '/map', '/note', '/number', '/partial', '/product',  '/protein_id', '/pseudo', '/standard_name', '/translation',  '/transl_except', '/transl_table', or '/usedin'. Some of these  tags also have values, for example '/gene' can have the value of  the gene name. 
  By default any feature tag in the feature table is extracted. You  can set this to match any feature tag you wish to show. 
  The tag may be wildcarded by using '*'. 
  If you wish to extract more than one tag, separate their names  with the character '|', eg: 
  gene | label</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_value</name>
          <prompt lang="en">Value of feature tags to extract</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -value=" + str(value))[value is not None]</code>
          </format>
          <argpos>10</argpos>
          <comment>
            <text lang="en">Tag values are the values associated with a feature tag. Tags are the types of extra values that a feature may have. For  example in the EMBL feature table, a 'CDS' type of feature may  have the tags '/codon', '/codon_start', '/db_xref', '/EC_number',  '/evidence', '/exception', '/function', '/gene', '/label', '/map',  '/note', '/number', '/partial', '/product', '/protein_id',  '/pseudo', '/standard_name', '/translation', '/transl_except',  '/transl_table', or '/usedin'. Only some of these tags can have  values, for example '/gene' can have the value of the gene name.  By default any feature tag value in the feature table is shown.  You can set this to match any feature tag value you wish to show. 
  The tag value may be wildcarded by using '*'. 
  If you wish to show more than one tag value, separate their names  with a space or the character '|', eg: 
  pax* | 10</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_join</name>
          <prompt lang="en">Output introns etc. as one sequence</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -join")[ bool(value) ]</code>
          </format>
          <argpos>11</argpos>
          <comment>
            <text lang="en">Some features, such as CDS (coding sequence) and mRNA are composed of introns concatenated together. There may be other  forms of 'joined' sequence, depending on the feature table. If  this option is set TRUE, then any group of these features will be  output as a single sequence. If the 'before' and 'after'  qualifiers have been set, then only the sequence before the first  feature and after the last feature are added.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_featinname</name>
          <prompt lang="en">Append type of feature to output sequence name</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -featinname")[ bool(value) ]</code>
          </format>
          <argpos>12</argpos>
          <comment>
            <text lang="en">To aid you in identifying the type of feature that has been output, the type of feature is added to the start of the  description of the output sequence. Sometimes the description of a  sequence is lost in subsequent processing of the sequences file,  so it is useful for the type to be a part of the sequence ID name.  If you set this to be TRUE then the name is added to the ID name  of the output sequence.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_describe</name>
          <prompt lang="en">Feature tag names to add to the description</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -describe=" + str(value))[value is not None]</code>
          </format>
          <argpos>13</argpos>
          <comment>
            <text lang="en">To aid you in identifying some further properties of a feature that has been output, this lets you specify one or more  tag names that should be added to the output sequence Description  text, together with their values (if any). For example, if this is  set to be 'gene', then if any output feature has the tag (for  example) '/gene=BRCA1' associated with it, then the text  '(gene=BRCA1)' will be added to the Description line. Tags are the  types of extra values that a feature may have. For example in the  EMBL feature table, a 'CDS' type of feature may have the tags  '/codon', '/codon_start', '/db_xref', '/EC_number', '/evidence',  '/exception', '/function', '/gene', '/label', '/map', '/note',  '/number', '/partial', '/product', '/protein_id', '/pseudo',  '/standard_name', '/translation', '/transl_except',  '/transl_table', or '/usedin'. Some of these tags also have  values, for example '/gene' can have the value of the gene name. 
  By default no feature tag is displayed. You can set this to match  any feature tag you wish to show. 
  The tag may be wildcarded by using '*'. 
  If you wish to extract more than one tag, separate their names  with the character '|', eg: 
  gene | label</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_outseq</name>
          <prompt lang="en">Name of the output sequence file (e_outseq)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>extractfeat.e_outseq</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outseq=" + str(value))[value is not None]</code>
          </format>
          <argpos>14</argpos>
        </parameter>

        <parameter>
          <name>e_osformat_outseq</name>
          <prompt lang="en">Choose the sequence output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>FASTA</value>
          </vdef>
          <vlist>
            <velem>
              <value>EMBL</value>
              <label>Embl</label>
            </velem>
            <velem>
              <value>FASTA</value>
              <label>Fasta</label>
            </velem>
            <velem>
              <value>GCG</value>
              <label>Gcg</label>
            </velem>
            <velem>
              <value>GENBANK</value>
              <label>Genbank</label>
            </velem>
            <velem>
              <value>NBRF</value>
              <label>Nbrf</label>
            </velem>
            <velem>
              <value>CODATA</value>
              <label>Codata</label>
            </velem>
            <velem>
              <value>RAW</value>
              <label>Raw</label>
            </velem>
            <velem>
              <value>SWISSPROT</value>
              <label>Swissprot</label>
            </velem>
            <velem>
              <value>GFF</value>
              <label>Gff</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -osformat=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>15</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outseq_out</name>
          <prompt lang="en">outseq_out option</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>
              <ref param="e_osformat_outseq">
              </ref>
            </dataFormat>
          </type>
          <filenames>
            <code proglang="python">e_outseq</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>16</argpos>
    </parameter>
  </parameters>
</program>

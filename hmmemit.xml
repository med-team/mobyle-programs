<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>hmmemit</name>
    <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="Entities/hmmer_package.xml"/>
    <doc>
      <title>HMMEMIT</title>
      <description>
        <text lang="en">Generate sequences from a profile HMM</text>
      </description>
      <comment>
        <text lang="en">hmmemit reads an HMM file from hmmfile and generates a number of sequences from it; or, if the -c option is selected, generate a single majority-rule consensus. This can be useful for various applications in which one needs a simulation of sequences consistent with a sequence family consensus.By default, hmmemit generates 10 sequences and outputs them in FASTA (unaligned) format.</text>
      </comment>
    </doc>
    <category>hmm:building</category>
  </head>
  <parameters>
    <parameter iscommand="1" ishidden="1">
      <name>hmmcmd</name>
      <prompt lang="en">HMM emit command</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">"hmmemit"</code>
        <code proglang="python">"hmmemit"</code>
      </format>
      <argpos>0</argpos>
    </parameter>
    <parameter ismandatory="1" issimple="1">
      <name>hmmfile</name>
      <prompt lang="en">HMM file</prompt>
      <type>
        <datatype>
          <class>HmmProfile</class>
          <superclass>AbstractText</superclass>
        </datatype>
      </type>
      <format>
        <code proglang="perl">" $hmmfile"</code>
        <code proglang="python">" " + str(hmmfile)</code>
      </format>
      <argpos>2</argpos>
    </parameter>
    <parameter>
      <name>outfile_name</name>
      <prompt lang="en">Name of the synthetic sequences file (-o)</prompt>
      <type>
        <datatype>
          <class>Filename</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">(defined $value) ? " -o $value" : ""</code>
        <code proglang="python">( "" , " -o " + str(value) )[ value is not None ]</code>
      </format>
      <argpos>1</argpos>
      <comment>
        <text lang="en">Save the synthetic sequences to file rather than writing them to stdout.</text>
      </comment>
    </parameter>
    <parameter isout="1">
      <name>output_file</name>
      <prompt lang="en">Output file</prompt>
      <type>
        <datatype>
          <class>Sequence</class>
        </datatype>
        <dataFormat>FASTA</dataFormat>
      </type>
      <precond>
        <code proglang="perl">defined $outfile_name</code>
        <code proglang="python">outfile_name is not None</code>
      </precond>
      <filenames>
        <code proglang="perl">$outfile_name</code>
        <code proglang="python">str(outfile_name)</code>
      </filenames>
    </parameter>
    <parameter isstdout="1">
      <name>out_file</name>
      <prompt lang="en">Output file</prompt>
      <type>
        <datatype>
          <class>Sequence</class>
        </datatype>
        <dataFormat>FASTA</dataFormat>
      </type>
      <precond>
        <code proglang="perl">not defined $outfile_name</code>
        <code proglang="python">outfile_name is None</code>
      </precond>
      <filenames>
        <code proglang="perl">"hmmemit.out"</code>
        <code proglang="python">"hmmemit.out"</code>
      </filenames>
    </parameter>
    <!--<parameter isout="1">
      <name>output_align_name</name>
      <prompt lang="en">Output align file</prompt>
      <type>
        <datatype>
          <class>Alignment</class>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$outfile_name and defined $output_format</code>
        <code proglang="python">outfile_name and output_format is not None</code>
      </precond>
      <filenames>
        <code proglang="perl">$outfile_name</code>
        <code proglang="python">str(outfile_name)</code>
      </filenames>
    </parameter>
    -->
    <parameter>
      <name>consensus</name>
      <prompt lang="en">Consensus sequence (-c)</prompt>
      <type>
        <datatype>
          <class>Boolean</class>
        </datatype>
      </type>
      <precond>
        <code proglang="perl">$number == 1 and not sample</code>
        <code proglang="python">number ==  1 and not sample</code>
      </precond>
      <vdef>
        <value>0</value>
      </vdef>
      <format>
        <code proglang="perl">($value) ? " -c" : ""</code>
        <code proglang="python">( "" , " -c" )[ value ]</code>
      </format>
      <argpos>1</argpos>
      <comment>
        <text lang="en">Emit a consensus sequence, instead of sampling a sequence from the profile
        HMM's probability distribution. The consensus sequence is formed by selecting
        the maximum probability residue at each match state.</text>
      </comment>
    </parameter>
    <parameter>
      <name>number</name>
      <prompt lang="en">Number of sequences to sample (-N)</prompt>
      <type>
        <datatype>
          <class>Integer</class>
        </datatype>
      </type>
      <vdef>
        <value>1</value>
      </vdef>
      <format>
        <code proglang="perl">(defined $value and $value != $vdef) ? " -N $value " : ""</code>
        <code proglang="python">( "" , " -N " + str(value) )[ value is not None and value != vdef]</code>
      </format>
      <argpos>1</argpos>
      <ctrl>
        <message>
          <text lang="en">Enter a value &gt; 0</text>
        </message>
        <code proglang="perl">0 &lt; $value</code>
        <code proglang="python">0 &lt; value</code>
      </ctrl>
      <comment>
        <text lang="en">Sample x sequences, rather than just one.</text>
      </comment>
    </parameter>
    <parameter>
      <name>sample</name>
      <prompt lang="en">Sample from profile, not core model (-p)</prompt>
      <type>
        <datatype>
          <class>Boolean</class>
        </datatype>
      </type>
      <vdef>
        <value>0</value>
      </vdef>
      <format>
        <code proglang="perl">( $value ) ? " -p " : ""</code>
        <code proglang="python">( "" , " -p " )[ value ]</code>
      </format>
      <argpos>1</argpos>
      <comment>
        <text lang="en">Sample sequences from the implicit profile, not from the core model. 
        The core
        model consists only of the homologous states (between the begin and end states
        of a HMMER Plan7 model). The profile includes the nonhomologous N, C, and J
        states, local/glocal and uni/multihit algorithm configuration, and the target length
        model. Therefore sequences sampled from a profile may include nonhomologous
        as well as homologous sequences, and may contain more than one homologous
        sequence segment. By default, the profile is in multihit local mode, and the target
        sequence length is configured for L=400. To change these defaults, see Options
        Controlling Emission from Profiles, below.</text>
      </comment>
    </parameter>
    <paragraph>
      <name>controlP</name>
      <prompt lang="en">Options controlling emission from profiles (with -p)</prompt>
      <precond>
        <code proglang="perl">define $sample</code>
        <code proglang="python">sample</code>
      </precond>
      <parameters>
        <parameter>
          <name>len</name>
          <prompt lang="en">Set expected length from profile (-L)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>400</value>
          </vdef>
          <format>
            <code proglang="perl">(defined $value and $value != $vdef) ? " -L $value " : ""</code>
            <code proglang="python">( "" , " -L " + str(value) )[ value is not None and value != vdef]</code>
          </format>
          <argpos>1</argpos>
          <comment>
            <text lang="en">Configure the profile's target sequence length model to generate a mean length of
          approximately the value rather than the default of 400.</text>
          </comment>
        </parameter>
        <parameter>
          <name>local</name>
          <prompt>Configure profile mode</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>null</value>
          </vdef>
          <vlist>
            <velem undef="1">
              <value>null</value>
              <label/>
            </velem>
            <velem>
              <value>local</value>
              <label>local mode</label>
            </velem>
            <velem>
              <value>unilocal</value>
              <label>unilocal mode</label>
            </velem>
            <velem>
              <value>glocal</value>
              <label>glocal mode</label>
            </velem>
            <velem>
              <value>uniglocal</value>
              <label>uniglocal mode</label>
            </velem>
          </vlist>
          <format>
            <code proglang="perl">(defined $value) ? " --$value " : ""</code>
            <code proglang="python">( "" , " --" + str(value) )[ value is not None ]</code>
          </format>
        </parameter>
      </parameters>
    </paragraph>
    <parameter>
      <name>seed</name>
      <prompt lang="en">Seed number (--seed)</prompt>
      <type>
        <datatype>
          <class>Integer</class>
        </datatype>
      </type>
      <vdef>
        <value>0</value>
      </vdef>
      <format>
        <code proglang="perl">(defined $value and $value != $vdef) ? " --seed $value " : ""</code>
        <code proglang="python">( "" , " --seed " + str(value) )[ value is not None and value !=vdef ]</code>
      </format>
      <argpos>1</argpos>
      <comment>
        <text lang="en">Seed the random number generator with the value, an integer &gt;= 0. If the value is nonzero,
          any stochastic simulations will be reproducible; the same command will give the
          same results. If the value is 0, the random number generator is seeded arbitrarily, and
          stochastic simulations will vary from run to run of the same command. The default
          is 0: use an arbitrary seed, so different hmmemit runs will generate different
          samples.</text>
      </comment>
      <ctrl>
        <message>
          <text lang="en">Enter a value &gt;= 0</text>
        </message>
        <code proglang="perl">0 &lt;= $value</code>
        <code proglang="python">0 &lt;= value</code>
      </ctrl>
    </parameter>
  </parameters>
</program>

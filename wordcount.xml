<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>wordcount</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>wordcount</title>
      <description>
        <text lang="en">Count and extract unique words in molecular sequence(s)</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/wordcount.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>nucleic:composition</category>
    <category>protein:composition</category>
    <command>wordcount</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_required</name>
      <prompt lang="en">Required section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_wordsize</name>
          <prompt lang="en">Word size (value greater than or equal to 1)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -wordsize=" + str(value))[value is not None]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <argpos>2</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_additional</name>
      <prompt lang="en">Additional section</prompt>

      <parameters>

        <parameter>
          <name>e_mincount</name>
          <prompt lang="en">Minimum word count to report (value greater than or equal to 1)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>1</value>
          </vdef>
          <format>
            <code proglang="python">("", " -mincount=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <argpos>3</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the output file (e_outfile)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>wordcount.e_outfile</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>4</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>WordcountReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>5</argpos>
    </parameter>
  </parameters>
</program>

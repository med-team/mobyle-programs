<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>pepwindowall</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>pepwindowall</title>
      <description>
        <text lang="en">Draw Kyte-Doolittle hydropathy plot for a protein alignment</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/pepwindowall.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:protein:composition</category>
    <command>pepwindowall</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequences</name>
          <prompt lang="en">sequences option</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>Alignment</class>
            </datatype>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>MSF</dataFormat>
            <dataFormat>PAIR</dataFormat>
            <dataFormat>MARKX0</dataFormat>
            <dataFormat>MARKX1</dataFormat>
            <dataFormat>MARKX2</dataFormat>
            <dataFormat>MARKX3</dataFormat>
            <dataFormat>MARKX10</dataFormat>
            <dataFormat>SRS</dataFormat>
            <dataFormat>SRSPAIR</dataFormat>
            <dataFormat>SCORE</dataFormat>
            <dataFormat>UNKNOWN</dataFormat>
            <dataFormat>MULTIPLE</dataFormat>
            <dataFormat>SIMPLE</dataFormat>
            <dataFormat>MATCH</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequences=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
          <comment>
            <text lang="en">File containing a sequence alignment</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_datafile</name>
          <prompt lang="en">Aaindex entry data file</prompt>
          <type>
            <biotype>Protein</biotype>
            <datatype>
              <class>AaindexData</class>
              <superclass>AbstractText</superclass>
            </datatype>
          </type>
          <format>
            <code proglang="python">("", " -datafile=" + str(value))[value is not None ]</code>
          </format>
          <argpos>2</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_additional</name>
      <prompt lang="en">Additional section</prompt>

      <parameters>

        <parameter>
          <name>e_length</name>
          <prompt lang="en">Window size (value from 1 to 200)</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>19</value>
          </vdef>
          <format>
            <code proglang="python">("", " -length=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <ctrl>
            <message>
              <text lang="en">Value greater than or equal to 1 is required</text>
            </message>
            <code proglang="python">value &gt;= 1</code>
          </ctrl>
          <ctrl>
            <message>
              <text lang="en">Value less than or equal to 200 is required</text>
            </message>
            <code proglang="python">value &lt;= 200</code>
          </ctrl>
          <argpos>3</argpos>
        </parameter>

        <parameter>
          <name>e_normalize</name>
          <prompt lang="en">Normalize data values</prompt>
          <type>
            <datatype>
              <class>Boolean</class>
            </datatype>
          </type>
          <vdef>
            <value>0</value>
          </vdef>
          <format>
            <code proglang="python">("", " -normalize")[ bool(value) ]</code>
          </format>
          <argpos>4</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_graph</name>
          <prompt lang="en">Choose the e_graph output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>png</value>
          </vdef>
          <vlist>
            <velem>
              <value>png</value>
              <label>Png</label>
            </velem>
            <velem>
              <value>gif</value>
              <label>Gif</label>
            </velem>
            <velem>
              <value>cps</value>
              <label>Cps</label>
            </velem>
            <velem>
              <value>ps</value>
              <label>Ps</label>
            </velem>
            <velem>
              <value>meta</value>
              <label>Meta</label>
            </velem>
            <velem>
              <value>data</value>
              <label>Data</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">(" -graph=" + str(vdef), " -graph=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>5</argpos>
        </parameter>

        <parameter>
          <name>xy_goutfile</name>
          <prompt lang="en">Name of the output graph</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>pepwindowall_xygraph</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -goutfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>6</argpos>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_png</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "png"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.png"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_gif</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "gif"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.gif"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_ps</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>PostScript</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "ps" or e_graph == "cps"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.ps"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_meta</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Picture</class>
              <superclass>Binary</superclass>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "meta"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.meta"</code>
          </filenames>
        </parameter>

        <parameter isout="1">
          <name>xy_outgraph_data</name>
          <prompt lang="en">Graph file</prompt>
          <type>
            <datatype>
              <class>Text</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_graph == "data"</code>
          </precond>
          <filenames>
            <code proglang="python">"*.dat"</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>7</argpos>
    </parameter>
  </parameters>
</program>

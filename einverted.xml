<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>einverted</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>einverted</title>
      <description>
        <text lang="en">Finds inverted repeats in nucleotide sequences</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/einverted.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:nucleic:repeats</category>
    <category>sequence:nucleic:2D_structure</category>
    <category>structure:2D_structure</category>
    <command>einverted</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <biotype>DNA</biotype>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>EMBL</dataFormat>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>GCG</dataFormat>
            <dataFormat>GENBANK</dataFormat>
            <dataFormat>NBRF</dataFormat>
            <dataFormat>CODATA</dataFormat>
            <dataFormat>RAW</dataFormat>
            <dataFormat>SWISSPROT</dataFormat>
            <dataFormat>GFF</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_required</name>
      <prompt lang="en">Required section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_gap</name>
          <prompt lang="en">Gap penalty</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>12</value>
          </vdef>
          <format>
            <code proglang="python">("", " -gap=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>2</argpos>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_threshold</name>
          <prompt lang="en">Minimum score threshold</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>50</value>
          </vdef>
          <format>
            <code proglang="python">("", " -threshold=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>3</argpos>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_match</name>
          <prompt lang="en">Match score</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>3</value>
          </vdef>
          <format>
            <code proglang="python">("", " -match=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>4</argpos>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_mismatch</name>
          <prompt lang="en">Mismatch score</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>-4</value>
          </vdef>
          <format>
            <code proglang="python">("", " -mismatch=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>5</argpos>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_additional</name>
      <prompt lang="en">Additional section</prompt>

      <parameters>

        <parameter>
          <name>e_maxrepeat</name>
          <prompt lang="en">Maximum extent of repeats</prompt>
          <type>
            <datatype>
              <class>Integer</class>
            </datatype>
          </type>
          <vdef>
            <value>2000</value>
          </vdef>
          <format>
            <code proglang="python">("", " -maxrepeat=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>6</argpos>
          <comment>
            <text lang="en">Maximum separation between the start of repeat and the end of the inverted repeat (the default is 2000 bases).</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the output file (e_outfile)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>outfile.inv</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>7</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>InvertedReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>

        <parameter>
          <name>e_outseq</name>
          <prompt lang="en">Name of the output sequence file (e_outseq)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>einverted.e_outseq</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outseq=" + str(value))[value is not None]</code>
          </format>
          <argpos>8</argpos>
          <comment>
            <text lang="en">The sequence of the inverted repeat regions without gap characters.</text>
          </comment>
        </parameter>

        <parameter>
          <name>e_osformat_outseq</name>
          <prompt lang="en">Choose the sequence output format</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>FASTA</value>
          </vdef>
          <vlist>
            <velem>
              <value>EMBL</value>
              <label>Embl</label>
            </velem>
            <velem>
              <value>FASTA</value>
              <label>Fasta</label>
            </velem>
            <velem>
              <value>GCG</value>
              <label>Gcg</label>
            </velem>
            <velem>
              <value>GENBANK</value>
              <label>Genbank</label>
            </velem>
            <velem>
              <value>NBRF</value>
              <label>Nbrf</label>
            </velem>
            <velem>
              <value>CODATA</value>
              <label>Codata</label>
            </velem>
            <velem>
              <value>RAW</value>
              <label>Raw</label>
            </velem>
            <velem>
              <value>SWISSPROT</value>
              <label>Swissprot</label>
            </velem>
            <velem>
              <value>GFF</value>
              <label>Gff</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -osformat=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>9</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outseq_out</name>
          <prompt lang="en">outseq_out option</prompt>
          <type>
            <datatype>
              <class>Sequence</class>
            </datatype>
            <dataFormat>
              <ref param="e_osformat_outseq">
              </ref>
            </dataFormat>
          </type>
          <filenames>
            <code proglang="python">e_outseq</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>10</argpos>
    </parameter>
  </parameters>
</program>

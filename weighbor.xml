<?xml version='1.0' encoding='UTF-8'?>
<!-- XML Authors: Corinne Maufrais, Nicolas Joly and Bertrand Neron,             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->
<program>
  <head>
    <name>weighbor</name>
    <version>1.2.1</version>
    <doc>
      <title>Weighbor</title>
      <description>
        <text lang="en">Weighted neighbor joining</text>
      </description>
      <authors>Bruno, Halpern, Socci</authors>
      <reference>W. J. Bruno, N. D. Socci, and A. L. Halpern. Weighted Neighbor Joining: A Likelihood-Based Approach to Distance-Based Phylogeny Reconstruction, Mol. Biol. Evol. 17 (1): 189-197 (2000).</reference>
      <comment>
        <text lang="en">Weighbor takes an input file of pairwise distances in Phylip format and computes the phylogentic tree that best
       corresponds to those distances.</text>
      </comment>
      <homepagelink>http://www.is.titech.ac.jp/~shimo/prog/consel/</homepagelink>
      <sourcelink>http://www.is.titech.ac.jp/~shimo/prog/consel/</sourcelink>
    </doc>
    <category>phylogeny:distance</category>
    <command>weighbor</command>
  </head>
  <parameters>
    <parameter ismandatory="1" issimple="1">
      <name>infile</name>
      <prompt lang="en">Distances matrix File (-i)</prompt>
      <type>
        <datatype>
          <class>PhylipDistanceMatrix</class>
          <superclass>AbstractText</superclass>
        </datatype>
      </type>
      <format>
        <code proglang="perl">" -i $value"</code>
        <code proglang="python">" -i " + str(value)</code>
      </format>
      <argpos>1</argpos>
    </parameter>
    <parameter issimple="1">
      <name>Length</name>
      <prompt lang="en">Length of the sequences (-L)</prompt>
      <type>
        <datatype>
          <class>Integer</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">(defined $value) ? " -L $value" : ""</code>
        <code proglang="python">( "" , " -L " + str(value) )[ value is not None ]</code>
      </format>
      <argpos>2</argpos>
      <comment>
        <text lang="en">Default is 500. This is the effective sequence length equal to the number of varying sites. Note if the -L option is not used then the program will print a warning message to stderr indicating that it is using this default length.</text>
      </comment>
    </parameter>
    <parameter>
      <name>size</name>
      <prompt lang="en">Size of the alphabet (-b)</prompt>
      <type>
        <datatype>
          <class>Integer</class>
        </datatype>
      </type>
      <vdef>
        <value>4</value>
      </vdef>
      <format>
        <code proglang="perl">(defined $value and $value != $vdef) ? " -b $value" : ""</code>
        <code proglang="python">( "" , " -b " + str(value) )[ value is not None and value != vdef]</code>
      </format>
      <argpos>2</argpos>
      <comment>
        <text lang="en">Sets the size of the alphabet of characters (number of bases) b. 1/b is equal to the probability that there will be a match for infinite evolution time. The default value for b is 4.</text>
      </comment>
    </parameter>
    <parameter>
      <name>verbose</name>
      <prompt lang="en">Verbose output (-v)</prompt>
      <type>
        <datatype>
          <class>Choice</class>
        </datatype>
      </type>
      <vdef>
        <value>Null</value>
      </vdef>
      <vlist>
        <velem undef="1">
          <value>Null</value>
          <label>Choose a verbose type</label>
        </velem>
        <velem>
          <value>-v</value>
          <label>Verbose (-v)</label>
        </velem>
        <velem>
          <value>-vv</value>
          <label>Very verbose (-vv)</label>
        </velem>
        <velem>
          <value>-vvv</value>
          <label>Very very verbose (-vvv)</label>
        </velem>
      </vlist>
      <format>
        <code proglang="perl">(defined $value and $value ne $vdef) ? " $value" : ""</code>
        <code proglang="python">( "" , " " + str(value) )[ value is not None and value != vdef]</code>
      </format>
      <argpos>2</argpos>
    </parameter>
    <parameter ishidden="1">
      <name>outfile</name>
      <prompt lang="en">Output file (-o)</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="perl">" -o weighbor.treefile"</code>
        <code proglang="python">" -o weighbor.treefile"</code>
      </format>
      <argpos>3</argpos>
    </parameter>
    <parameter isout="1">
      <name>treefile</name>
      <prompt lang="en">Tree output file</prompt>
      <type>
        <datatype>
          <class>Tree</class>
        </datatype>
        <dataFormat>NEWICK</dataFormat>
      </type>
      <filenames>
        <code proglang="perl">"weighbor.treefile"</code>
        <code proglang="python">"weighbor.treefile"</code>
      </filenames>
    </parameter>
  </parameters>
</program>


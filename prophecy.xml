<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- XML Authors: Corinne Maufrais and Nicolas Joly,                             -->
<!-- 'Biological Software and Databases' Group, Institut Pasteur, Paris.         -->
<!-- Distributed under LGPLv2 License. Please refer to the COPYING.LIB document. -->

<program>
  <head>
    <name>prophecy</name>
    <package>
      <name>EMBOSS</name>
      <version>6.3.1</version>
      <doc>
        <title>EMBOSS</title>
        <description>
          <text lang="en">European Molecular Biology Open Software Suite</text>
        </description>
        <authors>Rice,P. Longden,I. and Bleasby, A.</authors>
        <reference>EMBOSS: The European Molecular Biology Open Software Suite (2000)  Rice,P. Longden,I. and Bleasby, A. Trends in Genetics 16, (6) pp276--277</reference>
        <sourcelink>http://emboss.sourceforge.net/download</sourcelink>
        <homepagelink>http://emboss.sourceforge.net</homepagelink>
      </doc>
    </package>
    <doc>
      <title>prophecy</title>
      <description>
        <text lang="en">Create frequency matrix or profile from a multiple alignment</text>
      </description>
      <doclink>http://bioweb2.pasteur.fr/docs/EMBOSS/prophecy.html</doclink>
      <doclink>http://emboss.sourceforge.net/docs/themes</doclink>
    </doc>
    <category>sequence:nucleic:profiles</category>
    <category>sequence:protein:profiles</category>
    <command>prophecy</command>
  </head>

  <parameters>

    <paragraph>
      <name>e_input</name>
      <prompt lang="en">Input section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_sequence</name>
          <prompt lang="en">sequence option</prompt>
          <type>
            <datatype>
              <class>Alignment</class>
            </datatype>
            <dataFormat>FASTA</dataFormat>
            <dataFormat>MSF</dataFormat>
            <dataFormat>PAIR</dataFormat>
            <dataFormat>MARKX0</dataFormat>
            <dataFormat>MARKX1</dataFormat>
            <dataFormat>MARKX2</dataFormat>
            <dataFormat>MARKX3</dataFormat>
            <dataFormat>MARKX10</dataFormat>
            <dataFormat>SRS</dataFormat>
            <dataFormat>SRSPAIR</dataFormat>
            <dataFormat>SCORE</dataFormat>
            <dataFormat>UNKNOWN</dataFormat>
            <dataFormat>MULTIPLE</dataFormat>
            <dataFormat>SIMPLE</dataFormat>
            <dataFormat>MATCH</dataFormat>
            <card>1,n</card>
          </type>
          <format>
            <code proglang="python">("", " -sequence=" + str(value))[value is not None]</code>
          </format>
          <argpos>1</argpos>
        </parameter>

        <parameter issimple="1" ismandatory="1">
          <name>e_type</name>
          <prompt lang="en">Profile type</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <vdef>
            <value>F</value>
          </vdef>
          <vlist>
            <velem>
              <value>F</value>
              <label>Frequency</label>
            </velem>
            <velem>
              <value>G</value>
              <label>Gribskov</label>
            </velem>
            <velem>
              <value>H</value>
              <label>Henikoff</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -type=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>2</argpos>
        </parameter>

        <parameter>
          <name>e_datafile</name>
          <prompt lang="en">Scoring matrix</prompt>
          <type>
            <datatype>
              <class>Choice</class>
            </datatype>
          </type>
          <precond>
            <code proglang="python">e_type !="F"</code>
          </precond>
          <vdef>
            <value>mobyle_null</value>
          </vdef>
          <vlist>
            <velem undef="1">
              <value>mobyle_null</value>
              <label></label>
            </velem>
            <velem>
              <value>EBLOSUM30</value>
              <label>Eblosum30</label>
            </velem>
            <velem>
              <value>EBLOSUM35</value>
              <label>Eblosum35</label>
            </velem>
            <velem>
              <value>EBLOSUM40</value>
              <label>Eblosum40</label>
            </velem>
            <velem>
              <value>EBLOSUM45</value>
              <label>Eblosum45</label>
            </velem>
            <velem>
              <value>EBLOSUM50</value>
              <label>Eblosum50</label>
            </velem>
            <velem>
              <value>EBLOSUM55</value>
              <label>Eblosum55</label>
            </velem>
            <velem>
              <value>EBLOSUM60</value>
              <label>Eblosum60</label>
            </velem>
            <velem>
              <value>EBLOSUM62</value>
              <label>Eblosum62</label>
            </velem>
            <velem>
              <value>EBLOSUM62-12</value>
              <label>Eblosum62-12</label>
            </velem>
            <velem>
              <value>EBLOSUM65</value>
              <label>Eblosum65</label>
            </velem>
            <velem>
              <value>EBLOSUM70</value>
              <label>Eblosum70</label>
            </velem>
            <velem>
              <value>EBLOSUM75</value>
              <label>Eblosum75</label>
            </velem>
            <velem>
              <value>EBLOSUM80</value>
              <label>Eblosum80</label>
            </velem>
            <velem>
              <value>EBLOSUM85</value>
              <label>Eblosum85</label>
            </velem>
            <velem>
              <value>EBLOSUM90</value>
              <label>Eblosum90</label>
            </velem>
            <velem>
              <value>EBLOSUMN</value>
              <label>Eblosumn</label>
            </velem>
            <velem>
              <value>EDNAFULL</value>
              <label>Ednafull</label>
            </velem>
            <velem>
              <value>EDNAMAT</value>
              <label>Ednamat</label>
            </velem>
            <velem>
              <value>EDNASIMPLE</value>
              <label>Ednasimple</label>
            </velem>
            <velem>
              <value>EPAM10</value>
              <label>Epam10</label>
            </velem>
            <velem>
              <value>EPAM100</value>
              <label>Epam100</label>
            </velem>
            <velem>
              <value>EPAM110</value>
              <label>Epam110</label>
            </velem>
            <velem>
              <value>EPAM120</value>
              <label>Epam120</label>
            </velem>
            <velem>
              <value>EPAM130</value>
              <label>Epam130</label>
            </velem>
            <velem>
              <value>EPAM140</value>
              <label>Epam140</label>
            </velem>
            <velem>
              <value>EPAM150</value>
              <label>Epam150</label>
            </velem>
            <velem>
              <value>EPAM160</value>
              <label>Epam160</label>
            </velem>
            <velem>
              <value>EPAM170</value>
              <label>Epam170</label>
            </velem>
            <velem>
              <value>EPAM180</value>
              <label>Epam180</label>
            </velem>
            <velem>
              <value>EPAM190</value>
              <label>Epam190</label>
            </velem>
            <velem>
              <value>EPAM20</value>
              <label>Epam20</label>
            </velem>
            <velem>
              <value>EPAM200</value>
              <label>Epam200</label>
            </velem>
            <velem>
              <value>EPAM210</value>
              <label>Epam210</label>
            </velem>
            <velem>
              <value>EPAM220</value>
              <label>Epam220</label>
            </velem>
            <velem>
              <value>EPAM230</value>
              <label>Epam230</label>
            </velem>
            <velem>
              <value>EPAM240</value>
              <label>Epam240</label>
            </velem>
            <velem>
              <value>EPAM250</value>
              <label>Epam250</label>
            </velem>
            <velem>
              <value>EPAM260</value>
              <label>Epam260</label>
            </velem>
            <velem>
              <value>EPAM270</value>
              <label>Epam270</label>
            </velem>
            <velem>
              <value>EPAM280</value>
              <label>Epam280</label>
            </velem>
            <velem>
              <value>EPAM290</value>
              <label>Epam290</label>
            </velem>
            <velem>
              <value>EPAM30</value>
              <label>Epam30</label>
            </velem>
            <velem>
              <value>EPAM300</value>
              <label>Epam300</label>
            </velem>
            <velem>
              <value>EPAM310</value>
              <label>Epam310</label>
            </velem>
            <velem>
              <value>EPAM320</value>
              <label>Epam320</label>
            </velem>
            <velem>
              <value>EPAM330</value>
              <label>Epam330</label>
            </velem>
            <velem>
              <value>EPAM340</value>
              <label>Epam340</label>
            </velem>
            <velem>
              <value>EPAM350</value>
              <label>Epam350</label>
            </velem>
            <velem>
              <value>EPAM360</value>
              <label>Epam360</label>
            </velem>
            <velem>
              <value>EPAM370</value>
              <label>Epam370</label>
            </velem>
            <velem>
              <value>EPAM380</value>
              <label>Epam380</label>
            </velem>
            <velem>
              <value>EPAM390</value>
              <label>Epam390</label>
            </velem>
            <velem>
              <value>EPAM40</value>
              <label>Epam40</label>
            </velem>
            <velem>
              <value>EPAM400</value>
              <label>Epam400</label>
            </velem>
            <velem>
              <value>EPAM410</value>
              <label>Epam410</label>
            </velem>
            <velem>
              <value>EPAM420</value>
              <label>Epam420</label>
            </velem>
            <velem>
              <value>EPAM430</value>
              <label>Epam430</label>
            </velem>
            <velem>
              <value>EPAM440</value>
              <label>Epam440</label>
            </velem>
            <velem>
              <value>EPAM450</value>
              <label>Epam450</label>
            </velem>
            <velem>
              <value>EPAM460</value>
              <label>Epam460</label>
            </velem>
            <velem>
              <value>EPAM470</value>
              <label>Epam470</label>
            </velem>
            <velem>
              <value>EPAM480</value>
              <label>Epam480</label>
            </velem>
            <velem>
              <value>EPAM490</value>
              <label>Epam490</label>
            </velem>
            <velem>
              <value>EPAM50</value>
              <label>Epam50</label>
            </velem>
            <velem>
              <value>EPAM500</value>
              <label>Epam500</label>
            </velem>
            <velem>
              <value>EPAM60</value>
              <label>Epam60</label>
            </velem>
            <velem>
              <value>EPAM70</value>
              <label>Epam70</label>
            </velem>
            <velem>
              <value>EPAM80</value>
              <label>Epam80</label>
            </velem>
            <velem>
              <value>EPAM90</value>
              <label>Epam90</label>
            </velem>
            <velem>
              <value>SSSUB</value>
              <label>Sssub</label>
            </velem>
          </vlist>
          <format>
            <code proglang="python">("", " -datafile=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>3</argpos>
          <comment>
            <text lang="en">'Epprofile' for Gribskov type, or EBLOSUM62</text>
          </comment>
        </parameter>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_required</name>
      <prompt lang="en">Required section</prompt>

      <parameters>

        <parameter issimple="1" ismandatory="1">
          <name>e_name</name>
          <prompt lang="en">Enter a name for the profile</prompt>
          <type>
            <datatype>
              <class>String</class>
            </datatype>
          </type>
          <vdef>
            <value>mymatrix</value>
          </vdef>
          <format>
            <code proglang="python">("", " -name=" + str(value))[value is not None and value!=vdef]</code>
          </format>
          <argpos>4</argpos>
        </parameter>

        <paragraph>
          <name>e_profiletypesection</name>
          <prompt lang="en">Profile type specific options</prompt>

          <parameters>

            <parameter>
              <name>e_threshold</name>
              <prompt lang="en">Enter threshold reporting percentage (value from 1 to 100)</prompt>
              <type>
                <datatype>
                  <class>Integer</class>
                </datatype>
              </type>
              <precond>
                <code proglang="python">e_type =="F"</code>
              </precond>
              <vdef>
                <value>75</value>
              </vdef>
              <format>
                <code proglang="python">("", " -threshold=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <ctrl>
                <message>
                  <text lang="en">Value greater than or equal to 1 is required</text>
                </message>
                <code proglang="python">value &gt;= 1</code>
              </ctrl>
              <ctrl>
                <message>
                  <text lang="en">Value less than or equal to 100 is required</text>
                </message>
                <code proglang="python">value &lt;= 100</code>
              </ctrl>
              <argpos>5</argpos>
            </parameter>
          </parameters>
        </paragraph>

        <paragraph>
          <name>e_gapsection</name>
          <prompt lang="en">Gap options</prompt>

          <parameters>

            <parameter>
              <name>e_open</name>
              <prompt lang="en">Gap opening penalty</prompt>
              <type>
                <datatype>
                  <class>Float</class>
                </datatype>
              </type>
              <precond>
                <code proglang="python">e_type !="F"</code>
              </precond>
              <vdef>
                <value>3.0</value>
              </vdef>
              <format>
                <code proglang="python">("", " -open=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>6</argpos>
            </parameter>

            <parameter>
              <name>e_extension</name>
              <prompt lang="en">Gap extension penalty</prompt>
              <type>
                <datatype>
                  <class>Float</class>
                </datatype>
              </type>
              <precond>
                <code proglang="python">e_type !="F"</code>
              </precond>
              <vdef>
                <value>0.3</value>
              </vdef>
              <format>
                <code proglang="python">("", " -extension=" + str(value))[value is not None and value!=vdef]</code>
              </format>
              <argpos>7</argpos>
            </parameter>
          </parameters>
        </paragraph>
      </parameters>
    </paragraph>

    <paragraph>
      <name>e_output</name>
      <prompt lang="en">Output section</prompt>

      <parameters>

        <parameter>
          <name>e_outfile</name>
          <prompt lang="en">Name of the output file (e_outfile)</prompt>
          <type>
            <datatype>
              <class>Filename</class>
            </datatype>
          </type>
          <vdef>
            <value>prophecy.e_outfile</value>
          </vdef>
          <format>
            <code proglang="python">("" , " -outfile=" + str(value))[value is not None]</code>
          </format>
          <argpos>8</argpos>
        </parameter>

        <parameter isout="1">
          <name>e_outfile_out</name>
          <prompt lang="en">outfile_out option</prompt>
          <type>
            <datatype>
              <class>ProphecyReport</class>
              <superclass>Report</superclass>
            </datatype>
          </type>
          <filenames>
            <code proglang="python">e_outfile</code>
          </filenames>
        </parameter>
      </parameters>
    </paragraph>

    <parameter ishidden="1">
      <name>auto</name>
      <prompt lang="en">Turn off any prompting</prompt>
      <type>
        <datatype>
          <class>String</class>
        </datatype>
      </type>
      <format>
        <code proglang="python">" -auto -stdout"</code>
      </format>
      <argpos>9</argpos>
    </parameter>
  </parameters>
</program>
